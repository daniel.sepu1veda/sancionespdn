import './bootstrap';
import { createApp } from "vue/dist/vue.esm-bundler";
import app from './layouts/Particulares/app.vue';
import registro from './layouts/Particulares/Sanciones/Registro-main.vue';
import servidoresSancion from './layouts/Servidores_Publicos/Sanciones/Registro.vue';
import particularesSancionados from './layouts/Particulares/Main.vue';
import servidoresSancionados from './layouts/Servidores_Publicos/main.vue';
import DetallesSancionParticulares from './layouts/Particulares/Sanciones/detalles-particulares.vue';
import DetallesSancionServidores from './layouts/Servidores_Publicos/Sanciones/detalles-servidores-publicos.vue';
import indexServidores from './layouts/Servidores_Publicos/Index/index.vue';
import indexParticulares from './layouts/Particulares/Index/index.vue';
import createConstancias from './layouts/Constancias/Index/create.vue';
import indexConstancias from './layouts/Constancias/Index/index.vue';
import detalleConstancias from './layouts/Constancias/Index/detalles.vue';
import DetallesServidores from './layouts/Servidores_Publicos/Detalles.vue';
import DetallesParticular from './layouts/Particulares/Detalles.vue';
import indexUsuarios from './layouts/Usuarios/Index.vue';
import detallesUsuarios from './layouts/Usuarios/Detalles.vue';
import registroUsuarios from './layouts/Usuarios/Registro.vue';
import bitacoraUsuarios from './layouts/Usuarios/Bitacora.vue';
import cargaServidores from './layouts/Servidores_Publicos/carga.vue';
import cargaParticulares from './layouts/Particulares/carga.vue';
import indexNotificaciones from './layouts/Notificaciones/index.vue';
import createNotificaciones from './layouts/Notificaciones/create.vue';
import detallesNotificaciones from './layouts/Notificaciones/details.vue';
import indexHome from './layouts/Home/index.vue';



import Header from './layouts/globales/Header.vue';
import Aside from './layouts/globales/Aside.vue';

import axios from 'axios';

const valores = createApp({
  components: {
    particularesSancionados,
    app,
    registro,
    servidoresSancionados,
    servidoresSancion,
    DetallesSancionParticulares,
    DetallesSancionServidores,
    indexServidores,
    indexParticulares,
    DetallesServidores,
    DetallesParticular,
    createConstancias,
    indexConstancias,
    detalleConstancias,
    indexUsuarios,
    detallesUsuarios,
    registroUsuarios,
    bitacoraUsuarios,
    cargaServidores,
    cargaParticulares,
    indexNotificaciones,
    createNotificaciones,
    detallesNotificaciones,
    Header,
    indexHome,
    Aside,
  },
  props: {
    variables: Object
  },
  provide() {
    return {
      csrf: document
        .querySelector('meta[name="csrf-token"]')
        .getAttribute("content")
    };
  }
});

valores.mixin({ 
  methods: { 
    route,
    can(permissionName) {
        return Permissions.indexOf(permissionName) !== -1;
    },
    role(){
      return Role.id;
    },
  } 
});

valores.mount('#app');
