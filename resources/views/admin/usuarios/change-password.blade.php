<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
        <!-- Styles -->

        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}">

        <!-- Layout config Js -->
        <script src="{{ asset('js/layout.js') }}"></script>

        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <!-- Bootstrap Css -->
        <!-- Icons Css -->
        <link href="{{ asset('css/icons.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- App Css-->
        <link href="{{ asset('css/app.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- custom Css-->
        <link href="{{ asset('css/custom.min.css') }}" rel="stylesheet" type="text/css" />
       
    </head>
    <style type="text/css">
    .auto-style1 {
        width: 100%;
        position: relative;
        z-index: 1;
        display: -webkit-box;
        display: -webkit-flex;
        display: -moz-box;
        display: -ms-flexbox;
        display: flex;
        flex-wrap: wrap;
        flex-direction: column;
        align-items: center;
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center;
        padding: 70px 15px 74px 15px;
        left: 0px;
        top: 0px;
    }

    .bg{
        background-image:url("{{asset("images/login-img/lirbos.jpg")}}");
        background-position:center center;
    }

    h5{
        font-size: 1.16rem !important;
    }

   
   
</style>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
        <!-- Styles -->

        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}">

        <!-- Layout config Js -->
        <script src="{{ asset('js/layout.js') }}"></script>
        <!-- Bootstrap Css -->
        <!-- Icons Css -->
        <link href="{{ asset('css/icons.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- App Css-->
        <link href="{{ asset('css/app.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- custom Css-->
        <link href="{{ asset('css/custom.min.css') }}" rel="stylesheet" type="text/css" />
       
    </head>
    <style type="text/css">
    .auto-style1 {
        width: 100%;
        position: relative;
        z-index: 1;
        display: -webkit-box;
        display: -webkit-flex;
        display: -moz-box;
        display: -ms-flexbox;
        display: flex;
        flex-wrap: wrap;
        flex-direction: column;
        align-items: center;
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center;
        padding: 70px 15px 74px 15px;
        left: 0px;
        top: 0px;
    }

    .bg{
        background-image:url("{{asset("images/login-img/lirbos.jpg")}}");
        background-position:center center;
    }

    h5{
        font-size: 1.16rem !important;
    }

   
   
</style>
    <body style="background: #ffe259; background:linear-gradient(to right,#a0b5eb,#c9f0e4)"> 

<div class="container w-75 bg-primary mt-5 rounded shadow limiter">
    <div class="row align-items-md-stretch">
        <div class="col bg d-none d-lg-block col-md-5 col-lg-g com-xl-6 rounded">

        </div>

        

        <div class="col bg-white p-5 rounded-end">

            {{-- <img src="{{ asset('images/logo_sea_vertical.png') }}" alt="Logo PDE" height="120" style="display: block; margin-left: auto;"> --}}

            {{-- <img src="{{ asset('images/logo_sea_vertical.png') }}" alt="Logo PDE" height="120" style="display: block; margin:0 auto;"> --}}

            <img src="{{ asset('images/SEA_logo.png') }}" alt="Logo PDE" height="100" style="display: block; margin: 0 auto;">
            
            {{-- <img src="{{ asset('images/SEA_logo.png') }}" alt="Logo PDE" height="100" style="display: block; margin-left: auto;">             --}}

            <h5 class="fw-bold" style="text-justify: auto; margin-top: 10px;">Sistema Estatal de Servidores Públicos y Particulares Sancionados</h5>

            <h6 class="" style="text-align: center; margin-top: 10px;">Cambio de contraseña por primer ingreso</h6>

            <img src="{{ asset('images/index-img/icon.png') }}" alt="Logo PDE" height="100" style="display: block; margin: 0 auto;">

            <p style="text-align: center">Usuario: {{ auth()->user()->nombres }}</p>


            <form class="" method="POST" action="{{ route('usuarios.edit') }}" onsubmit="return cambiarContrasena()">                @csrf
                <div class="form-group">
                    <label>Nueva contraseña</label>
                    <input  type="password" name="password" id="password1" class="form-control"  autocomplete="off"  placeholder="Ingrese su nueva contraseña">
                        <span class="text-danger" role="alert">
                            
                        </span>
                  </div>

                  <input type="hidden" name="id" value="{{auth()->user()->id}}">

                  <div class="form-group" style="margin-top: 15px;">
                    <label>Ingrese de nuevo la contraseña</label>
                    <input  type="password" id="password2" class="form-control"  autocomplete="off" placeholder="Ingrese su contraseña nuevamente">
                        <span class="text-danger" role="alert">
                        </span>
                  </div>

                <div class="d-grid" style="margin-top:15px">
                    <button type="submit" class="btn btn-primary">Cambiar contraseña</button>
                  </div>
             
                <div class="my-3" style="text-align: center;">
                    <span style="font-weight:bold">¿No es usted? Volver </span><a href="{{ route('login') }}">Iniciar sesión</a>
                </div>

                <input type="hidden" name="_method" value="PUT">



            </form>
            
        </div>
    </div>
   
    <script>
        function cambiarContrasena() {
            var password1 = document.getElementById('password1').value;
            var password2 = document.getElementById('password2').value;
    
            if (password1.trim() === '' || password2.trim() === '') {
                // Use SweetAlert for error message
                Swal.fire({
                    icon: 'error',
                    title: 'Campos vacíos',
                    text: 'Por favor, complete ambos campos de contraseña.'
                });
                return false; // Cancela el envío del formulario
            }
    
            if (password1 === password2) {
                // Use SweetAlert for success message
                Swal.fire({
                    icon: 'success',
                    title: 'Contraseña cambiada exitosamente!',
                    showConfirmButton: true,
                }).then((result) => {
                    // Después de mostrar la alerta, submit el formulario
                    if (result.isConfirmed) {
                        document.forms[0].submit();
                    }else{
                        document.forms[0].submit();
                    }
                });
                return false; // Cancela el envío del formulario para permitir que se muestre la alerta
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Las contraseñas no son iguales',
                    text: 'Por favor, asegúrese de que las contraseñas coincidan.'
                });
                return false; // Cancela el envío del formulario
            }
        }
    </script>
    
    
    
    

</div>
</body>
</html>
</html>