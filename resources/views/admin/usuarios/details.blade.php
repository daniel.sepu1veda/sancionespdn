<!DOCTYPE html>
<html lang="en">
    <x-head>
    </x-head>
    <body>
        <div id="app">
            <x-header></x-header>
            <x-aside></x-aside>

            <app> <detalles-usuarios :variables='@json(get_defined_vars())'></detalles-usuarios> </app>
        </div>
        @routes
    </body>
    <x-scripts>
    </x-scripts>
</html>

