<!DOCTYPE html>
<html lang="en">
    <x-head>
    </x-head>
    <body>
        <div id="app">
            <x-header></x-header>
            <x-aside></x-aside>
            <app> <servidores-sancion :variables='@json(get_defined_vars())'>
            
            </servidores-sancion> </app>
        </div>
        @routes
    </body>
    <x-scripts>
    </x-scripts>
</html>

