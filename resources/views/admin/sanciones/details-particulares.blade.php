<!DOCTYPE html>
<html lang="en">
    <x-head>
    </x-head>
    <body>
        <div id="app">
            <x-header></x-header>
            <x-aside></x-aside>
            <app> <detalles-sancion-particulares :variables='@json(get_defined_vars())'>
            
            </detalles-sancion-particulares> </app>
        </div>
        @routes
    </body>
    <x-scripts>
    </x-scripts>
</html>

