<!DOCTYPE html>
<html lang="en">
    <x-head>
    </x-head>
    <body>
        <div id="app">
            <x-header></x-header>
            <x-aside></x-aside>
            <app> <detalles-sancion-servidores :variables='@json(get_defined_vars())'>
            
            </detalles-sancion-servidores> </app>
        </div>
        @routes
    </body>
    <x-scripts>
    </x-scripts>
</html>

