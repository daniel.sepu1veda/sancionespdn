<!DOCTYPE html>
<html lang="en">
    <x-head>
    </x-head>
    <body>
        <div id="app">
            <x-header></x-header>
            <x-aside></x-aside>
            <app> <index-home :variables='@json(get_defined_vars())'>
            
            </index-home> </app>
        </div>
        @routes
    </body>
    <x-scripts>
    </x-scripts>
</html>

