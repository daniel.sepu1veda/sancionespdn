<!DOCTYPE html>
<html lang="en">
    <x-head>
    </x-head>
    <body>
        <div id="app">
            <app>
                <x-header></x-header>
                <x-aside></x-aside>

                <detalle-constancias :variables='@json(get_defined_vars())'>
                </detalle-constancias> 
            </app>
        </div>
        @routes
    </body>
    <x-scripts>
    </x-scripts>
</html>
