<!DOCTYPE html>
<html lang="en">

<x-head>
</x-head>

<body>
    <div id="app">
        <app>
            <x-header></x-header>
            <x-aside></x-aside>
            <div class="main-content">
                <div class="page-content">
                    <div class="container-fluid">
                    @if(session()->has('success'))
                    <x-sucess-alert></x-sucess-alert>
                    @endif

                    @if(session()->has('error'))
                    <x-error-alert></x-error-alert>
                    @endif
                        <create-constancias :variables='@json(get_defined_vars())'>
                        </create-constancias>
                    </div>
                </div>
            </div>
        </app>
    </div>

    @routes

</body>

<x-scripts>
</x-scripts>

</html>
