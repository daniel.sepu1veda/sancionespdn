<!DOCTYPE html>
<html lang="en">
    <x-head>
    </x-head>
    <body>
        <div id="app">
            <app>
                <x-header></x-header>
                <x-aside></x-aside>
                <bitacora-usuarios></bitacora-usuarios> 
            </app>
        </div>
        @routes
    </body>
    
    <x-scripts>
    </x-scripts>
</html>
