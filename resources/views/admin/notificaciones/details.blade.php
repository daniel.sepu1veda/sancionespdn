<!DOCTYPE html>
<html lang="en">
    <x-head>
    </x-head>
    <body>
        <div id="app">
            <app>
                <x-header></x-header>
                <x-aside></x-aside>

                <detalles-notificaciones :variables='@json(get_defined_vars())'>
                </detalles-notificaciones> 
            </app>
        </div>
        @routes
    </body>
    <x-scripts>
    </x-scripts>
</html>
