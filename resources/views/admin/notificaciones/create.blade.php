<!DOCTYPE html>
<html lang="en">
    <x-head>
    </x-head>
    <body>
        <div id="app">
            <app> 
                <x-header></x-header>
                <x-aside></x-aside>
                <create-notificaciones :variables='@json(get_defined_vars())'>
                </create-notificaciones> 
            </app>
        </div>
        @routes
    </body>
    <x-scripts>
    </x-scripts>
</html>
