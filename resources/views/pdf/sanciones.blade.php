<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sanción</title>
    <style>
        body {
            font-family: 'Arial', sans-serif;
            margin: 10px; /* Eliminar el margen predeterminado */
            padding: 10px;
            background-color: rgba(255, 255, 255, 1); 
            font-size: 16px;
            width: 100%;
            height: 100%;
            background-image: url('data:image/jpeg;base64,{{base64_encode(file_get_contents(public_path('images/lineas4.jpg')))}}');
        }

        html{
            width: 100%;
        height: 100%;
        background-image: url('data:image/jpeg;base64,{{base64_encode(file_get_contents(public_path('images/lineas4.jpg')))}}');

        }
        
        .container {
            margin: 0 auto; /* Centrar el contenido horizontalmente */
            padding: 40px;
            max-width: 800px;
        }

        h1, h2 {
            color: #333;
        }

        h1 {
            padding-bottom: 10px;
            font-size: 24px;
            text-align: center;
        }

        h2 {
            margin-top: 20px;
            border-bottom: 1px solid #333;
            padding-bottom: 10px;
            font-size: 20px;
        }

        p, ul {
            color: #555;
        }

        ul {
            list-style: none;
            padding-left: 0;
        }

        li {
            margin-bottom: 10px;
        }

        .label {
            font-weight: bold;
            margin-right: 10px;
            display: inline-block;
            min-width: 240px;
            color: #333;
        }

        a {
            color: #007bff;
            text-decoration: none;
        }

       

        .separator {
         border-top: 1px solid #7f63e4;
         margin-top: 20px;
  }

        a:hover {
            text-decoration: underline;
        }

        span{
            color:black
        }

        
    </style>
</head>
    <body>
        <div class="container">

            <div style="position: absolute; top: 0; left: 0;">
                <img style="height: 100px;" src="{{'data:image/jpeg;base64,'.base64_encode(file_get_contents(public_path('images/LOGOSESEA.png'))) }}" alt="Logo">
            </div>
    
            <!-- Imagen en la lado derecho (parte superior) -->
            <div style="position: absolute; top: 13px; right: 0;">
                <img style="height: 80px;" src="{{'data:image/jpeg;base64,'.base64_encode(file_get_contents(public_path('images/pde.png'))) }}" alt="Logo">
            </div>

            </div>

            <h1>Secretaría Ejecutiva Sistema Estatal Anticorrupción</h1>

            <h2 style="text-align: center; border-bottom: 5px solid #7f63e4; margin-top: 5px; ">Detalles de la Sanción</h2>
        
            <!-- Información General -->
            <h2>Información General</h2>
            <ul>
                <li><span class="label">Expediente:</span> {{ $sancion->expediente }}</li>
                @if($sancion->sancionadoTipo == 'App\Models\Particular')
                    <li><span class="label">Institución Sancionadora:</span> {{ $sancion->institucionDependencia->nombre }}</li>
                @endif
                <li><span class="label">Autoridad Sancionadora:</span> {{ $sancion->autoridadSancionadora }}</li>
                <li><span class="label">Causa/Motivo/Hechos:</span> {{ $sancion->causaMotivoHechos }}</li>
                <li><span class="label">Objeto de Contrato:</span> {{ $sancion->objetoContrato }}</li>
                <li><span class="label">Acto:</span> {{ $sancion->acto }}</li>
                <li><span class="label">Observaciones:</span> {{ $sancion->observaciones }}</li>
                <li><span class="label">Tipo de Falta:</span> {{ $sancion->tipoFalta->valor ?? '' }}</li>
                <li><span class="label">Tipos de Sanción:</span></li>
                @foreach($sancion->tiposSancion as $tipoSancion)
                    <li><span class="label"></span>{{ $tipoSancion->pivot->valor }}</li>
                @endforeach
            </ul>
        
            <!-- Datos del Sancionado -->
            @if($sancion->sancionadoTipo == 'App\Models\ServidorPublico')
                <h2>Datos del Servidor Público</h2>
                <ul>
                    <li><span class="label">RFC:</span> {{ $sancion->sancionado->rfc ?? ''}}</li>
                    <li><span class="label">Nombre:</span> {{ $sancion->sancionado->nombres ?? ''}}</li>
                    <li><span class="label">Primer apellido:</span> {{ $sancion->sancionado->primerApellido ?? ''}}</li>
                    <li><span class="label">Segundo apellido:</span> {{ $sancion->sancionado->segundoApellido ?? ''}}</li>
                    <li><span class="label">Género:</span> {{ $sancion->sancionado->genero['valor'] ?? ''}}</li>
                    <li><span class="label">Institución:</span> {{ $sancion->institucionDependencia->nombre ?? ''}}</li>
                    <li><span class="label">Puesto:</span> {{ $sancion->sancionado->puesto ?? ''}}</li>
                    <li><span class="label">Nivel:</span> {{ $sancion->sancionado->nivel ?? ''}}</li>
                </ul>
            @endif
            
            @if($sancion->sancionadoTipo == 'App\Models\Particular')
                <h2>Datos del Particular</h2>
                @if($sancion->sancionado->tipoPersonaId == 1)
                    <ul>
                        <li><span class="label">RFC:</span> {{ $sancion->sancionado->rfc ?? '' }}</li>
                        <li><span class="label">Nombre:</span> {{ $sancion->sancionado->nombreRazonSocial ?? '' }}</li>
                        <li><span class="label">Teléfono:</span> {{ $sancion->sancionado->objetoSocial ?? ''}}</li>
                        <li><span class="label">Persona:</span> {{ $sancion->sancionado->tipoPersona->valor ?? ''}}</li>
                    </ul>
                @endif
                @if($sancion->sancionado->tipoPersonaId == 2)
                    <ul>
                        <li><span class="label">RFC:</span> {{ $sancion->sancionado->rfc ?? '' }}</li>
                        <li><span class="label">Razón Social:</span> {{ $sancion->sancionado->nombreRazonSocial ?? '' }}</li>
                        <li><span class="label">Objeto social:</span> {{ $sancion->sancionado->objetoSocial ?? ''}}</li>
                        <li><span class="label">Persona:</span> {{ $sancion->sancionado->tipoPersona->valor ?? ''}}</li>
                    </ul>
                @endif

                {{-- Domicilio Mexico --}}
                @if(isset($sancion->sancionado->domicilioMexico))
                    <h2>Domicilio en México</h2>
                        <ul>
                            <li><span class="label">Código Postal:</span> {{ $sancion->sancionado->domicilioMexico->codigoPostal ?? '' }}</li>
                            <li><span class="label">Número Exterior:</span> {{ $sancion->sancionado->domicilioMexico->numeroExterior ?? '' }}</li>
                            <li><span class="label">Número Interior:</span> {{ $sancion->sancionado->domicilioMexico->numeroInterior ?? '' }}</li>
                            <li><span class="label">País:</span> {{ $sancion->sancionado->domicilioMexico->pais['valor'] ?? '' }}</li>
                            <li><span class="label">Entidad Federativa:</span> {{ $sancion->sancionado->domicilioMexico->entidadFederativa['valor'] ?? '' }}</li>
                            <li><span class="label">Municipio:</span> {{ $sancion->sancionado->domicilioMexico->municipio['valor'] ?? '' }}</li>
                            <li><span class="label">Localidad:</span> {{ $sancion->sancionado->domicilioMexico->localidad['valor'] ?? '' }}</li>
                            <li><span class="label">Vialidad:</span> {{ $sancion->sancionado->domicilioMexico->vialidad['valor'] ?? '' }}</li>
                        </ul>
                @endif

                {{-- Domicilio Exntarjero --}}
                @if(isset($sancion->sancionado->domicilioExtranjero))
                    <h2>Domicilio en el Extranjero</h2>
                    <ul>
                        <li><span class="label">Calle:</span> {{ $sancion->sancionado->domicilioExtranjero->calle ?? '' }}</li>
                        <li><span class="label">Número Exterior:</span> {{ $sancion->sancionado->domicilioExtranjero->numeroExterior ?? '' }}</li>
                        <li><span class="label">Número Interior:</span> {{ $sancion->sancionado->domicilioExtranjero->numeroInterior ?? '' }}</li>
                        <li><span class="label">Ciudad/Localidad:</span> {{ $sancion->sancionado->domicilioExtranjero->ciudadLocalidad ?? '' }}</li>
                        <li><span class="label">Estado/Provincia:</span> {{ $sancion->sancionado->domicilioExtranjero->estadoProvincia ?? '' }}</li>
                        <li><span class="label">Código Postal:</span> {{ $sancion->sancionado->domicilioExtranjero->codigoPostal ?? '' }}</li>
                        <li><span class="label">País:</span> {{ $sancion->sancionado->domicilioExtranjero->pais['valor'] ?? '' }}</li>
                    </ul>
                @endif
            @endif
        
            <!-- Resolución -->
            @if(isset($sancion->resolucion))
                <h2>Resolución</h2>
                <ul>
                    <li><span class="label">Sentido:</span> {{ $sancion->resolucion->sentido ?? '' }}</li>
                    @if($sancion->sancionadoTipo == 'App\Models\ServidorPublico')
                        <li><span class="label">Fecha:</span> {{ $sancion->resolucion->fechaResolucion ?? '' }}</li>
                    @else
                        <li><span class="label">Fecha de Notificación:</span> {{ $sancion->resolucion->fechaNotificacion ?? '' }}</li>
                    @endif
                    <span class="label">URL:</span> <a href="{{ $sancion->resolucion->url ?? '' }}">Ver Resolución</a>
                </ul>
            @endif
        
            <!-- Multa -->
            @if(isset($sancion->multa))
                <h2>Multa</h2>
                <ul>
                    <li><span class="label">Monto:</span> {{ $sancion->multa->monto ?? '' }} {{ $sancion->multa->moneda->valor ?? '' }}</li>
                </ul>
            @endif
        
            <!-- Inhabilitación -->
            @if(isset($sancion->inhabilitacion))
                <h2>Inhabilitación</h2>
                <ul>
                    <li><span class="label">Plazo:</span> {{ $sancion->inhabilitacion->plazo ?? '' }}</li>
                    <li><span class="label">Fecha Inicial:</span> {{ $sancion->inhabilitacion->fechaInicial ?? '' }}</li>
                    <li><span class="label">Fecha Final:</span> {{ $sancion->inhabilitacion->fechaFinal ?? '' }}</li>
                </ul>
            @endif
        
            <!-- Documentos -->
            @if(sizeof($sancion->documentos) > 0)
                <h2>Documentos</h2>
                @foreach($sancion->documentos as $documento)
                <ul>
                    <li>
                        <span class="label">Tipo:</span> {{ $documento->tipo ?? '' }}<br>
                        <span class="label">Título:</span> {{ $documento->titulo ?? '' }}<br>
                        <span class="label">Descripción:</span> {{ $documento->descripcion ?? '' }}<br>
                        <span class="label">URL:</span> <a href="{{ $documento->url ?? '' }}">Ver Documento</a>
                    </li>
                </ul>
                @endforeach
            @endif
        </div>
    </body>
</html>
