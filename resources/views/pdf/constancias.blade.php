<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Constancia</title>
    <style>

        
        body {
            font-family: 'Arial', sans-serif;
            margin: 10px; /* Eliminar el margen predeterminado */
            padding: 10px;
            background-color: rgba(255, 255, 255, 1); 
            font-size: 16px;
            width: 100%;
            height: 100%;
            background-image: url('data:image/jpeg;base64,{{base64_encode(file_get_contents(public_path('images/lineas4.jpg')))}}');

        }

        html{
            width: 100%;
        height: 100%;
        background-image: url('data:image/jpeg;base64,{{base64_encode(file_get_contents(public_path('images/lineas4.jpg')))}}');

        }
        
        .container {
            margin: 0 auto; /* Centrar el contenido horizontalmente */
            padding: 40px;
            max-width: 800px;
        }

        h1, h2 {
            color: #333;
        }

        h1 {
            padding-bottom: 10px;
            font-size: 24px;
            text-align: center;
        }

        h2 {
            margin-top: 20px;
            border-bottom: 1px solid #333;
            padding-bottom: 10px;
            font-size: 20px;
        }

        p, ul {
            color: #555;
        }

        ul {
            list-style: none;
            padding-left: 0;
        }

        li {
            margin-bottom: 10px;
        }

        .label {
            font-weight: bold;
            margin-right: 10px;
            display: inline-block;
            min-width: 240px;
            color: #333;
        }

        a {
            color: #007bff;
            text-decoration: none;
        }

       

        .separator {
         border-top: 1px solid #7f63e4;
         margin-top: 20px;
  }

        a:hover {
            text-decoration: underline;
        }

        span{
            color:black
        }

        
    </style>
    
</head>
    <body>
        <div class="container">
            <div style="position: absolute; top: 0; left: 0;">
                <img style="height: 100px;" src="{{'data:image/jpeg;base64,'.base64_encode(file_get_contents(public_path('images/LOGOSESEA.png'))) }}" alt="Logo">
            </div>
    
            <!-- Imagen en la lado derecho (parte superior) -->
            <div style="position: absolute; top: 13px; right: 0;">
                <img style="height: 80px;" src="{{'data:image/jpeg;base64,'.base64_encode(file_get_contents(public_path('images/pde.png'))) }}" alt="Logo">
            </div>

            </div>

            <h1>Secretaría Ejecutiva Sistema Estatal Anticorrupción</h1>

            <h2 style="text-align: center; border-bottom: 5px solid #7f63e4; margin-top: 5px; ">Constancia de Sanción</h2>

            @if(is_null($sancion))
                <h1>Constancia</h1>
                <h2>RFC {{$servidorPublico->rfc ?? ($particular->rfc ?? '')}} no encontrado en el sistema</h3> 
            @endif

            @if(isset($sancion))
                <h2>Sanción encontrada en el sistema con expediente: {{$sancion->expediente}}</h2>

                <!-- Información General -->
                <h2>Información General</h2>
                <ul>
                    <li><span class="label">Expediente:</span> {{ $sancion->expediente }}</li>
                    @if($sancion->sancionadoTipo == 'App\Models\Particular')
                        <li><span class="label">Institución Sancionadora:</span> {{ $sancion->institucionDependencia->nombre }}</li>
                    @endif
                    <li><span class="label">Autoridad Sancionadora:</span> {{ $sancion->autoridadSancionadora }}</li>
                    <li><span class="label">Causa/Motivo/Hechos:</span> {{ $sancion->causaMotivoHechos }}</li>
                    <li><span class="label">Objeto de Contrato:</span> {{ $sancion->objetoContrato }}</li>
                    <li><span class="label">Acto:</span> {{ $sancion->acto }}</li>
                    <li><span class="label">Observaciones:</span> {{ $sancion->observaciones }}</li>
                </ul>
            
                <!-- Datos del Sancionado -->
                @if($sancion->sancionadoTipo == 'App\Models\ServidorPublico')
                    <h2>Datos del Servidor Público</h2>
                    <ul>
                        <li><span class="label">RFC:</span> {{ $sancion->sancionado->rfc }}</li>
                        <li><span class="label">Nombre:</span> {{ $sancion->sancionado->nombres }}</li>
                        <li><span class="label">Primer apellido:</span> {{ $sancion->sancionado->primerApellido }}</li>
                        <li><span class="label">Segundo apellido:</span> {{ $sancion->sancionado->segundoApellido }}</li>
                        <li><span class="label">Género:</span> {{ $sancion->sancionado->genero['valor'] }}</li>
                        <li><span class="label">Institución:</span> {{ $sancion->institucionDependencia->nombre }}</li>
                        <li><span class="label">Puesto:</span> {{ $sancion->sancionado->puesto }}</li>
                        <li><span class="label">Nivel:</span> {{ $sancion->sancionado->nivel }}</li>
                    </ul>
                    
                    {{-- @if(isset($servidorPublico->historial))
                        <h2>Datos Laborales del Servidor Público</h2>
                        @if(isset($servidorPublico->historial->cargoActual))
                            <ul>
                                <li><span class="label">Institución:</span> {{ $servidorPublico->historial->cargoActual->ente_publico}}</li>
                                <li><span class="label">Cargo:</span> {{ $servidorPublico->historial->cargoActual->cargo_comision}}</li>
                                <li><span class="label">Fecha de posesión:</span> {{ $servidorPublico->historial->cargoActual->fecha_posesion}}</li>
                            </ul>
                        @endif
                        @foreach ($servidorPublico->historial as $item)
                            
                        @endforeach
                        <ul>
                            <li><span class="label"></span> {{ "sí"}}</li>
                        </ul>
                    @endif --}}
                @endif
                
                @if($sancion->sancionadoTipo == 'App\Models\Particular')
                    <h2>Datos del Particular</h2>
                    @if($sancion->sancionado->tipoPersonaId == 1)
                        <ul>
                            <li><span class="label">RFC:</span> {{ $sancion->sancionado->rfc }}</li>
                            <li><span class="label">Nombre:</span> {{ $sancion->sancionado->nombreRazonSocial }}</li>
                            <li><span class="label">Teléfono:</span> {{ $sancion->sancionado->objetoSocial}}</li>
                            <li><span class="label">Persona:</span> {{ $sancion->sancionado->tipoPersona->valor}}</li>
                        </ul>
                    @endif
                    @if($sancion->sancionado->tipoPersonaId == 2)
                        <ul>
                            <li><span class="label">RFC:</span> {{ $sancion->sancionado->rfc }}</li>
                            <li><span class="label">Razón Social:</span> {{ $sancion->sancionado->nombreRazonSocial }}</li>
                            <li><span class="label">Objeto social:</span> {{ $sancion->sancionado->objetoSocial}}</li>
                            <li><span class="label">Persona:</span> {{ $sancion->sancionado->tipoPersona->valor}}</li>
                        </ul>
                    @endif
                @endif
            @endif
            
            <div class="title m-b-md text-center" style="text-align: center; align-content: center;  margin: 20px auto;">
                {!!QrCode::size(200)->generate($constancia->url) !!}
                <p style="border-top: none">Folio {{$constancia->folio}}</p>
             </div>
        </div>
    </body>
</html>
