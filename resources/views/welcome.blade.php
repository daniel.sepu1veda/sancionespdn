<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
        <!-- Styles -->

        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}">

        <!-- Layout config Js -->
        <script src="{{ asset('js/layout.js') }}"></script>
        <!-- Bootstrap Css -->
        <!-- Icons Css -->
        <link href="{{ asset('css/icons.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- App Css-->
        <link href="{{ asset('css/app.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- custom Css-->
        <link href="{{ asset('css/custom.min.css') }}" rel="stylesheet" type="text/css" />
       
    </head>
    <style type="text/css">
    .auto-style1 {
        width: 100%;
        position: relative;
        z-index: 1;
        display: -webkit-box;
        display: -webkit-flex;
        display: -moz-box;
        display: -ms-flexbox;
        display: flex;
        flex-wrap: wrap;
        flex-direction: column;
        align-items: center;
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center;
        padding: 70px 15px 74px 15px;
        left: 0px;
        top: 0px;
    }

    .bg{
        background-image: url("{{ asset('images/login-img/lirbos.jpg') }}");
        background-position:center center;
    }

    .btn-primary:hover {
color: #fff;
background-color: red;
border-color: #285e8e; /*set the color you want here*/
}
   
</style>
    <body style="background: #ffe259; background:linear-gradient(to right,#a0b5eb,#c9f0e4)"> 

<div class="container w-75 bg-primary mt-5 rounded shadow limiter">
    <div class="row align-items-md-stretch">
        <div class="col bg d-none d-lg-block col-md-5 col-lg-g com-xl-6 rounded">

        </div>

        

        <div class="col bg-white p-5 rounded-end">

            {{-- <img src="{{ asset('images/logo_sea_vertical.png') }}" alt="Logo PDE" height="120" style="display: block; margin-left: auto;"> --}}

            {{-- <img src="{{ asset('images/logo_sea_vertical.png') }}" alt="Logo PDE" height="120" style="display: block; margin:0 auto;"> --}}

            <img src="{{ asset('images/pdn.png') }}" alt="Logo PDE" height="150" style="display: block; margin: 0 auto;">
            
            {{-- <img src="{{ asset('images/SEA_logo.png') }}" alt="Logo PDE" height="100" style="display: block; margin-left: auto;">             --}}

            <h5 class="fw-bold" style="text-justify: auto; margin-top: 10px;">Sistema Nacional de Servidores Públicos y Particulares Sancionados</h5>

            <form class=""  method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group">
                    <label for="exampleInputEmail1">Correo electronico</label>
                    <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Ingrese su correo electronico">
                    @error('email')
                        <span class="text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>

                  <div class="form-group" style="margin-top: 15px;">
                    <label for="exampleInputPassword1">Contraseña</label>
                    <input name="password" type="password" class="form-control" id="exampleInputPassword1" placeholder="Ingrese su contraseña">
                    @error('password')
                        <span class="text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>


                <div class="mb-4 form-check contact100-form-checkbox" style="margin-top: 15px;"> 
                    <input type="checkbox"  name="remember-me" class="form-check-input" id="ckb1" /> 
                    <label for="ckb1" class="form-check-label">Mantener la sesión iniciada</label>
                </div>

                <div class="d-grid">
                    <button id="BtIniciarSesion" type="submit" class="btn btn-dark" Text="Iniciar sesión">Iniciar sesión</button>
                   
                </div>
             
                <div class="my-3">
                    <span style="font-weight:bold">En caso de no recordar tu contraseña, ponerse en contacto con el encargado de sistemas de la SESEA</span>
                </div>

            </form>
            
        </div>
    </div>

</div>
</body>
</html>
