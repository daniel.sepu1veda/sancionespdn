<script>
    @auth
    window.Permissions = @json(auth()->user()->allPermissions);
    window.Role = @json(auth()->user()->rol);
    @else
    window.Permissions = [];
    @endauth
</script> 