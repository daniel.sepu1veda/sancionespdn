@php
    $notifications = App\Helpers\AppHelper::get_notificaciones();
@endphp

<script>
    document.addEventListener('DOMContentLoaded', function() {
        document.getElementById('verTodas').addEventListener('click', function() {
            window.location.href = "{{ route('notificaciones') }}";
        });

        // Agregar evento de clic a cada notificación generada
        const notificationItems = document.querySelectorAll('.notification-item');
        notificationItems.forEach(item => {
            item.addEventListener('click', function() {
                // Obtener el ID de la notificación desde el atributo 'data-notification-id'
                const notificationId = this.getAttribute('data-notification-id');
                if (notificationId) {
                    window.location.href = "{{ route('notificaciones.show', '') }}/" + notificationId;
                }
            });
        });
    });
</script>

<div id="page-topbar">
    <div class="navbar-header">
        <div class="d-flex">
            <!-- LOGO -->
            <div class="navbar-brand-box d-flex align-items-center justify-content-center" style="background-color: #2a3042;">
               
                    <span class="logo-sm"></span>
                    <a href="{{ route('home') }}">
                        <span class="logo-lg">
                            <img src="{{ asset('images/login-img/pdn.png') }}" alt="Logo PDE" height="60" style="margin-top: 10px;">
                        </span>
                    </a>
            </div>
        
            <button type="button" class="btn btn-sm px-3 font-size-16 header-item waves-effect disabled" style="color: black; margin-top: 20px" id="vertical-menu-btn">
                <h4 class="mb-sm-0 font-size-18">Sistema estatal de servidores públicos y particulares sancionados</h4>
            </button>
        </div>
        

        <div class="d-flex">
            @if(auth()->check())
            <div class="dropdown d-inline-block">
                <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown"
                        data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img src="{{ asset('images/index-img/icon.png') }}" class="rounded-circle header-profile-user" alt="Header Avatar">
                    <span class="d-none d-xl-inline-block ms-1" key="t-henry">{{ auth()->user()->nombres }}</span>
                    <i class="mdi mdi-chevron-down d-none d-xl-inline-block"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-end">
                    <!-- item -->
                    <form id="logoutNavBar" action="{{ route('logout') }}" method="POST">
                        @csrf
                        <button class="dropdown-item text-danger" href="#">
                            <i class="bx bx-power-off font-size-16 align-middle me-1 text-danger"></i>
                            <span key="t-logout">Cerrar sesión</span>
                        </button>
                    </form>
                </div>
            </div>
            @endif

            {{-- @can('particulares.get') --}}
            @if(auth()->check())
            <div class="dropdown d-inline-block">
                <button type="button" class="btn header-item noti-icon waves-effect" id="page-header-notifications-dropdown"
                        data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="bx bx-bell"></i>
                    <span class="badge bg-danger rounded-pill">{{ App\Helpers\AppHelper::get_notificaciones_count() }}</span>

                </button>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end p-0"
                     aria-labelledby="page-header-notifications-dropdown">

                     <div style="background-color: #3f5088; padding: 10px; border-radius: 4px;">
                        <h5 class="text-white mt-2" style="text-align: center">Notificaciones</h5>
                    </div>
                    <div class="p-3">
                        
                        <div class="row align-items-center">
                            
                            <div class="col">
                                @foreach ($notifications as $notification)
                                <div class="{{'text-reset d-block dropdown-item position-relative notification-item'}}" data-notification-id="{{ $notification->id }}" style="background-color: #f1f5f8; display: flex; align-items: center; margin: 5px;">
                                    <div class="d-flex">
                                        <div class="flex-1">
                                            <a class="stretched-link" style="cursor: pointer">
                                                <h6>{{$notification->descripcion}}
                                                    <br><span class="text-muted"></span>
                                                </h6>
                                            </a>
                                            <p class="mb-0 fs-11 fw-medium text-uppercase text-muted">
                                                <span><i class="mdi mdi-clock-outline"></i> {{ date("d/m/Y h:ia", strtotime($notification->created_at))}} </span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            
                            
                            
                            <div class="text-center mt-2">
                                <a href="#" class="btn btn-soft-success waves-effect waves-light w-100" id="verTodas">
                                    Ver Todas <i class='bx bx-right-arrow-alt'></i>
                                </a>
                            </div>
                            
                            
                            
                            
                        </div>
                    </div>
                    <div data-simplebar style="max-height: 230px;">
                        <a href="javascript: void(0);" class="text-reset notification-item"></a>
                        <a href="javascript: void(0);" class="text-reset notification-item"></a>
                        <a href="javascript: void(0);" class="text-reset notification-item"></a>
                    </div>
                    <div class="p-2 border-top d-grid"></div>
                </div>
            </div>
            @endif
            {{-- @endcan --}}
        </div>
    </div>
</div>


