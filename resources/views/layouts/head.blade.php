<head>
    @vite('resources/js/app.js')

    <meta charset="utf-8" />
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <!-- Layout config Js -->
    <script src="{{ asset('js/layout.js') }}"></script>
    
    <!-- Bootstrap Css -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    
    <!-- Icons Css -->
    <link href="{{ asset('css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    
    <!-- App Css-->
    <link href="{{ asset('css/app.min.css') }}" rel="stylesheet" type="text/css" />
    
    <!-- Custom Css-->
    <link href="{{ asset('css/custom.min.css') }}" rel="stylesheet" type="text/css" />

    <script src="{{ asset('libs/apexcharts/apexcharts.min.js') }}"></script>
    
    <!-- Bootstrap Bundle Js -->
    <script src="{{ asset('libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <script src="{{ asset('js/pages/dashboard-blog.init.js') }}"></script>

    <script src="{{ asset('js/pages/dashboard-job.init.js') }}"></script>

    


    
    

</head>