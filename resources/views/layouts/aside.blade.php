<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        .arrow-icon {
            transition: transform 0.3s ease;
        }
        
    </style>
</head>
<body>
    <div class="vertical-menu" style="background-color: #2a3042;">
        <div data-simplebar class="h-100">
            <div id="sidebar-menu">
                <ul class="metismenu list-unstyled" id="side-menu">
                    <li class="menu-title" key="t-menu" style="color: white;">Menu</li>
                    <li class="menu-item" style="margin-top: 10px; cursor: pointer;">
                        <span class="menu-title" style="color: white;">
                            <i class="bx bx-chevron-right arrow-icon"></i>
                            Servidores Públicos
                        </span>
                        <ul class="submenu" style="display: none">
                            <li onmouseover="this.style.backgroundColor='#6450b5'"   onmouseout="this.style.backgroundColor='transparent'"><a href="{{ route('servidores.publicos.sancionados') }}" style="color: white;">Servidores Públicos Sancionados</a></li>
                        </ul>
                    </li>
                    
                    <li class="menu-item" style="margin-top: 20px; cursor: pointer;">
                        <span class="menu-title" style="color: white;">
                            <i class="bx bx-chevron-right arrow-icon"></i>
                            Particulares
                        </span>
                        <ul class="submenu" style="display: none">
                            <li onmouseover="this.style.backgroundColor='#6450b5'"  onmouseout="this.style.backgroundColor='transparent'"><a href="{{ route('particulares.sancionados') }}" style="color: white;">Particulares Sancionados</a></li>
                        </ul>
                    </li>
                    
                </ul>
            </div>
        </div>
    </div>

    <script>
        document.addEventListener('DOMContentLoaded', function () {
            var menuItems = document.querySelectorAll('.menu-item');
    
            menuItems.forEach(function (menuItem) {
                var submenu = menuItem.querySelector('.submenu');
                submenu.style.display = 'none'; // Set the initial display to 'none'
    
                menuItem.addEventListener('click', function () {
                    var submenuDisplay = window.getComputedStyle(submenu).display;
                    var arrowIcon = menuItem.querySelector('.arrow-icon');
                    var menuTitle = menuItem.querySelector('.menu-title');
    
                    if (submenuDisplay === 'block') {
                        submenu.style.display = 'none';
                        arrowIcon.style.transform = 'rotate(0deg)';
                        menuTitle.style.color = 'white'; // Set back to normal color
                    } else {
                        submenu.style.display = 'block';
                        arrowIcon.style.transform = 'rotate(90deg)';
                        menuTitle.style.color = '#916b9b'; // Set the new color
                        menuTitle.style.textShadow = '2px 2px 5px rgba(0, 0, 0, 0.5)';
                    }


                    
                });
            });
        });
    </script>
    
</body>
</html>
