<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sanctions', function (Blueprint $table) {
            $table->id();
            $table->timestamp('fechaCaptura');
            $table->string('expediente')->nullable();
            $table->string('autoridadSancionadora')->nullable();
            $table->text('causaMotivoHechos')->nullable();
            $table->string('objetoContrato')->nullable();
            $table->string('acto')->nullable();
            $table->text('observaciones')->nullable();
            $table->string('sancionadoTipo')->nullable();
            $table->unsignedBigInteger('sancionadoId')->nullable()->constrained()->onDelete('cascade');
            $table->foreignId('institucionDependenciaId')->nullable()->constrained('institutions')->onDelete('cascade');
            $table->foreignId('tipoFaltaId')->nullable()->constrained('fault_types')->onDelete('cascade');
            $table->foreignId('responsableSancionId')->nullable()->constrained('sanction_imposers')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sanctions');
    }
};
