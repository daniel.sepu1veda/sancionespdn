<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->id();
            $table->string('descripcion')->nullable();
            $table->text('texto')->nullable();
            $table->text('url')->nullable();
            $table->foreignId('usuarioId')->nullable()->constrained('users');
            $table->foreignId('destinatarioId')->nullable()->constrained('users');
            $table->foreignId('estadoNotificacionId')->nullable()->constrained('notification_statuses');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('notifications');
    }
};
