<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('foreign_addresses', function (Blueprint $table) {
            $table->id();
            $table->string('calle')->nullable();
            $table->string('numeroExterior')->nullable();
            $table->string('numeroInterior')->nullable();
            $table->string('ciudadLocalidad')->nullable();
            $table->string('estadoProvincia')->nullable();
            $table->string('codigoPostal')->nullable();
            $table->unsignedBigInteger('paisId')->nullable();
            $table->foreignId('particularId')->nullable()->constrained('individuals')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('foreign_addresses');
    }
};
