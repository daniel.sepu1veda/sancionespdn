<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sanction_legal_representative', function (Blueprint $table) {
            $table->id();
            $table->foreignId('sancionId')->nullable()->constrained('sanctions')->onDelete('cascade');
            $table->foreignId('apoderadoLegalId')->nullable()->constrained('legal_representatives')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sanction_legal_representative');
    }
};
