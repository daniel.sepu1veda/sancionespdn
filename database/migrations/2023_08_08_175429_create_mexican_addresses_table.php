<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('mexican_addresses', function (Blueprint $table) {
            $table->id();
            $table->string('codigoPostal')->nullable();
            $table->string('numeroExterior')->nullable();
            $table->string('numeroInterior')->nullable();
            $table->unsignedBigInteger('paisId')->nullable();
            $table->unsignedBigInteger('entidadFederativaId')->nullable();
            $table->unsignedBigInteger('municipioId')->nullable();
            $table->unsignedBigInteger('localidadId')->nullable();
            $table->foreignId('vialidadId')->nullable()->constrained('roads');
            $table->foreignId('particularId')->nullable()->constrained('individuals')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('mexican_addresses');
    }
};
