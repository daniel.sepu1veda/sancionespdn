<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('individuals', function (Blueprint $table) {
            $table->id();
            $table->string('nombreRazonSocial')->nullable();
            $table->text('objetoSocial')->nullable();
            $table->string('rfc')->nullable();
            $table->string('curp')->nullable();
            $table->string('telefono')->nullable();
            $table->foreignId('tipoPersonaId')->nullable()->constrained('person_types');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('individuals');
    }
};
