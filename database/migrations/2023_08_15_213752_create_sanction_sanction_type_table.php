<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sanction_sanction_type', function (Blueprint $table) {
            $table->id();
            $table->string('valor')->nullable();
            $table->foreignId('sancionId')->nullable()->constrained('sanctions')->onDelete('cascade');
            $table->foreignId('tipoSancionId')->nullable()->constrained('sanction_types')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sanction_sanction_type');
    }
};
