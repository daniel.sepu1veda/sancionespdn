<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Database\Seeder;
use App\Models\User;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        //roles
        $sistemas = Role::create(['name' => 'Administrador SESEA']);
        $sistemas = Role::create(['name' => 'Administrador de Ente Público']);
        $sistemas = Role::create(['name' => 'Contraloría del Estado']);
        $sistemas = Role::create(['name' => 'Sala de Justicia Administrativa']);
        $sistemas = Role::create(['name' => 'Juzgado Penal']);
        $sistemas = Role::create(['name' => 'Autoridad Competente']);
        $sistemas = Role::create(['name' => 'Interesado']);


        //obtener permiso de json
        $permissions = json_decode(file_get_contents(base_path("database/jsons/permissions.json")), true);
        foreach ($permissions as $permission) {
            $new_permission = Permission::create([
                'name' => $permission['name'],
                'description' => $permission['description'],
                'module' => $permission['module'],
            ]);

            //asignar permiso a rol
            if(isset($permission['roles']) && is_array($permission['roles'])){
                foreach ($permission['roles'] as $role) {
                    $new_permission->assignRole($role);
                }
            }
        }

        //asignar permisos
        $usuarios = User::all();
        foreach ($usuarios as $usuario){
            if($usuario->role_id != null){
                $usuario->assignRole($usuario->role_id);
            }
        }
    }
}
