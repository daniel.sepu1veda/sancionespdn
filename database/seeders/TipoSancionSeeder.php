<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\TipoSancion;

class TipoSancionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $tipos_sancion = json_decode(file_get_contents(base_path("database/jsons/sanction_types.json")), true);

        foreach ($tipos_sancion as $tipo_sancion) {
            TipoSancion::create($tipo_sancion);
        }
    }
}
