<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\DirectorGeneral;
use App\Models\ApoderadoLegal;
use App\Models\Particular;
use Illuminate\Support\Str;

class ParticularSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for($i=0; $i<20; $i++){
            //particular sancionado
            $tipoPersonaId = rand(1,2);

            $particular = Particular::create([
                'nombreRazonSocial' => fake()->name(),
                'objetoSocial' => fake()->sentence(),
                'rfc' => strtoupper(Str::random(13)),
                'curp' => strtoupper(Str::random(18)),
                'telefono' => fake()->phoneNumber(),
                'tipoPersonaId' => $tipoPersonaId,
            ]);

            if($tipoPersonaId == 2){
                ApoderadoLegal::create([
                    'nombres' => fake()->name(),
                    'primerApellido' => fake()->lastname(),
                    'segundoApellido' => fake()->lastname(),
                    'rfc' => strtoupper(Str::random(13)),
                    'curp' => strtoupper(Str::random(18)),
                    'particularId' => $particular->id,
                ]);

                DirectorGeneral::create([
                    'nombres' => fake()->name(),
                    'primerApellido' => fake()->lastname(),
                    'segundoApellido' => fake()->lastname(),
                    'rfc' => strtoupper(Str::random(13)),
                    'curp' => strtoupper(Str::random(18)),
                    'particularId' => $particular->id,
                ]);
            }
        }
    }
}
