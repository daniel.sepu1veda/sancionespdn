<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Pais;

class PaisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $paises = json_decode(file_get_contents(base_path("database/jsons/countries.json")), true);

        foreach ($paises as $pais) {
            Pais::create($pais);
        }
    }
}
