<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\TipoDocumento;

class TipoDocumentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $tipos_documento = json_decode(file_get_contents(base_path("database/jsons/document_types.json")), true);

        foreach ($tipos_documento as $tipo_documento) {
            TipoDocumento::create($tipo_documento);
        }
    }
}
