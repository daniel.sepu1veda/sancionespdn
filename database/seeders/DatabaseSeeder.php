<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call(UserSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(TipoSancionSeeder::class);
        $this->call(TipoFaltaSeeder::class);
        $this->call(TipoPersonaSeeder::class);
        $this->call(TipoDocumentoSeeder::class);
        $this->call(GeneroSeeder::class);
        $this->call(MonedaSeeder::class);
        $this->call(PaisSeeder::class);
        //$this->call(InstitucionDependenciaSeeder::class);
        //$this->call(ParticularSeeder::class);
        $this->call(EstadoNotificacionSeeder::class);
        $this->call(NotificacionSeeder::class);
        $this->call(TipoVialidadSeeder::class);
        //$this->call(DomicilioMexicoSeeder::class);
        //$this->call(DomicilioExtranjeroSeeder::class);
        //$this->call(SancionSeeder::class);
    }
}
