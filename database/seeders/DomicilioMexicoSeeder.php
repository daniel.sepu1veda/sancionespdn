<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Services\DomicilioService;
use Illuminate\Database\Seeder;
use App\Models\DomicilioMexico;
use App\Models\TipoVialidad;
use App\Models\Particular;
use App\Models\Localidad;
use App\Models\Vialidad;

class DomicilioMexicoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $particulares = Particular::where('id', '<', 11)->get();
        $tiposVialidad = TipoVialidad::all();

        //localidades
        if(env('DB_PDE_ALLOW_CONNECTION', false)){
            $localidades = DomicilioService::get_localidades_by_cp(23088);
        }
        
        //por cada particular
        foreach($particulares as $particular){
            if(env('DB_PDE_ALLOW_CONNECTION', false)){
                $localidad = $localidades['localidades'][rand(1,14)];
            }
            
            //vialidad
            $tipoVialidad = $tiposVialidad->random();

            $vialidad = Vialidad::create([
                'clave' => $tipoVialidad->clave,
                'valor' => fake()->name(),
                'tipoVialidadId' => $tipoVialidad->id,
            ]);

            //crear domicilio
            DomicilioMexico::create([
                'codigoPostal' => '23088',
                'numeroExterior' => fake()->randomNumber(3),
                'numeroInterior' => fake()->randomNumber(3),
                'paisId' => 146, 
                'entidadFederativaId' => 3, 
                'municipioId' => rand(17, 21), 
                'localidadId' => isset($localidad) ? $localidad['id'] : null,  
                'vialidadId' => $vialidad->id,
                'particularId' => $particular->id, 
            ]);
        }
    }
}
