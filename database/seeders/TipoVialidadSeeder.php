<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\TipoVialidad;

class TipoVialidadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $tipos_vialidad = json_decode(file_get_contents(base_path("database/jsons/road_types.json")), true);

        foreach ($tipos_vialidad as $tipo_vialidad) {
            TipoVialidad::create($tipo_vialidad);
        }
    }
}
