<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\EstadoNotificacion;


class EstadoNotificacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $estados = json_decode(file_get_contents(base_path("database/jsons/notification_statuses.json")), true);

        foreach ($estados as $estado) {
            EstadoNotificacion::create($estado);
        }
    }
}
