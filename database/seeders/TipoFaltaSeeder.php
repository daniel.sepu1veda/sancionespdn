<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\TipoFalta;

class TipoFaltaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $tipos_falta = json_decode(file_get_contents(base_path("database/jsons/fault_types.json")), true);

        foreach ($tipos_falta as $tipo_falta) {
            TipoFalta::create($tipo_falta);
        }
    }
}
