<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\DomicilioExtranjero;
use App\Models\Particular;

class DomicilioExtranjeroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $particulares = Particular::where('id', 1)->orWhere('id', '>=', 11)->get();

        //por cada particular
        foreach($particulares as $particular){
            //crear domicilio
            DomicilioExtranjero::create([
                'calle' => fake()->name(),
                'numeroExterior' => fake()->randomNumber(3),
                'numeroInterior' => fake()->randomNumber(3),
                'ciudadLocalidad' => fake()->city(),
                'estadoProvincia' => fake()->state(),
                'codigoPostal' => fake()->postcode(),
                'paisId' => rand(1, 243),
                'particularId' => $particular->id, 
            ]);
        }
    }
}
