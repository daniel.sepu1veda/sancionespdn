<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Notificacion;

class NotificacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for($i=0; $i<30; $i++){
            Notificacion::create([
                'descripcion' => 'Notificación de omisión de registro',
                'texto' => fake()->text(),
                'usuarioId' => 2,
                'destinatarioId' => rand(2,7),
                'estadoNotificacionId' => rand(1,4),
            ]);
        }
    }
}
