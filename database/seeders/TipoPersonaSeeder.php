<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\TipoPersona;

class TipoPersonaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $tipos_persona = json_decode(file_get_contents(base_path("database/jsons/person_types.json")), true);

        foreach ($tipos_persona as $tipo_persona) {
            Tipopersona::create($tipo_persona);
        }
    }
}
