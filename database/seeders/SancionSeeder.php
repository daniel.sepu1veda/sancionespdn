<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Services\ServidorPublicoService;
use App\Models\InstitucionDependencia;
use App\Models\ResponsableSancion;
use App\Models\ServidorPublico;
use App\Models\Inhabilitacion;
use App\Models\TipoDocumento;
use App\Models\TipoSancion;
use App\Models\Particular;
use App\Models\Resolucion;
use App\Models\Documento;
use App\Models\TipoFalta;
use App\Models\Sancion;
use App\Models\Multa;
use Carbon\Carbon;

class SancionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $particulares = Particular::with('domiciliosMexico', 'domiciliosExtranjero', 'apoderadosLegales', 'directoresGenerales')->get();
        $instituciones = InstitucionDependencia::all();
        $tiposDocumento = TipoDocumento::all();
        $tiposSancion = TipoSancion::all();
        $tiposFalta = TipoFalta::all();

        //sanciones de particulares
        for($i=0; $i<20; $i++){
            //institucion
            $institucion = $instituciones->random();
            $responsableSancion = null;
            
            //responsable sanción
            if(env('DB_PDE_ALLOW_CONNECTION', false)){
                while(is_null($responsableSancion)){
                    $random = rand(596, 8891);
                    $responsableSancion = ServidorPublicoService::get_servidor_publico_institucion_dependencia($random);
                    if(is_null($responsableSancion)){
                        echo $random . "\n";
                    }
                };

                $responsableSancion = ResponsableSancion::create([
                    'nombres' => $responsableSancion['nombres'],
                    'primerApellido' => $responsableSancion['primerApellido'],
                    'segundoApellido' => $responsableSancion['segundoApellido'],
                    'institucionDependenciaId' => $responsableSancion['institucionDependenciaId'] ?? null,
                ]);
            }else{
                $responsableSancion = ResponsableSancion::create([
                    'nombres' => fake()->firstname(),
                    'primerApellido' => fake()->lastname(),
                    'segundoApellido' => fake()->lastname(),
                    'institucionDependenciaId' => null,
                ]);
            }

            //particular 
            $particular = $particulares[$i];
            
            //sanción
            $sancion = Sancion::create([
                'expediente' => strtoupper(Str::random(10)),
                'autoridadSancionadora' => $institucion->nombre,
                'causaMotivoHechos' => fake()->sentence(),
                'objetoContrato' => fake()->sentence(),
                'acto' => fake()->sentence(),
                'observaciones' => fake()->sentence(),
                'institucionDependenciaId' => $institucion->id,
                'sancionadoId' => $particular->id,
                'sancionadoTipo' => 'App\Models\Particular',
                'tipoFaltaId' => $tiposFalta->random()->id,
                'responsableSancionId' => isset($responsableSancion) ? $responsableSancion->id : null,
            ]);

            //tipos de sancion
            for($h=0; $h<2; $h++){
                $tipoSancion = $tiposSancion->random();
                $sancion->tiposSancion()->attach($tipoSancion->id, [
                    'valor' => ($tipoSancion->clave == 'O') ? fake()->word() : $tipoSancion->valor,
                    'created_at' => now(),
                    'updated_at' => now(),
                ]);

                //crear multa
                if($tipoSancion->clave == 'M' || $sancion->id == 1){
                    Multa::create([
                        'monto' => rand(5000, 999999),
                        'monedaId' => rand(1, 3),
                        'sancionId' => $sancion->id,
                    ]);
                }

                //crear inhabilitacion
                if($tipoSancion->clave == 'I'|| $sancion->id == 1){
                    Inhabilitacion::create([
                        'plazo' => fake()->sentence(),
                        'fechaInicial' => Carbon::now()->subDays(rand(1, 100)),
                        'fechaFinal' => Carbon::now()->days(rand(1, 100)),
                        'sancionId' => $sancion->id,
                    ]);
                }
            }

            //apoderado legal
            if(!is_null($particular->apoderadosLegales->first())){
                $sancion->apoderadoLegal()->attach($particular->apoderadosLegales->first()->id, [
                    'created_at' => now(),
                    'updated_at' => now(),
                ]);
            }   

            //director general
            if(!is_null($particular->directoresGenerales->first())){
                $sancion->directorGeneral()->attach($particular->directoresGenerales->first()->id, [
                    'created_at' => now(),
                    'updated_at' => now(),
                ]);
            }

            //domicilio mexico
            if(!is_null($particular->domiciliosMexico->first())){
                $sancion->domicilioMexico()->attach($particular->domiciliosMexico->first()->id, [
                    'created_at' => now(),
                    'updated_at' => now(),
                ]);
            }

            //domicilio extranjero
            if(!is_null($particular->domiciliosExtranjero->first())){
                $sancion->domicilioExtranjero()->attach($particular->domiciliosExtranjero->first()->id, [
                    'created_at' => now(),
                    'updated_at' => now(),
                ]);
            }

            //resolución
            Resolucion::create([
                'sentido' => fake()->sentence(),
                'url' => fake()->url(),
                'fechaResolucion' => fake()->date(),
                'fechaNotificacion' => fake()->date(),
                'sancionId' => $sancion->id,
            ]);

            //documentos
            for($j= 0; $j<2; $j++){
                $tipoDocumento = $tiposDocumento->random();
                Documento::create([
                    'tipo' => $tipoDocumento->clave,
                    'titulo' => fake()->sentence(),
                    'descripcion' => fake()->sentence(),
                    'url' => fake()->url(),
                    'fecha' => fake()->date(),
                    'tipoDocumentoId' => $tipoDocumento->id,
                    'sancionId' => $sancion->id,
                ]);
            }
        }

        //sanciones de servidores públicos
        if(env('DB_PDE_ALLOW_CONNECTION', false)){
            for($i=0; $i<15; $i++){
                //institucion
                $institucion = $instituciones->random();
                $responsableSancion = null;
                $servidorPublico = null;
                
                //responsable sanción
                while(is_null($responsableSancion ?? null)){
                    $random = rand(596, 8891);
                    $responsableSancion = ServidorPublicoService::get_servidor_publico_institucion_dependencia($random);
                    if(is_null($responsableSancion)){
                        echo $random . "\n";
                    }
                };
                
                $responsableSancion = ResponsableSancion::create([
                    'nombres' => $responsableSancion['nombres'],
                    'primerApellido' => $responsableSancion['primerApellido'],
                    'segundoApellido' => $responsableSancion['segundoApellido'],
                    'institucionDependenciaId' => $responsableSancion['institucionDependenciaId'] ?? null,
                ]);
    
                //servidor público 
                while(is_null($servidorPublico ?? null)){
                    $random = rand(596, 8891);
                    $servidorPublico = ServidorPublicoService::get_servidor_publico_institucion_dependencia($random);
                    if(is_null($servidorPublico)){
                        echo $random . "\n";
                    }else{
                        //registrar servidor en la base de datos
                        $servidor = ServidorPublico::create([
                            'rfc' => $servidorPublico['rfc'],
                            'puesto' => fake()->word(),
                            'nivel' => fake()->word(),
                        ]);
                    }
                }
                
                //sanción
                $sancion = Sancion::create([
                    'fechaCaptura' => Carbon::now()->subDays(rand(0, 10)),
                    'expediente' => strtoupper(Str::random(10)),
                    'autoridadSancionadora' => $servidorPublico['institucionDependencia']['siglas'] ?? fake()->name,
                    'causaMotivoHechos' => fake()->sentence(),
                    'objetoContrato' => fake()->sentence(),
                    'acto' => fake()->sentence(),
                    'observaciones' => fake()->sentence(),
                    'institucionDependenciaId' => $servidorPublico['institucionDependencia']['id'] ?? null,
                    'sancionadoId' => $servidor['id'],
                    'sancionadoTipo' => 'App\Models\ServidorPublico',
                    'tipoFaltaId' => $tiposFalta->random()->id,
                    'responsableSancionId' => $responsableSancion['id'],
                ]);
    
                //tipos de sancion
                for($h=0; $h<2; $h++){
                    $tipoSancion = $tiposSancion->random();
                    $sancion->tiposSancion()->attach($tipoSancion->id, [
                        'valor' => ($tipoSancion->clave == 'O') ? fake()->word() : $tipoSancion->valor,
                        'created_at' => now(),
                        'updated_at' => now(),
                    ]);

                    //crear multa
                    if($tipoSancion->clave == 'M' || $sancion->id == 21){
                        Multa::create([
                            'monto' => rand(5000, 999999),
                            'monedaId' => rand(1, 3),
                            'sancionId' => $sancion->id,
                        ]);
                    }

                    //crear inhabilitacion
                    if($tipoSancion->clave == 'I' || $sancion->id == 21){
                        Inhabilitacion::create([
                            'plazo' => fake()->sentence(),
                            'fechaInicial' => Carbon::now()->subDays(rand(1, 100)),
                            'fechaFinal' => Carbon::now()->days(rand(1, 100)),
                            'sancionId' => $sancion->id,
                        ]);
                    }
                }
    
                //resolución
                Resolucion::create([
                    'sentido' => fake()->sentence(),
                    'url' => fake()->url(),
                    'fechaResolucion' => fake()->date(),
                    'fechaNotificacion' => fake()->date(),
                    'sancionId' => $sancion->id,
                ]);

                //documentos
                if($sancion->id < 31){
                    for($j= 0; $j<2; $j++){
                        $tipoDocumento = $tiposDocumento->random();
                        Documento::create([
                            'tipo' => $tipoDocumento->clave,
                            'titulo' => fake()->sentence(),
                            'descripcion' => fake()->sentence(),
                            'url' => fake()->url(),
                            'fecha' => fake()->date(),
                            'tipoDocumentoId' => $tipoDocumento->id,
                            'sancionId' => $sancion->id,
                        ]);
                    }
                }
            }
        }
    }
}
