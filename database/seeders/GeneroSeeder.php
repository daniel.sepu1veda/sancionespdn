<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Genero;

class GeneroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $generos = json_decode(file_get_contents(base_path("database/jsons/genders.json")), true);

        foreach ($generos as $genero) {
            Genero::create($genero);
        }
    }
}