<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\InstitucionDependencia;
use App\Models\User;

class InstitucionDependenciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $institucionesDependencias = json_decode(file_get_contents(base_path("database/jsons/institutions.json")), true);

        foreach ($institucionesDependencias as $institucionDependencia) {
            InstitucionDependencia::create($institucionDependencia);
        }

        //instituciones
        $institucionesDependencias = InstitucionDependencia::all();

        //usuarios
        $users = User::where('id', '<>', 1)->get();

        //relacionar usuarios con instituciones (para datos de pruebas)
        foreach($users as $user){
            $institucion = $institucionesDependencias->random();
            $user->institucionesDependencias()->attach($institucion->id, [
                'created_at' => now(),
                'updated_at' => now(),
            ]);
            //si no es administrador de ente, se relacionará otra institución
            if($user->roleId > 2){
                $institucion = $institucionesDependencias->random();
                $user->institucionesDependencias()->attach($institucion->id, [
                    'created_at' => now(),
                    'updated_at' => now(),
                ]);
            }
        }

        //relacionar admin con todos los entes
        $user = User::findOrFail(1);

        foreach($institucionesDependencias as $institucion){
            $user->institucionesDependencias()->attach($institucion->id, [
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }

    }
}