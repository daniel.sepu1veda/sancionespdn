<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Moneda;

class MonedaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $monedas = json_decode(file_get_contents(base_path("database/jsons/currencies.json")), true);

        foreach ($monedas as $moneda) {
            Moneda::create($moneda);
        }
    }
}
