<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class ResponsableSancion extends Model
{
    use HasFactory, SoftDeletes;

    //atributos
    protected $fillable = [
        'nombres',
        'primerApellido',
        'segundoApellido',
        'institucionDependenciaId',
    ];

    //atributos ocultos
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    //nombre de tabla
    protected $table = 'sanction_imposers';
    
    //atributos
    protected $appends = [
        'nombre_completo', 
    ];

    public function institucionDependencia()
    {
        return $this->belongsTo(InstitucionDependencia::class, 'institucionDependenciaId');
    }

    public function sanciones()
    {
        return $this->hasMany(Sancion::class, 'responsableSancionId');
    }

    public function nombreCompleto(): Attribute
    {
        return new Attribute(
            get: function() {
                 return $this->nombres . ' ' . $this->primerApellido . ' ' . $this->segundoApellido;
            },
        );
    }

    public function toArray()
    {
        $attributes = collect(parent::toArray())->mapWithKeys(function ($value, $key) {
            return [Str::camel($key) => $value];
        })->all();

        $appends = $this->appends;

        foreach ($appends as $append) {
            $attributes[Str::camel($append)] = $this->{$append};
        }

        return $attributes;
    }
}
