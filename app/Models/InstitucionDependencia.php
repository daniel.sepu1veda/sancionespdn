<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class InstitucionDependencia extends Model
{
    use HasFactory, SoftDeletes;

    //atributos
    protected $fillable = [
        'nombre',
        'siglas',
        'clave',
    ];

    //atributos ocultos
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    //nombre de tabla
    protected $table = 'institutions';

    public function usuarios()
    {
        return $this->belongsToMany(User::class, 'user_institution', 'institucionDependenciaId', 'userId');
    }

    public function sanciones()
    {
        return $this->hasMany(Sancion::class, 'institucionDependenciaId');
    }

    public function toArray()
    {
        return collect(parent::toArray())->mapWithKeys(function ($value, $key) {
            return [Str::camel($key) => $value];
        })->all();
    }
}
