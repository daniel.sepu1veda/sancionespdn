<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Models\Permission;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\Auth;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles, SoftDeletes;

    //atributos
    protected $fillable = [
        'nombres',
        'primerApellido',
        'segundoApellido',
        'rfc',
        'correo',
        'email',
        'password',
        'roleId',
        'firstLogin',
    ];

    //atributos protegidos
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_secret',
        'two_factor_recovery_codes',
        'two_factor_confirmed_at',
        'email_verified_at',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    //casts
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    //atributos
    protected $appends = [
        'nombre_completo', 
        'all_permissions',
    ];

    //relaciones
    public function institucionesDependencias()
    {
        return $this->belongsToMany(InstitucionDependencia::class, 'user_institution', 'userId', 'institucionDependenciaId');
    }

    public function rol()
    {
        return $this->belongsTo('Spatie\Permission\Models\Role', 'roleId');
    }

    public function logs()
    {
        return $this->hasMany(Log::class, 'userId');
    }

    public function notificaciones()
    {
        return $this->hasMany(Notificacion::class, 'destinatarioId');
    }

    public function nombreCompleto(): Attribute
    {
        return new Attribute(
            get: function() {
                 return $this->nombres . ' ' . $this->primerApellido . ' ' . $this->segundoApellido;
            },
        );
    }

    public function nombreInstitucion(): Attribute
    {
        return new Attribute(
            get: function() {
                if($this->institucionesDependencias->count() == 1){
                    return $this->institucionesDependencias->first()->nombre;
                }
                return ($this->institucionesDependencias->count() . ' Instituciones');
            },
        );
    }

    public function allPermissions(): Attribute
    {
        return new Attribute(
            get: function() {
                return auth()->check() ? $this->rol->permissions->pluck('name') : [];
            },
        );
    }
    
    public function toArray()
    {
        $attributes = collect(parent::toArray())->mapWithKeys(function ($value, $key) {
            return [Str::camel($key) => $value];
        })->all();

        $appends = $this->appends;

        foreach ($appends as $append) {
            $attributes[Str::camel($append)] = $this->{$append};
        }

        return $attributes;
    }
}
