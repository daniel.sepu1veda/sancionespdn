<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vialidad extends Model
{
    use HasFactory, SoftDeletes;

    //atributos
    protected $fillable = [
        'clave',
        'valor',
        'tipoVialidadId',
    ];

    //atributos ocultos
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    //nombre de tabla
    protected $table = 'roads';

    public function tipoVialidad()
    {
        return $this->belongsTo(TipoVialidad::class, 'tipoVialidadId');
    }
}
