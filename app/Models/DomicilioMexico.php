<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Services\DomicilioService;
use Illuminate\Support\Str;

class DomicilioMexico extends Model
{
    use HasFactory, SoftDeletes;

    //atributos
    protected $fillable = [
        'codigoPostal',
        'numeroExterior',
        'numeroInterior',
        'paisId',
        'entidadFederativaId',
        'municipioId',
        'localidadId',
        'vialidadId',
        'particularId',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $appends = [
        'pais', 
        'entidad_federativa', 
        'municipio', 
        'localidad'
    ];

    //nombre de tabla
    protected $table = 'mexican_addresses';

    public function pais(): Attribute
    {
        return new Attribute(
            get: function() {
                 return DomicilioService::get_pais($this->paisId);
            },
        );
    }

    public function entidadFederativa(): Attribute
    {
        return new Attribute(
            get: function() {
                 return DomicilioService::get_entidad_federativa($this->entidadFederativaId);
            },
        );
    }

    public function municipio(): Attribute
    {
        return new Attribute(
            get: function() {
                 return DomicilioService::get_municipio($this->municipioId);
            },
        );
    }

    public function localidad(): Attribute
    {
        return new Attribute(
            get: function() {
                 return DomicilioService::get_localidad($this->localidadId);
            },
        );
    }

    //relaciones
    public function vialidad()
    {
        return $this->belongsTo(Vialidad::class, 'vialidadId');
    }

    public function particular()
    {
        return $this->belongsTo(Particular::class, 'particularId');
    }   

    public function sanciones()
    {
        return $this->belongsToMany(DomicilioMexico::class, 'sanction_mexican_address', 'domicilioMexicoId', 'sancionId');
    }

    public function toArray()
    {
        $attributes = collect(parent::toArray())->mapWithKeys(function ($value, $key) {
            return [Str::camel($key) => $value];
        })->all();

        $appends = $this->appends;

        foreach ($appends as $append) {
            $attributes[Str::camel($append)] = $this->{$append};
        }

        return $attributes;
    }
}
