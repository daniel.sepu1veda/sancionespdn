<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoDocumento extends Model
{
    use HasFactory, SoftDeletes;

    //atributos
    protected $fillable = [
        'clave',
        'valor',
    ];

    //atributos ocultos
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    //nombre de tabla
    protected $table = 'document_types';
    
    public function documentos()
    {
        return $this->hasMany(Documento::class, 'tipoDocumentoId');
    }

}
