<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Sancion extends Model
{
    use HasFactory, SoftDeletes;
    
    //atributos
    protected $fillable = [
        'fechaCaptura',
        'expediente',
        'autoridadSancionadora',
        'causaMotivoHechos',
        'objetoContrato',
        'acto',
        'observaciones',
        'sancionadoTipo',
        'sancionadoId',
        'institucionDependenciaId',
        'tipoFaltaId',
        'responsableSancionId',
    ];

    //atributos ocultos
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    //nombre de tabla
    protected $table = 'sanctions';

    public function institucionDependencia()
    {
        return $this->belongsTo(InstitucionDependencia::class, 'institucionDependenciaId');
    }

    public function sancionado()
    {
        return $this->morphTo('sancionado', 'sancionadoTipo', 'sancionadoId');
    }

    public function tipoFalta()
    {
        return $this->belongsTo(TipoFalta::class, 'tipoFaltaId');
    }

    public function responsableSancion()
    {
        return $this->belongsTo(ResponsableSancion::class, 'responsableSancionId');
    }

    public function tiposSancion()
    {
        return $this->belongsToMany(TipoSancion::class, 'sanction_sanction_type', 'sancionId', 'tipoSancionId')->withPivot('valor');
    }

    public function domicilioMexico()
    {
        return $this->belongsToMany(DomicilioMexico::class, 'sanction_mexican_address', 'sancionId', 'domicilioMexicoId');
    }

    public function domicilioExtranjero()
    {
        return $this->belongsToMany(DomicilioExtranjero::class, 'sanction_foreign_address', 'sancionId', 'domicilioExtranjeroId');
    }

    public function apoderadoLegal()
    {
        return $this->belongsToMany(ApoderadoLegal::class, 'sanction_legal_representative', 'sancionId', 'apoderadoLegalId');
    }

    public function directorGeneral()
    {
        return $this->belongsToMany(DirectorGeneral::class, 'sanction_general_director', 'sancionId', 'directorGeneralId');
    }

    public function resolucion()
    {
        return $this->hasOne(Resolucion::class, 'sancionId');
    }

    public function inhabilitacion()
    {
        return $this->hasOne(Inhabilitacion::class, 'sancionId');
    }

    public function multa()
    {
        return $this->hasOne(Multa::class, 'sancionId');
    }

    public function documentos()
    {
        return $this->hasMany(Documento::class, 'sancionId');
    }

    public function toArray()
    {
        return collect(parent::toArray())->mapWithKeys(function ($value, $key) {
            return [Str::camel($key) => $value];
        })->all();
    }
}
