<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Constancia extends Model
{
    use HasFactory, SoftDeletes;

    //atributos
    protected $fillable = [
        'folio',
        'rfc',
        'sancionado',
        'constanciableTipo',
        'constanciableId',
        'userId',
    ];

    //nombre de tabla
    protected $table = 'certificates';

    public function constanciable()
    {
        return $this->morphTo('constanciable', 'constanciableTipo', 'constanciableId');
    }

    public function usuario()
    {
        return $this->belongsTo(User::class, 'usuarioId');
    }
    
    public function url(): Attribute
    {
        return new Attribute(
            get: function() {
                 return env('APP_URL', 'http://127.0.0.1:8000') . '/constancias/' . $this->folio;
            },
        );
    }
}
