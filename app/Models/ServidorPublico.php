<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Services\ServidorPublicoService;
use Illuminate\Support\Str;

class ServidorPublico extends Model
{
    use HasFactory, SoftDeletes;

    //atributos
    protected $fillable = [
        'rfc',
        'nombres',
        'primerApellido',
        'segundoApellido',
        'puesto',
        'nivel',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    //nombre de tabla
    protected $table = 'public_servants';

    public function sanciones()
    {
        return $this->morphMany(Sancion::class, 'sancionado', 'sancionadoTipo', 'sancionadoId');
    }

    public function nombreCompleto(): Attribute
    {
        return new Attribute(
            get: function() {
                 return $this->nombres . ' ' . $this->primerApellido . ' ' . $this->segundoApellido;
            },
        );
    }

    public function toArray()
    {
        return collect(parent::toArray())->mapWithKeys(function ($value, $key) {
            return [Str::camel($key) => $value];
        })->all();
    }
}
