<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Notificacion extends Model
{
    use HasFactory, SoftDeletes;

    //atributos
    protected $fillable = [
        'descripcion',
        'texto',
        'url',
        'usuarioId',
        'destinatarioId',
        'estadoNotificacionId'
    ];

    //atributos ocultos
    protected $hidden = [
        'updated_at',
        'deleted_at',
    ];

    //nombre de tabla
    protected $table = 'notifications';

    public function usuario()
    {
        return $this->belongsTo(User::class, 'usuarioId');
    }

    public function destinatario()
    {
        return $this->belongsTo(User::class, 'destinatarioId');
    }

    public function estadoNotificacion()
    {
        return $this->belongsTo(EstadoNotificacion::class, 'estadoNotificacionId');
    }

    public function toArray()
    {
        return collect(parent::toArray())->mapWithKeys(function ($value, $key) {
            return [Str::camel($key) => $value];
        })->all();
    }
}
