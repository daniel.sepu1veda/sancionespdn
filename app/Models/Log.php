<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Log extends Model
{
    use HasFactory, SoftDeletes;

    //atributos
    protected $fillable = [
        'descripcion',
        'accion',
        'modulo',
        'logeableTipo',
        'logeableId',
        'userId',
    ];

    public function logeable()
    {
        return $this->morphTo('logeable', 'logeableTipo', 'logeableId');
    }

    public function usuario()
    {
        return $this->belongsTo(User::class, 'userId');
    }
}
