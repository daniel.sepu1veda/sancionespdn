<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Resolucion extends Model
{
    use HasFactory, SoftDeletes;

    //atributos
    protected $fillable = [
        'sentido',
        'url',
        'fechaResolucion',
        'fechaNotificacion',
        'sancionId'
    ];

    //atributos ocultos
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    //nombre de tabla
    protected $table = 'resolutions';

    public function sancion()
    {
        return $this->belongsTo(Sancion::class, 'sancionId');
    }
}
