<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Documento extends Model
{
    use HasFactory, SoftDeletes;

    //atributos
    protected $fillable = [
        'tipo',
        'titulo',
        'descripcion',
        'url',
        'fecha',
        'tipoDocumentoId',
        'sancionId',
    ];

    //atributos ocultos
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    //nombre de tabla
    protected $table = 'documents';

    public function tipoDocumento()
    {
        return $this->belongsTo(TipoDocumento::class, 'tipoDocumentoId');
    }

    public function sancion()
    {
        return $this->belongsTo(Sancion::class, 'sancionId');
    }

    public function toArray()
    {
        return collect(parent::toArray())->mapWithKeys(function ($value, $key) {
            return [Str::camel($key) => $value];
        })->all();
    }
}
