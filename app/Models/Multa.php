<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Multa extends Model
{
    use HasFactory, SoftDeletes;

    //atributos
    protected $fillable = [
        'monto',
        'monedaId',
        'sancionId'
    ];

    //atributos ocultos
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    //nombre de tabla
    protected $table = 'fines';

    public function moneda()
    {
        return $this->belongsTo(Moneda::class, 'monedaId');
    }

    public function sancion()
    {
        return $this->belongsTo(Sancion::class, 'sancionId');
    }
}
