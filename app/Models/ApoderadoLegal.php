<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class ApoderadoLegal extends Model
{
    use HasFactory, SoftDeletes;

    //atributos
    protected $fillable = [
        'nombres',
        'primerApellido',
        'segundoApellido',
        'curp',
        'rfc',
        'particularId',
    ];

    //atributos ocultos
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    //nombre de tabla
    protected $table = 'legal_representatives';

    //atributos
    protected $appends = [
        'nombre_completo', 
    ];

    public function particular()
    {
        return $this->belongsTo(Particular::class, 'particularId');
    }

    public function sancion()
    {
        return $this->belongsToMany(Sancion::class, 'sanction_legal_representative', 'apoderadoLegalId', 'sancionId');
    }

    public function nombreCompleto(): Attribute
    {
        return new Attribute(
            get: function() {
                 return $this->nombres . ' ' . $this->primerApellido . ' ' . $this->segundoApellido;
            },
        );
    }

    public function tipo(): Attribute
    {
        return new Attribute(
            get: function() {
                 return get_class($this);
            },
        );
    }

    public function toArray()
    {
        return collect(parent::toArray())->mapWithKeys(function ($value, $key) {
            return [Str::camel($key) => $value];
        })->all();
    }
}
