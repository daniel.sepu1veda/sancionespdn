<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Particular extends Model
{
    use HasFactory, SoftDeletes;

    //atributos
    protected $fillable = [
        'nombreRazonSocial',
        'objetoSocial',
        'rfc',
        'curp',
        'telefono',
        'tipoPersonaId',
    ];

    //atributos ocultos
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    //nombre de tabla
    protected $table = 'individuals';
    
    //atributos
    protected $appends = [
        'nombre_completo', 
    ];

    public function tipoPersona()
    {
        return $this->belongsTo(TipoPersona::class,  'tipoPersonaId');
    }

    public function domiciliosMexico()
    {
        return $this->hasMany(DomicilioMexico::class, 'particularId');
    }

    public function domiciliosExtranjero()
    {
        return $this->hasMany(DomicilioExtranjero::class, 'particularId');
    }

    public function apoderadosLegales()
    {
        return $this->hasMany(ApoderadoLegal::class, 'particularId');
    }

    public function directoresGenerales()
    {
        return $this->hasMany(DirectorGeneral::class, 'particularId');
    }

    public function sanciones()
    {
        return $this->morphMany(Sancion::class, 'sancionado', 'sancionadoTipo', 'sancionadoId');
    }

    public function nombreCompleto(): Attribute
    {
        return new Attribute(
            get: function() {
                 return $this->nombreRazonSocial;
            },
        );
    }

    public function tipo(): Attribute
    {
        return new Attribute(
            get: function() {
                 return get_class($this);
            },
        );
    }

    public function toArray()
    {
        return collect(parent::toArray())->mapWithKeys(function ($value, $key) {
            return [Str::camel($key) => $value];
        })->all();
    }
}
