<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Services\DomicilioService;

class DomicilioExtranjero extends Model
{
    use HasFactory, SoftDeletes;

    //atributos
    protected $fillable = [
        'calle',
        'numeroExterior',
        'numeroInterior',
        'ciudadLocalidad',
        'estadoProvincia',
        'codigoPostal',
        'paisId',
        'particularId',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $appends = [
        'pais'
    ];

    //nombre de la tabla
    protected $table = 'foreign_addresses';

    //atributos
    public function pais(): Attribute
    {
        return new Attribute(
            get: function() {
                return DomicilioService::get_pais($this->paisId);
            },
        );
    }

    //relaciones
    public function particular()
    {
        return $this->belongsTo(Particular::class, 'particularId');
    }

    public function sanciones()
    {
        return $this->belongsToMany(Sancion::class, 'sanction_foreign_address', 'domicilioExtranjeroId', 'sancionId');
    }
}
