<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoSancion extends Model
{
    use HasFactory, SoftDeletes;

    //atributos
    protected $fillable = [
        'clave',
        'valor',
    ];

    //atributos ocultos
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    //nombre de tabla
    protected $table = 'sanction_types';

    public function sanciones()
    {
        return $this->belongsToMany(Sancion::class, 'sanction_sanction_type', 'tipoSancionId', 'sancionId');
    }
}
