<?php
namespace App\Helpers;

use App\Models\Notificacion;

class AppHelper 
{
    public static function get_notificaciones()
    {
        if(auth()->check()){
            $notificaciones = Notificacion::select('id', 'descripcion', 'destinatarioId', 'estadoNotificacionId', 'created_at')
                                        ->where('estadoNotificacionId', 1)
                                        ->where('destinatarioId', auth()->user()->id)
                                        ->orderBy('created_at', 'DESC')
                                        ->take(3)
                                        ->get();

            return $notificaciones;
        }
        return [];
    }

    public static function get_notificaciones_count()
    {
        if(auth()->check()){
            $notificacionesCount = Notificacion::select('id', 'descripcion', 'destinatarioId', 'estadoNotificacionId')
                                            ->where('estadoNotificacionId', 1)
                                            ->where('destinatarioId', auth()->user()->id)
                                            ->count();

            return $notificacionesCount;
        }
        return null;
    }
}