<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithProperties;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeSheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use App\Traits\ExportStyleTrait;
use Illuminate\Http\Request;
use App\Models\Sancion;
use Carbon\Carbon;

class SancionServidorPublicoExport implements FromQuery, WithMapping, WithProperties, ShouldAutoSize, WithStyles,
    WithHeadings, WithEvents, WithDrawings
{
    use ExportStyleTrait;

    public $request;
    protected $catalogos;

    public function __construct(Request $request)
    {
        $this->request = $request;

        //renglones inicial y final
        $start_row = 7;
        $end_row = 27;

        //catálogos
        $this->catalogos = [
            [
                'model' => 'App\Models\InstitucionDependencia',
                'start_cell' => 'D' . $start_row,
                'end_cell' => 'D' . $end_row,
                'key' => 'siglas',
            ],
            [
                'model' => 'App\Models\TipoFalta',
                'start_cell' => 'I' . $start_row,
                'end_cell' => 'I' . $end_row,
                'key' => 'clave',
            ],
            [
                'model' => 'App\Models\Moneda',
                'start_cell' => 'Q' . $start_row,
                'end_cell' => 'Q' . $end_row,
                'key' => 'valor',
            ],
            [
                'model' => 'App\Models\TipoDocumento',
                'start_cell' => 'R' . $start_row,
                'end_cell' => 'R' . $end_row,
                'key' => 'valor',
            ],
        ];
    }

    public function query()
    {
        return Sancion::where('sancionadoTipo', 'App\Models\ServidorPublico')
                    ->with('institucionDependencia');
    }

    public function headings(): array
    {
        return [
            'RFC del Servidor Público',
            'Puesto del Servidor Público',
            'Nivel del Servidor Público',
            'Institución del Servidor Público',
            'Expediente',
            'Autoridad Sancionadora',
            'Causa/Motivo/Hechos',
            'Observaciones',
            'Tipo de Falta',
            'Tipos de Sanción',
            'Fecha de la Resolución',
            'URL de la Resolución',
            'Plazo de Inhabilitación',
            'Fecha Inicial Inhabilitación',
            'Fecha Final Inhabilitación',
            'Monto Multa',
            'Moneda Multa',
            'Tipo de Documento Adicional',
            'Título de Documento Adicional',
            'Descripción de Documento Adicional',
            'URL de Documento Adicional',
            'Fecha de Documento Adicional',
        ];
    }

    public function map($sancion): array
    {
        if(isset($this->request->cargaMasiva)){
            return [];
        }
        return [
            $sancion->sancionado->rfc ?? '',
            $sancion->sancionado->puesto ?? '',
            $sancion->sancionado->nivel ?? '',
            $sancion->institucionDependencia->siglas ?? '',
            $sancion->expediente ?? '',
            $sancion->autoridadSancionadora ?? '',
            $sancion->causaMotivoHechos ?? '',
            $sancion->observaciones ?? '',
            $sancion->tipoFalta->clave ?? '',
            $this->get_tipos_sancion($sancion->tiposSancion) ?? '',
            $sancion->resolucion->fechaResolucion ?? '',
            $sancion->resolucion->url ?? '',
            $sancion->inhabilitacion->plazo ?? '',
            $sancion->inhabilitacion->fechaInicial ?? '',
            $sancion->inhabilitacion->fechaFinal ?? '',
            $sancion->multa->monto ?? '',
            $sancion->multa->moneda->valor ?? '',
            $sancion->documentos->first()->tipoDocumento->valor ?? '',
            $sancion->documentos->first()->titulo ?? '',
            $sancion->documentos->first()->descripcion ?? '',
            $sancion->documentos->first()->url ?? '',
            $sancion->documentos->first()->fecha ?? '',
        ];
    }

    public function drawings()
    {
        return $this->setDrawings();
    }

    public function properties(): array
    {
        return [
            'creator'        => 'Sistemas',
            'title'          => 'Reporte de Sanciones',
            'description'    => 'Sanciones',
            'subject'        => 'Sanciones',
            'category'       => 'Sanciones',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        $this->applyStyles($sheet);
    }

    public function registerEvents(): array
    {
        return $this->setEvents('Sanciones de Servidores Públicos', $this->catalogos);
    }

    public function get_tipos_sancion($tiposSancion)
    {
        $claves = [];
        $totalTipos = count($tiposSancion);
        foreach ($tiposSancion as $key => $tipo) {
            $claves[] = $tipo['clave'];
            if ($key < $totalTipos - 1) {
                $claves[] = ', ';
            }
        }
        return implode('', $claves);
    }
}
