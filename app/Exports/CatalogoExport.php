<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithProperties;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeSheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use App\Traits\ExportStyleTrait;
use Illuminate\Http\Request;


class CatalogoExport implements FromQuery, WithMapping, WithProperties, ShouldAutoSize, WithStyles,
    WithHeadings, WithEvents, WithDrawings
{
    use ExportStyleTrait;

    private $model;
    private $fields;
    private $headings;
    private $title;

    public function __construct(Request $request)
    {
        $this->model = $request->model;
        $this->fields = $request->fields;
        $this->headings = $request->headings;
        $this->title = $request->title;
    }

    public function query()
    {
        return $this->model::select($this->fields);
    }

    public function headings(): array
    {
        return $this->headings;
    }

    public function map($item): array
    {
        $array = array();

        foreach ($this->fields as $field) {
            $array[$field] = $item->{$field};
        }

        return $array;

    }

    public function drawings()
    {
        return $this->setDrawings();
    }

    public function properties(): array
    {
        return [
            'creator'        => 'Sistemas',
            'title'          => $this->title,
            'description'    => 'Catálogos del sistema',
            'subject'        => 'Catálogos',
            'category'       => 'Catálogos',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        $this->applyStyles($sheet);
    }

    public function registerEvents(): array
    {
        return $this->setEvents('Catálogo de ' . $this->title);
    }
}
