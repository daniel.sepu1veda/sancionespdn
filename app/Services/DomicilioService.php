<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use App\Models\EntidadFederativa;
use App\Models\Municipio;

class DomicilioService
{
    public static function get_entidad_federativa($id)
	{
        if(env('DB_PDE_ALLOW_CONNECTION', false)){
            //obtener entidad federativa de la base de datos
            $entidadFederativa = DB::connection('pgsql_pde')
                        ->table('declaradb.globales_estados')
                        ->where('id', $id)
                        ->first();

            //formato
            if($entidadFederativa){
                $entidadFederativa = [
                    'id' => $entidadFederativa->id,
                    'clave' =>  $entidadFederativa->clave,
                    'valor' => mb_strtoupper($entidadFederativa->nombre, 'UTF-8'),
                ];
            }
            return $entidadFederativa;
        }
        return null;
	}

    public static function get_municipio($id)
	{
        if(env('DB_PDE_ALLOW_CONNECTION', false)){
            //obtener municipio de la base de datos
            $municipio = DB::connection('pgsql_pde')
                        ->table('declaradb.globales_municipios')
                        ->where('id', $id)
                        ->first();

            //formato
            if($municipio){
                $municipio = [
                    'id' => $municipio->id,
                    'clave' =>  $municipio->clave,
                    'valor' => mb_strtoupper($municipio->nombre, 'UTF-8'),
                ];
            }
            return $municipio;
        }
        return null;
	}

    public static function get_municipio_by_clave($municipioClave, $entidadFederativaId)
	{
        if(env('DB_PDE_ALLOW_CONNECTION', false)){
            //obtener municipio de la base de datos
            $municipio = DB::connection('pgsql_pde')
                        ->table('declaradb.globales_municipios')
                        ->where('id_estado', $entidadFederativaId)
                        //id_municipio (municipioClave) es entero, clave es string, por eso se realiza una conversión
                        //demuestra inconsistencia en la estructura de la base de datos del S1 ya que el campo no debería llamarse de esa manera
                        ->where(DB::raw('CAST(clave AS INTEGER)'), $municipioClave)
                        ->first();

            //formato
            if($municipio){
                $municipio = [
                    'id' => $municipio->id,
                    'clave' =>  $municipio->clave,
                    'valor' => mb_strtoupper($municipio->nombre, 'UTF-8'),
                ];
            }
            return $municipio;
        }
        return null;
	}

    public static function get_pais($id)
	{   
        if(env('DB_PDE_ALLOW_CONNECTION', false)){
            //obtener entidad federativa de la base de datos
            $pais = DB::connection('pgsql_pde')
                        ->table('declaradb.globales_paises')
                        ->where('id_pais', $id)
                        ->first();

            //formato
            if($pais){
                $pais = [
                    'id' => $pais->id_pais,
                    'clave' =>  $pais->abr2,
                    'valor' => mb_strtoupper($pais->nombre, 'UTF-8'),
                ];
            }
            return $pais;
        }
        return null;
	}

    public static function get_localidad($id)
	{
        if(env('DB_PDE_ALLOW_CONNECTION', false)){
            //obtener localidad de la base de datos
            $localidad = DB::connection('pgsql_pde')
                        ->table('declaradb.globales_cp_sepomex')
                        ->where('id', $id)
                        ->first();

            //formato
            if($localidad){
                $localidad = [
                    'id' => $localidad->id,
                    'clave' =>  $localidad->id,
                    'valor' => mb_strtoupper($localidad->asentamiento, 'UTF-8'),
                    'entidadFederativaId' => $localidad->id_estado,
                    'municipioClave' => $localidad->id_municipio,
                ];
            }
            return $localidad;
        }
        return null;
	}

    public static function get_localidades_by_cp($cp)
	{
        if(env('DB_PDE_ALLOW_CONNECTION', false) && isset($cp)){
            //obtener localidades por codigo postal
            $data = DB::connection('pgsql_pde')
                    ->table('declaradb.globales_cp_sepomex')
                    ->where('cp', $cp)
                    ->get();
            
            if(sizeof($data) > 0){
                //arreglo con todas las localidades con formato
                $localidades = $data->map(function($item, $key) {
                    return [
                        'id' => $item->id,
                        'clave' => $item->id,
                        'valor' => mb_strtoupper($item->asentamiento, 'UTF-8'),
                        'cp' => $item->cp,
                        'entidadFederativaId' => $item->id_estado,
                        'municipioClave' => $item->id_municipio,
                    ];
                });

                //obtener estado
                $entidadFederativa = DomicilioService::get_entidad_federativa($data->first()->id_estado);

                //obtener municipio
                $municipio = DomicilioService::get_municipio_by_clave($data->first()->id_municipio, $data->first()->id_estado);

                //formato
                $domicilio = [
                    'entidadFederativa' => $entidadFederativa,
                    'municipio' => $municipio,
                    'localidades' => $localidades,
                ];
                return $domicilio;
            }
        }
        return null;
	}
}