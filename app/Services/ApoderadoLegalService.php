<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\ApoderadoLegal;

class ApoderadoLegalService
{   
    public static function get_by_nombres(Request $request)
    {
        $apoderadosLegales = ApoderadoLegal::when(isset($request->nombres), function($q) use($request){
                                $q->where('nombres', 'LIKE', '%' . $request->nombres . '%');
                            })
                            ->when(isset($request->primerApellido), function($q) use($request){
                                $q->where('primerApellido', 'LIKE', '%' . $request->primerApellido . '%');
                            })
                            ->when(isset($request->segundoApellido), function($q) use($request){
                                $q->where('segundoApellido', 'LIKE', '%' . $request->segundoApellido . '%');
                            })
                            ->when(isset($request->rfc), function($q) use($request){
                                $q->where('rfc', $request->rfc);
                            })
                            ->when(isset($request->curp), function($q) use($request){
                                $q->where('curp', $request->curp);
                            })
                            ->when(isset($request->nombreRazonSocial), function($q) use($request){
                                $q->where('nombres', 'LIKE', '%' . $request->nombreRazonSocial . '%');
                            })
                            ->orderBy('id')
                            ->get()->append('tipo');

        return $apoderadosLegales;
    }
}
