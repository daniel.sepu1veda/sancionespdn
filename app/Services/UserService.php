<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\User;
use Carbon\Carbon;

class UserService
{   
    //servicio para generar nombre de usuario
    public static function generate_username(User $user)
	{
        $exists = true;

        while($exists){
            //obtener nombre
            $nombre = (isset($user->primerApellido) ? $user->primerApellido[0] : 'X'). $user->nombres[0];
            $username = 'AMD.S3@' . $nombre . fake()->randomNumber(2);

            //validar si existe el nombre de usuario
            $exists = User::where('email', $username)->exists();
        }

        return $username;
    }

    //servicio para generar contraseña
    public static function generate_password(User $user)
	{
        //obtener 
        $nombre = (isset($user->primerApellido) ? $user->primerApellido[0] : ''). $user->nombres[0];
        $institucion = $user->institucionesDependencias->first()->siglas ?? 'XX';
        
        //fecha
        $password = 'ADMIN' . $nombre . substr($institucion, 0, 2) . substr(microtime(), 18, 20);

        return $password;
    }
}
