<?php

namespace App\Services;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Services\DirectorGeneralService;
use App\Services\ApoderadoLegalService;
use App\Services\DomicilioService;
use App\Models\DomicilioExtranjero;
use App\Models\DomicilioMexico;
use App\Models\DirectorGeneral;
use App\Models\ApoderadoLegal;
use App\Models\Particular;
use App\Models\Localidad;
use App\Models\Vialidad;
use App\Models\Sancion;

class ParticularService
{
    public static function get_particular_by_rfc($rfc)
    {
        $particular = Particular::with('tipoPersona', 'apoderadosLegales', 'directoresGenerales')
                                ->with('domiciliosMexico', function ($q){
                                    $q->with('vialidad');
                                }) 
                                ->with('domiciliosExtranjero')
                                ->where('rfc', $rfc)
                                ->first();

        //asignar primeros elementos al particular
        $particular['domicilioMexico'] = $particular->domiciliosMexico->first();
        $particular['domicilioExtranjero'] = $particular->domiciliosExtranjero->first();
        $particular['apoderadoLegal'] = $particular->apoderadosLegales->first();
        $particular['directorGeneral'] = $particular->directoresGenerales->first();

        unset($particular->domiciliosMexico);
        unset($particular->domiciliosExtranjero);
        unset($particular->apoderadosLegales);
        unset($particular->directoresGenerales);

        return $particular;
    }

    public static function get_by_nombres(Request $request)
    {
        $particulares = Particular::query()
                                ->when(isset($request->nombres), function($q) use($request){
                                    $q->where('nombreRazonSocial', 'LIKE', '%' . $request->nombres . '%');
                                })
                                ->when(isset($request->primerApellido), function($q) use($request){
                                    $q->where('nombreRazonSocial', 'LIKE', '%' . $request->primerApellido . '%');
                                })
                                ->when(isset($request->segundoApellido), function($q) use($request){
                                    $q->where('nombreRazonSocial', 'LIKE', '%' . $request->segundoApellido . '%');
                                })
                                ->when(isset($request->rfc), function($q) use($request){
                                    $q->where('rfc', $request->rfc);
                                })
                                ->when(isset($request->curp), function($q) use($request){
                                    $q->where('curp', $request->curp);
                                })
                                ->when(isset($request->nombreRazonSocial), function($q) use($request){
                                    $q->orWhere('nombreRazonSocial', 'LIKE', '%' . $request->nombreRazonSocial . '%');
                                })
                                ->orderBy('id')
                                ->get()->append('tipo');

        return $particulares;
    }

    public static function store_sancion(Request $request, Sancion $sancion)
    {
        //registrar nuevo particular
        $particular = is_null($request->sancionadoId)
                ? ParticularService::store(new Request($request->sancionado))
                : Particular::with('domiciliosMexico', 'domiciliosExtranjero')->findOrFail($request->sancionadoId);

        //relación con domicilio
        if(isset($request->sancionado['domicilioMexico'])){
            if(isset($request->sancionado['domicilioMexico']['id'])){
                $sancion->domicilioMexico()->attach($request->sancionado['domicilioMexico']['id'], [
                    'created_at' => now(),
                    'updated_at' => now()
                ]);
            }else{
                if(isset($particular->domicilioMexico)){
                    $sancion->domicilioMexico()->attach($particular->domicilioMexico->id, [
                        'created_at' => now(),
                        'updated_at' => now()
                    ]);
                }
            }
        }

        //relación con domicilio extranjero
        if(isset($request->sancionado['domicilioExtranjero'])){
            if(isset($request->sancionado['domicilioExtranjero']['id'])){
                $sancion->domicilioExtranjero()->attach($request->sancionado['domicilioExtranjero']['id'], [
                    'created_at' => now(),
                    'updated_at' => now()
                ]);
            }else{
                if(isset($particular->domicilioExtranjero)){
                    $sancion->domicilioExtranjero()->attach($particular->domicilioExtranjero->id, [
                        'created_at' => now(),
                        'updated_at' => now()
                    ]);
                }
            }
        }

        //apoderado legal
        if(isset($request->sancionado['apoderadoLegal'])){
            if(isset($request->sancionado['apoderadoLegal']['id'])){
                $sancion->apoderadoLegal()->attach($request->sancionado['apoderadoLegal']['id'], [
                    'created_at' => now(),
                    'updated_at' => now()
                ]);
            }else{
                if(isset($particular->apoderadoLegal)){
                    $sancion->apoderadoLegal()->attach($particular->apoderadoLegal->id, [
                        'created_at' => now(),
                        'updated_at' => now()
                    ]);
                }
            }
        }

        //directorGeneral
        if(isset($request->sancionado['directorGeneral'])){
            if(isset($request->sancionado['directorGeneral']['id'])){
                $sancion->directorGeneral()->attach($request->sancionado['directorGeneral']['id'], [
                    'created_at' => now(),
                    'updated_at' => now()
                ]);
            }else{
                if(isset($particular->directorGeneral)){
                    $sancion->directorGeneral()->attach($particular->directorGeneral->id, [
                        'created_at' => now(),
                        'updated_at' => now()
                    ]);
                }
            }
        }

        //relacion con particular
        $sancion->sancionadoId = $particular->id;
        $sancion->save();
    }

	public static function store(Request $request)
	{
        //crear particular
        $particular = Particular::create($request->all());

        //domicilio mexico
        if($request->has('domicilioMexico')){
            $domicilio = DomicilioMexico::create($request->domicilioMexico);

            //vialidad
            if(isset($request->domicilioMexico['vialidad'])){
                $vialidad = Vialidad::create($request->domicilioMexico['vialidad']);
                $domicilio->vialidadId = $vialidad->id;
            }

            //asignar relaciones
            $domicilio->particularId = $particular->id;
            $domicilio->save();

            //agregarlo al particular
            $particular->domicilioMexico = $domicilio;
        } 

        //domicilio extranjero
        if($request->has('domicilioExtranjero')){
            $domicilio = DomicilioExtranjero::create($request->domicilioExtranjero);
            $domicilio->particularId = $particular->id;
            $domicilio->save();

            //agregarlo al particular
            $particular->domicilioExtranjero = $domicilio;
        }

        //apoderado legal
        if($request->has('apoderadoLegal')){
            $apoderadoLegal = ApoderadoLegal::create($request->apoderadoLegal);
            $apoderadoLegal->particularId = $particular->id;
            $apoderadoLegal->save();

            //agregarlo al particular
            $particular->apoderadoLegal = $apoderadoLegal;
        }

        //director general
        if($request->has('directorGeneral')){
            $directorGeneral = DirectorGeneral::create($request->directorGeneral);
            $directorGeneral->particularId = $particular->id;
            $directorGeneral->save();

            //agregarlo al particular
            $particular->directorGeneral = $directorGeneral;
        }
        
        return $particular;
    }

    public static function update(Request $request)
    {
        //obtener particular
        $particular = Particular::with('domiciliosMexico', 'domiciliosExtranjero', 'sanciones')->findOrFail($request->id);

        //actualizar particular
        $particular->update($request->all());

        //actualizar o crear domicilio mexico
        if(isset($request->domiciliosMexico)){
            foreach($request->domiciliosMexico as $domicilioMexico){
                $domicilio = DomicilioMexico::updateOrCreate(
                    ['id' => $domicilioMexico['id'] ?? null],
                    $domicilioMexico
                );
                
                //actualizar o crear localidad
                if(isset($domicilioMexico['localidad'])){
                    $localidad = Localidad::updateOrCreate(
                        ['id' => $domicilioMexico['localidad']['id'] ?? null], 
                        $domicilioMexico['localidad'] 
                    );
                    $domicilio->localidadId = $localidad->id;
                }
                
                //actualizar o crear vialidad
                if(isset($domicilioMexico['vialidad'])){
                    $vialidad = Vialidad::updateOrCreate(
                        ['id' => $domicilioMexico['vialidad']['id'] ?? null], 
                        $domicilioMexico['vialidad'] 
                    );
                    $domicilio->vialidadId = $vialidad->id;
                }
                
                //guardar nuevas relaciones
                $domicilio->save();
            }
        } 

        //actualizar o crear domicilio extranjero
        if(isset($request->domiciliosExtranjero)){
            foreach($request->domiciliosExtranjero as $domicilioExtranjero){
                $domicilio = DomicilioExtranjero::updateOrCreate(
                    ['id' => $domicilioExtranjero['id'] ?? null],
                    $domicilioExtranjero
                );
            }
        }
    }

    public static function get_fake_data()
    {
        $particular = [
            'nombreRazonSocial' => fake()->name(),
            'objetoSocial' => fake()->sentence(),
            'rfc' => strtoupper(Str::random(13)),
            'telefono' => fake()->phoneNumber(),
            'tipoPersonaId' => rand(1,2),
            'domicilioMexico' => [
                'codigoPostal' => fake()->postcode(),
                'numeroExterior' => fake()->randomNumber(3),
                'numeroInterior' => fake()->randomNumber(3),
                'paisId' => 142, 
                'entidadFederativaId' => 3,
                'municipioId' => 18,
                'localidad' => [
                    'clave' => fake()->randomNumber(4),
                    'valor' => fake()->city(),
                ],
                'vialidad' => [
                    'clave' => rand(0, 1) > 0 ? 'CALLE' : 'AVENIDA',
                    'valor' => fake()->city(),
                ],
            ],
            'domicilioExtranjero' => [
                'calle' => fake()->name(),
                'numeroExterior' => fake()->randomNumber(3),
                'numeroInterior' => fake()->randomNumber(3),
                'ciudadLocalidad' => fake()->city(),
                'estadoProvincia' => fake()->state(),
                'codigoPostal' => fake()->postcode(),
                'paisId' => rand(1, 243),
            ],
            'apoderadoLegal' => [
                'nombres' => fake()->name(),
                'primerApellido' => fake()->lastname(),
                'segundoApellido' => fake()->lastname(),
                'curp' => strtoupper(Str::random(18)),
            ],
            'directorGeneral' => [
                'nombres' => fake()->name(),
                'primerApellido' => fake()->lastname(),
                'segundoApellido' => fake()->lastname(),
                'curp' => strtoupper(Str::random(18)),
            ],
        ];

        return $particular;
    }

    public static function get_particular_sancion(Sancion $sancion)
    {
        //cargar relaciones
        $sancion->load('sancionado.tipoPersona');
        $sancion->load('domicilioMexico.vialidad');
        $sancion->load('domicilioExtranjero');
        $sancion->load('apoderadoLegal');
        $sancion->load('directorGeneral');

        //agregar relaciones al sancionado
        $sancion['sancionado']['domicilioMexico'] = $sancion->domicilioMexico->first();
        $sancion['sancionado']['domicilioExtranjero'] = $sancion->domicilioExtranjero->first();
        $sancion['sancionado']['apoderadoLegal'] = $sancion->apoderadoLegal->first();
        $sancion['sancionado']['directorGeneral'] = $sancion->directorGeneral->first();

        //quitar relaciones con sanción
        unset($sancion->domicilioMexico);
        unset($sancion->domicilioExtranjero);
        unset($sancion->apoderadoLegal);
        unset($sancion->directorGeneral);
        
        return $sancion;
    }

    public static function get_localidades_particular(Particular $particular)
    {
        //declarar localidades
        $localidades = [];

        //obtener localidades
        if(isset($particular->domicilioMexico) && isset($particular->domicilioMexico->codigoPostal)){
            $localidades = DomicilioService::get_localidades_by_cp(intval($particular->domicilioMexico->codigoPostal ?? 0));
            $localidades = $localidades['localidades'] ?? [];
        }
        
        return $localidades;
    }
}
