<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Collection;
use App\Services\ParticularService;
use Illuminate\Support\Facades\DB;
use App\Models\ServidorPublico;
use Illuminate\Http\Request;
use App\Models\Sancion;

class ServidorPublicoService
{
	public static function get_servidor_publico($id)
	{
        //obtener servidor publico
        $servidorPublico = ServidorPublico::where('id', $id)->first();

        return ServidorPublicoService::get_servidor_publico_with_format($servidorPublico);
	}

    public static function get_servidor_publico_by_rfc($rfc)
    {
        //obtener servidor público
		$servidorPublico = ServidorPublico::where('rfc', $rfc)->first();

        return ServidorPublicoService::get_servidor_publico_with_format($servidorPublico);
    }

    public static function get_servidor_publico_with_format($servidorPublico = null)
    {
        //dar formato a los datos       
        if($servidorPublico){
            $servidorPublico['genero'] = null;
        }

        return $servidorPublico;
    }

    public static function get_all(Request $request)
    {
        //validar filtrado por rfc
        if(isset($request->rfc)){
            //obtener homoclave 
            $request['homoclave'] = substr($request->rfc, 10, 3);
            //obtener rfc sin homoclave
            $request['rfc'] = substr($request->rfc, 0, 10);
        }
        
        $servidoresPublicos = DB::connection('pgsql_pde')
                            ->table('declaradb.principal_declarante')
                            ->where('role', 'user')
                            ->when(isset($request->rfc), function($q) use($request){
                                $q->where('rfc', $request->rfc);
                                $q->where('rfc_homoclave', $request->homoclave);
                            })
                            ->when(isset($request->institucionesIds), function($q) use($request){
                                $q->whereIn('ente_publico', $request->institucionesIds);
                            })
                            ->orderBy('id')
                            ->paginate(10);

        foreach($servidoresPublicos->getCollection() as $index => $servidorPublico){
            $servidorPublico = ServidorPublicoService::get_servidor_publico_institucion_dependencia(($servidorPublico->rfc . $servidorPublico->rfc_homoclave));
            $servidoresPublicos->getCollection()[$index] = $servidorPublico;
        }    
                            
        return $servidoresPublicos;
    }

    public static function get_by_nombres(Request $request)
    {
        $servidoresPublicos = ServidorPublico::query()
                            ->when(isset($request->nombres), function($q) use($request){
                                $q->where('nombres', 'LIKE', '%' . $request->nombres . '%');
                            })
                            ->when(isset($request->primerApellido), function($q) use($request){
                                $q->where('primerApellido', 'LIKE', '%' . $request->primerApellido . '%');
                            })
                            ->when(isset($request->segundoApellido), function($q) use($request){
                                $q->where('segundoApellido', 'LIKE', '%' . $request->segundoApellido . '%');
                            })
                            ->when(isset($request->rfc), function($q) use($request){
                                $q->where('rfc', $request->rfc);
                            })
                            ->when(isset($request->curp), function($q) use($request){
                                $q->where('curp', $request->curp);
                            })
                            ->when(isset($request->nombreRazonSocial), function($q) use($request){
                                $q->where('nombres', 'LIKE', '%' . $request->nombreRazonSocial . '%');
                            })
                            ->orderBy('id')
                            ->get();

        foreach($servidoresPublicos as $servidorPublico){
            $servidorPublico['tipo'] = 'App\Models\ServidorPublico';
        }

        return $servidoresPublicos;
    }
    
    public static function get_institucion_dependencia($id = null)
    {
        $institucionDependencia = DB::connection('pgsql_pde')
                            ->table('declaradb.globales_ente_publico')
                            ->where('id', $id)
                            ->first();

        if($institucionDependencia){
            $institucionDependencia = [
                'id' => $institucionDependencia->id,
                'nombre' => $institucionDependencia->nombre_ente,
                'siglas' => $institucionDependencia->siglas_ente,
                'clave' => $institucionDependencia->id,
            ];
        }

        return $institucionDependencia;
    }

    public static function get_servidor_publico_institucion_dependencia($search)
	{
        //obtener servidor publico
        if(strlen($search) > 10){
            $servidorPublico = ServidorPublicoService::get_servidor_publico_by_rfc($search);
        }else{
		    $servidorPublico = ServidorPublicoService::get_servidor_publico($search);
        }

        //obtener institucion dependencia
        $institucionDependencia = ServidorPublicoService::get_institucion_dependencia($servidorPublico['institucionDependenciaId'] ?? null);

        if($institucionDependencia){
            $servidorPublico['institucionDependencia'] = $institucionDependencia;
        }
        
        return $servidorPublico;
	}

    public static function get_sanciones_servidor_publico(Collection $sanciones)
    {
        foreach($sanciones as $sancion){
            $sancion['sancionado'] = ServidorPublicoService::get_servidor_publico_with_data($sancion['sancionado']);
        }
        return $sanciones;
    }

    public static function get_genero($curp)
    {
        //si existe el curp
        if(isset($curp)){
            //validar genero
            switch($curp[10]){
                case 'M': 
                    return ['clave' => 'F', 'valor' => 'FEMENINO'];
                    break;
                case 'H': 
                    return ['clave' => 'M', 'valor' => 'MASCULINO'];
                    break;
                default:
                    return ['clave' => 'O', 'valor' => 'OTRO'];
                    break;
            }
        }
        return ['clave' => 'O', 'valor' => 'OTRO'];
    }

    public static function get_servidor_publico_with_data($servidorPublico = null)
    {
        if($servidorPublico){
            $servidor = ServidorPublicoService::get_servidor_publico_by_rfc($servidorPublico['rfc']);
            $servidorPublico['genero'] = null;
        }

        return $servidorPublico;
    }

    //funcion para registrar servidor público y relacionarlo con la sanción
    public static function store_sancion(Request $request, Sancion $sancion)
    {
        //crear servidor público
        $servidorPublico = ServidorPublico::create($request->sancionado);
        //relacionar la sanción
        $sancion->sancionadoId = $servidorPublico->id;
        $sancion->save();
    }

    //función para validar rfc
    public static function validate_rfc($rfc = null)
    {
        if($rfc){
            //expresion regular para validar rfc
            $rfcPattern = '/^[A-ZÑ&]{4}[0-9]{6}[A-Z0-9Ñ]{3}$/u';
            
            //validar rfc
            return preg_match($rfcPattern, strtoupper($rfc)) === 1;
        }
        return false;
    }

    public static function get_historial($servidorPublico = null)
    {
        $servidorPublico = ServidorPublicoService::get_servidor_publico_by_rfc($servidorPublico->rfc ?? ($servidPublico['rfc'] ?? null));

        if($servidorPublico){
            //obtener el último registro de evolución patrimonial
            $evolucionPatrimonial = DB::connection('pgsql_pde')
                        ->table('declaradb.principal_evolucion_patrimonial')
                        ->where('id_declarador', $servidorPublico['id'])
                        ->orderBy('fecha_inicio', 'DESC')
                        ->first();

            if($evolucionPatrimonial){
                //obtener experiencia laboral
                $evolucionPatrimonial->experienciaLaboral = DB::connection('pgsql_pde')
                            ->table('declaradb.principal_exp_laboral')
                            ->where('id_declarante', $evolucionPatrimonial->id_declarante)
                            ->get();

                //obtener cargo actual
                $evolucionPatrimonial->cargoActual = DB::connection('pgsql_pde')
                            ->table('declaradb.principal_encargo_actual')
                            ->where('id_declarante', $evolucionPatrimonial->id_declarante)
                            ->get();

                return $evolucionPatrimonial;
            }
            
        }
        return null;
	}
}