<?php

namespace App\Services;

use Illuminate\Http\Request;
use Carbon\Carbon;

class FiltroService
{   
    //función para obtener fechas por rango
    public static function get_fechas(Request $request)
    {
        if(isset($request->fechaFinal)){
            $request['fechaFinal'] = Carbon::parse($request->fechaFinal)->endOfDay();
        }
        if(isset($request->rango)){
            switch($request->rango){
                case 'Todos':
                    $request['fechaInicial'] = null;
                    $request['fechaFinal'] = null;
                    break;
                case 'Este mes':
                    $request['fechaInicial'] = Carbon::now()->startOfMonth();
                    $request['fechaFinal'] = Carbon::now();
                    break;
                case 'Últimos 30 días':
                    $request['fechaInicial'] = Carbon::now()->subMonth()->startOfDay();
                    $request['fechaFinal'] = Carbon::now();
                    break;
                case 'Últimos 2 meses':
                    $request['fechaInicial'] = Carbon::now()->subMonths(2)->startOfDay();
                    $request['fechaFinal'] = Carbon::now();
                    break;
                case 'Últimos 6 meses':
                    $request['fechaInicial'] = Carbon::now()->subMonths(6)->startOfDay();
                    $request['fechaFinal'] = Carbon::now();
                    break;
                case 'Este año':
                    $request['fechaInicial'] = Carbon::now()->startOfYear();
                    $request['fechaFinal'] = Carbon::now();
                    break;
                case 'El año pasado':
                    $request['fechaInicial'] = Carbon::now()->subYear()->startOfYear();
                    $request['fechaFinal'] = Carbon::now()->subYear()->endOfYear();
                    break; 
                case 'Últimos dos años':
                    $request['fechaInicial'] = Carbon::now()->subYears(2)->startOfYear();
                    $request['fechaFinal'] = Carbon::now()->subYear()->endOfYear();
                    break; 
                default:
                    $request['fechaInicial'] = null;
                    $request['fechaFinal'] = null;
                    break;
            }
        }
        return $request;
    }
}