<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\Constancia;

class ConstanciaService
{   
    //función para generar folio
    public static function generate_folio()
    {
        //obtener cantidad de constancias generadas
        $folio = '0'. Constancia::count();

        //agregar 0
        while(strlen($folio)<4){
            $folio = '0'.$folio;
        }

        //agregar C
        $folio = 'C'. $folio;

        return $folio;
    }
}