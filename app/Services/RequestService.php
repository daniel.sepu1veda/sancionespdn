<?php

namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Services\ParticularService;
use App\Models\InstitucionDependencia;
use App\Models\TipoDocumento;
use App\Models\TipoSancion;
use App\Models\Particular;
use App\Models\TipoFalta;
use Carbon\Carbon;

class RequestService
{   
    public static function sancion_particular_store_request()
	{
        $instituciones = InstitucionDependencia::all();
        $tiposSancion = TipoSancion::all();
        $tiposFalta = TipoFalta::all();
        $particulares = Particular::with('domiciliosMexico', 'domiciliosExtranjero')->get();

        //institucion
        $institucion = $instituciones->random();

        //particular
        $particular = $particulares->random();
        $particular['domicilioMexico'] = $particular->domiciliosMexico->first() ?? null;
        $particular['domicilioExtranjero'] = $particular->domiciliosMexico->first() ?? null;

		$data = [   
            'fechaCaptura' => Carbon::now()->subDays(rand(0, 10)),
            'expediente' => '2023' . strtoupper(Str::random(6)),
            'autoridadSancionadora' => $institucion->nombre,
            'causaMotivoHechos' => fake()->sentence(),
            'objetoContrato' => fake()->sentence(),
            'acto' => fake()->sentence(),
            'observaciones' => fake()->sentence(),
            'institucionDependenciaId' => $institucion->id,
            'sancionadoId' => $particular->id,
            'sancionadoTipo' => 'App\Models\Particular',
            'tipoFaltaId' => $tiposFalta->random()->id,
            'tiposSancion' => [
                '2', '3', '4',
            ],
            'sancionado' => $particular,
        ];

        $request = new Request($data);
        
        return $request;
	}

    public static function sancion_nuevo_particular_store_request()
	{
        $instituciones = InstitucionDependencia::all();
        $tiposSancion = TipoSancion::all();
        $tiposFalta = TipoFalta::all();
        $particulares = Particular::with('domiciliosMexico', 'domiciliosExtranjero')->get();

        //institucion
        $institucion = $instituciones->random();
        
        //particular
        $particular = ParticularService::get_fake_data();

		$data = [   
            'fechaCaptura' => Carbon::now()->subDays(rand(0, 10)),
            'expediente' => '2023' . strtoupper(Str::random(6)),
            'autoridadSancionadora' => $institucion->nombre,
            'causaMotivoHechos' => fake()->sentence(),
            'objetoContrato' => fake()->sentence(),
            'acto' => fake()->sentence(),
            'observaciones' => fake()->sentence(),
            'institucionDependenciaId' => $institucion->id,
            'sancionadoTipo' => 'App\Models\Particular',
            'tipoFaltaId' => $tiposFalta->random()->id,
            'tiposSancion' => [
                '2', '3', '4',
            ],
            'sancionado' => $particular,
            'responsableSancion' => [
                // 'id' => 13,
                'nombres' => fake()->name(),
                'primerApellido' => fake()->lastname(),
                'segundoApellido' => fake()->lastname(),
                'institucionDependenciaId' => $institucion->id,
            ],
            'resolucion' => [
                "sentido" => fake()->sentence(),
                "url" => fake()->url(),
                "fechaResolucion" => Carbon::now()->subDays(rand(0, 10)),
                "fechaNotificacion" => Carbon::now()->subDays(rand(0, 10)),
            ],
            "inhabilitacion" => [
                "plazo"=> fake()->sentence(),
                "fechaInicial"=> Carbon::now()->subDays(rand(50, 200)),
                "fechaFinal"=> Carbon::now()->subDays(rand(0, 50)),
            ],
            "multa" => [
                "monto" => fake()->randomNumber(5),
                "monedaId" => rand(1,3),
            ],
            "documentos" => [
                [
                    "tipo" => TipoDocumento::findOrFail(rand(1,4))->clave,
                    "titulo" => fake()->sentence(),
                    "descripcion" => fake()->sentence(),
                    "url" => fake()->url(),
                    "fecha" => Carbon::now()->subDays(rand(0, 10)),
                    "tipoDocumentoId" => rand(1,4),
                ],
                [
                    "tipo" => TipoDocumento::findOrFail(rand(1,4))->clave,
                    "titulo" => fake()->sentence(),
                    "descripcion" => fake()->sentence(),
                    "url" => fake()->url(),
                    "fecha" => Carbon::now()->subDays(rand(0, 10)),
                    "tipoDocumentoId" => rand(1,4),
                ],
            ],
        ];

        $request = new Request($data);
        
        return $request;
	}

	public static function particular_store_request()
	{
		$particular = ParticularService::get_fake_data();
        
        $request = new Request($particular);
        
        return $request;
	}

    public static function particular_update_request($id)
	{
		$particular = Particular::with('tipoPersona', 'genero')
                                ->with('domiciliosMexico.localidad')
                                ->with('domiciliosMexico.vialidad')
                                ->with('domiciliosExtranjero.pais')
                                ->findOrFail($id);
        
        $request = new Request($particular->toArray());

        //nuevos datos
        $request['objetoSocial'] = fake()->sentence();
        $request['rfc'] = strtoupper(Str::random(13));
        
        //nuevos datos domicilios Mexico
        foreach($request->domiciliosMexico as $domicilioMexico){
            $domicilioMexico->codigoPostal = fake()->randomNumber(4);
        }

        //nuevos datos domicilios extranjero
        foreach($request->domiciliosExtranjero as $domiciliosExtranjero){
            $domicilioExtranjero->calle = fake()->name();
            $domiciliosExtranjero->codigoPostal = fake()->randomNumber(4);
        }

        return $request;
	}
}
