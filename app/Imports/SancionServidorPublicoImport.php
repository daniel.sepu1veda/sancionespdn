<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Services\ServidorPublicoService;
use App\Models\InstitucionDependencia;
use App\Models\ServidorPublico;
use App\Models\Inhabilitacion;
use App\Models\TipoDocumento;
use App\Models\TipoSancion;
use App\Models\Resolucion;
use App\Models\Documento;
use App\Models\TipoFalta;
use App\Models\Sancion;
use App\Models\Moneda;
use App\Models\Multa;

class SancionServidorPublicoImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        //obtener servidor publico por rfc
        $servidorPublico = ServidorPublicoService::get_servidor_publico_by_rfc($row[0]);
        if(isset($servidorPublico)){
            //registrar servidor publico con puesto y nivel
            $servidorPublico = ServidorPublico::create([
                'rfc' => $row[0],
                'puesto' => $row[1],
                'nivel' => $row[2],
            ]);

            //obtener institucion
            $institucion = InstitucionDependencia::where('siglas', $row[3])->first();

            //obtener tipo de falta
            $tipoFalta = TipoFalta::where('clave', $row[8])->first();

            //registrar sanción
            $sancion = Sancion::create([
                'institucionDependenciaId' => $institucion->id ?? null,
                'expediente' => $row[4],
                'autoridadSancionadora' => $row[5],
                'causaMotivoHechos' => $row[6],
                'observaciones' => $row[7],
                'sancionadoId' => $servidorPublico->id ?? null,
                'sancionadoTipo' => 'App\Models\ServidorPublico',
                'tipoFaltaId' => $tipoFalta->id ?? null,
            ]);

            //crear resolucion
            if(isset($row[10]) || isset($row[11])){
                Resolucion::create([
                    'fechaResolucion' => $row[10],
                    'url' => $row[11],
                    'sancionId' => $sancion->id,
                ]);
            }

            //crear inhabilitacion
            if(isset($row[12]) || isset($row[13])){
                Inhabilitacion::create([
                    'plazo' => $row[12],
                    'fechaInicial' => $row[13],
                    'fechaFinal' => $row[14],
                    'sancionId' => $sancion->id,
                ]);
            }

            //crear multa
            if(isset($row[15]) || isset($row[16])){
                //obtener moneda
                $moneda = Moneda::where('valor', $row[16])->first();
                //crear registro
                Multa::create([
                    'monto' => $row[15] ?? 0,
                    'monedaId' => $moneda->id ?? 1,
                    'sancionId' => $sancion->id,
                ]);
            }

            //crear documento adicional
            if(isset($row[18]) || isset($row[20])){
                //obtener tipo de documento
                $tipoDocumento = TipoDocumento::where('valor', $row[17])->first();
                //crear registro
                Documento::create([
                    'tipo' => $tipoDocumento->clave ?? null,
                    'titulo' => $row[18],
                    'descripcion' => $row[19],
                    'url' => $row[20],
                    'fecha' => $row[21],
                    'tipoDocumentoId' => $tipoDocumento->id ?? null,
                    'sancionId' => $sancion->id,
                ]);
            }

            //tipos de sancion
            if(isset($row[9])){
                //obtener claves de tipos de sancion
                $claves = explode(',', $row[9]);
                foreach($claves as $clave){
                    $tipoSancion = TipoSancion::where('clave', $clave)->first();
                    //crear relacion
                    if($tipoSancion){
                        $sancion->tiposSancion()->attach($tipoSancion->id, [
                            'valor' => $tipoSancion->valor,
                            'created_at' => now(),
                            'updated_at' => now(),
                        ]);
                    }
                }  
            }      
        }
    }

    public function startRow(): int
    {
        return 7;
    }
}
