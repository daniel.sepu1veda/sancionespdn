<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithStartRow;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use App\Services\ServidorPublicoService;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Models\InstitucionDependencia;
use App\Models\ServidorPublico;
use App\Models\Inhabilitacion;
use App\Models\TipoDocumento;
use App\Models\TipoSancion;
use App\Models\Resolucion;
use App\Models\Documento;
use App\Models\TipoFalta;
use App\Models\Sancion;
use App\Models\Moneda;
use App\Models\Multa;

class SancionServidorPublicoImportValidator implements ToModel, WithStartRow
{
    public function __construct()
    {
        session(['row' => 6]);
        session(['log' => null]);
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        //contador
        session(['row' => session('row') + 1]);
        //si existe la celda
        if(isset($row[0])){
            //obtener servidor publico por rfc
            $servidorPublico = ServidorPublicoService::get_servidor_publico_by_rfc($row[0]);
            if(isset($servidorPublico)){
                //puesto
                if(is_null($row[1])) session(['log' => session('log') . 'Linea ' . session('row') . ': Campo Puesto es obligatorio' . '&&&']);
                //institucion
                if(is_null($row[3])) session(['log' => session('log') . 'Linea ' . session('row') . ': Campo Institución es obligatorio' . '&&&']);
                //expediente
                if(is_null($row[4])) session(['log' => session('log') . 'Linea ' . session('row') . ': Campo Expediente es obligatorio' . '&&&']);
                //expediente duplicado
                if(Sancion::where('expediente', $row[4])->exists()) session(['log' => session('log') . 'Linea ' . session('row') . ': Expediente ' . $row[4] . ' ya existe en el sistema' . '&&&']);
                //causa motivo hechos
                if(is_null($row[6])) session(['log' => session('log') . 'Linea ' . session('row') . ': Campo Causa/Motivo/Hechos es obligatorio' . '&&&']);
                //tipo de falta
                if(is_null($row[8])) session(['log' => session('log') . 'Linea ' . session('row') . ': Tipo de Falta es obligatorio' . '&&&']);
                //tipos de sancion
                if(is_null($row[9])) session(['log' => session('log') . 'Linea ' . session('row') . ': Tipos de Sanción es obligatorio' . '&&&']);
                //fecha resolucion
                if(is_null($row[10])) session(['log' => session('log') . 'Linea ' . session('row') . ': Campo Fecha de Resolución obligatorio' . '&&&']);
                //url resolucion
                if(is_null($row[11])) session(['log' => session('log') . 'Linea ' . session('row') . ': Campo URL de Resolución obligatorio' . '&&&']);
                //formato de fecha resolucion
                if(isset($row[10]) && !$this->is_date($row[10])) session(['log' => session('log') . 'Linea ' . session('row') . ': Formato de Fecha de Resolución incorrecto' . '&&&']);
                //formato de fecha inicial inhabilitacion
                if(isset($row[13]) && !$this->is_date($row[13])) session(['log' => session('log') . 'Linea ' . session('row') . ': Formato de Fecha Inicial de Inhabilitación incorrecto' . '&&&']);
                //formato de fecha final inhabilitacion
                if(isset($row[14]) && !$this->is_date($row[14])) session(['log' => session('log') . 'Linea ' . session('row') . ': Formato de Fecha Final de Inhabilitación incorrecto' . '&&&']);
                //formato de fecha documento
                if(isset($row[21]) && !$this->is_date($row[21])) session(['log' => session('log') . 'Linea ' . session('row') . ': Formato de Fecha de Documento incorrecto' . '&&&']);
                //validar tipos de sancion
                if(isset($row[9])){
                    //obtener claves de tipos de sancion
                    $claves = explode(',', $row[9]);
                    foreach($claves as $clave){
                        //obtener tipo de sancion
                        $tipoSancion = TipoSancion::where('clave', $clave)->first();
                        if(is_null($tipoSancion)) session(['log' => session('log') . 'Linea ' . session('row') . ': Clave de Tipo de Sanción "' . $clave . '" no encontrada' . '&&&']);
                    }  
                }
            }else{
                //guardar en sesión rfc no encontrado
                session(['log' => session('log') . 'Linea ' . session('row') . ': RFC ' . $row[0] . ' no existe en el S1' . '&&&']);
            }  
        }   
    }

    public function startRow(): int
    {
        return 7;
    }

    public function is_date($fecha)
    {
        //validar si la fecha es entero
        if(is_int($fecha)){
            $dateFormat1 = Date::excelToDateTimeObject($fecha)->format('Y-m-d');
            if ($dateFormat1) {
                return true;
            }
        }

        //validar si la fecha es string
        if(is_string($fecha)){
            if (preg_match('/^\d{4}-\d{2}-\d{2}$/', $fecha)){
                return true;
            }
        }

        return false;
    }
}
