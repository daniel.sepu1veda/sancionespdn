<?php

namespace App\Traits;

use Illuminate\Support\Facades\Auth;
use App\Models\Log;

trait LogTrait 
{
	public function crearLog($descripcion, $accion, $modulo, $object)
	{
        if(auth()->check()){
            Log::create([
                'descripcion' => $descripcion,
                'accion' => ucwords($accion),
                'modulo' => $modulo,
                'userId' => auth()->user()->id,
                'logeableTipo' => is_null($object) ? null : get_class($object),
                'logeableId' => $object->id ?? null,
            ]);
        }
	}
}