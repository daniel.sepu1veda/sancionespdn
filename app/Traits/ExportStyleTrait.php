<?php

namespace App\Traits;

use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Cell\DataValidation;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Style\Border;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

trait ExportStyleTrait
{
    public function applyStyles(Worksheet $sheet)
    {

        $rows = $sheet->getHighestRow();
        $columns = $sheet->getHighestColumn();

        //Bordes a todas las celdas de la tabla
        $sheet->getStyle("A3:{$columns}{$rows}")
            ->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

        $sheet->getStyle("A:AC")
            ->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_TEXT);

        //Estilos en negritas y alineación
        $sheet->getStyle('6')->getFont()->applyFromArray(['bold' => 'true']);
        $sheet->getStyle('6')->getAlignment()->applyFromArray(['horizontal' => 'center']);
        $sheet->getStyle("A7:{$columns}{$rows}")->getAlignment()->applyFromArray(['horizontal' => 'left']);
        $sheet->getStyle('A3')->getFont()->applyFromArray(['bold' => 'true']);
        $sheet->getStyle('A4')->getFont()->applyFromArray(['bold' => 'true']);
        $sheet->getStyle('A5')->getFont()->applyFromArray(['bold' => 'true']);

        //Altura de las filas
        $sheet->getRowDimension(1)->setRowHeight(60);
        $sheet->getRowDimension(6)->setRowHeight(20);

        //Combinar columnas
        $sheet->mergeCells("A1:{$columns}1");
        $sheet->mergeCells("A2:{$columns}2");
        $sheet->mergeCells("B3:{$columns}3");
        $sheet->mergeCells("B4:{$columns}4");
        $sheet->mergeCells("B5:{$columns}5");
    }

    public function setDrawings()
    {
        if(file_exists(public_path('images/pde.png'))){
            $logo = new Drawing();
            $logo->setName('Logo');
            $logo->setDescription('Logo');
            $logo->setPath(public_path('images/pde.png'));
            $logo->setHeight(60);
            $logo->setOffsetX(10);
            $logo->setOffsetY(10);
            $logo->setCoordinates('A1');

            return [$logo];
        }
        
        return [];
    }

    public function setEvents($tituloReporte, $catalogs = null)
    {
        return [
            BeforeSheet::class => function(BeforeSheet $event) use ($tituloReporte){
                //Añadir filas antes de colocar los datos
                $event->sheet->append([' ']);
                $event->sheet->append([' ']);
                $event->sheet->append(['Reporte: ', $tituloReporte]);
                $event->sheet->append(['Fecha: ', Carbon::now()->format('d-m-Y')]);
                $event->sheet->append(['Usuario: ', auth()->user()->name]);
            },

            AfterSheet::class => function (AfterSheet $event) use($catalogs){
                //Inmovilizar filas
                $event->sheet->getDelegate()->freezePane('A1');
                $event->sheet->getDelegate()->freezePane('A2');
                $event->sheet->getDelegate()->freezePane('A3');
                $event->sheet->getDelegate()->freezePane('A4');
                $event->sheet->getDelegate()->freezePane('A5');
                $event->sheet->getDelegate()->freezePane('A6');
                $event->sheet->getDelegate()->freezePane('A7');

                //catálogos
                if(isset($catalogs)){
                    foreach($catalogs as $catalog){
                        //obtener el módelo del catálogo, sino existe, obtener los valores que se mandan
                        $collection = isset($catalog['model']) ? $catalog['model']::get() : $catalog['values'];

                        //primer y último row
                        $startCell = $catalog['start_cell'] ?? null;
                        $endCell = $catalog['end_cell'] ?? null;

                        //convertir el catálogo a una cadena de texto, si no existe obtener los valores que se mandan
                        $configs = isset($catalog['model']) ? $collection->pluck($catalog['key'] ?? 'valor')->implode(', ') : $collection;

                        //aplicar lista desplegables
                        if(isset($startCell) && isset($endCell)){
                            $objValidation = $event->sheet->getCell($startCell)->getDataValidation();
                            $objValidation->setType(DataValidation::TYPE_LIST);
                            $objValidation->setErrorStyle(DataValidation::STYLE_INFORMATION);
                            $objValidation->setAllowBlank(false);
                            $objValidation->setShowInputMessage(true);
                            $objValidation->setShowErrorMessage(true);
                            $objValidation->setShowDropDown(true);
                            $objValidation->setErrorTitle('Advertencia de registro');
                            $objValidation->setError('El valor no está en la lista');
                            $objValidation->setPromptTitle('Elija de la lista');
                            $objValidation->setPrompt('Por favor elija un valor de la lista');
                            $objValidation->setFormula1('"' . $configs . '"');

                            $range = $startCell . ':' . $endCell;
                            $event->sheet->setDataValidation($range, $objValidation);
                        }
                    }
                }
            },
        ];
    }
}