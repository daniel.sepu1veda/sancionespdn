<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ServidorPublico;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class ServidorPublicoController extends Controller
{
    public function get_servidores_publicos(Request $request)
    {
        $servidoresPublicos = ServidorPublico::query()
                            ->when(isset($request->nombres), function($q) use($request){
                                $q->where('nombres', 'LIKE', '%' . $request->nombres . '%');
                            })
                            ->when(isset($request->primerApellido), function($q) use($request){
                                $q->where('primerApellido', 'LIKE', '%' . $request->primerApellido . '%');
                            })
                            ->when(isset($request->segundoApellido), function($q) use($request){
                                $q->where('segundoApellido', 'LIKE', '%' . $request->segundoApellido . '%');
                            })
                            ->when(isset($request->rfc), function($q) use($request){
                                $q->where('rfc', $request->rfc);
                            })
                            ->when(isset($request->curp), function($q) use($request){
                                $q->where('curp', $request->curp);
                            })
                            ->orderBy('id')
                            ->paginate(10);
                    
        return $this->jsonResponse("Registro obtenido correctamente", $servidoresPublicos, Response::HTTP_OK , []);
    }
}
