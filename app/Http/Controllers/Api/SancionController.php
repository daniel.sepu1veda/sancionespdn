<?php

namespace App\Http\Controllers\Api;

use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\Controller;
use App\Services\ServidorPublicoService;
use App\Services\DirectorGeneralService;
use App\Services\ApoderadoLegalService;
use App\Services\ParticularService;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Models\DirectorGeneral;
use App\Models\ServidorPublico;
use App\Models\ApoderadoLegal;
use App\Models\Particular;
use App\Models\Sancion;

class SancionController extends Controller
{
    public function get_sancionados(Request $request)
    {
        $servidoresPublicos = ServidorPublicoService::get_by_nombres($request);
        $particulares = ParticularService::get_by_nombres($request);
        $apoderadosLegales = ApoderadoLegalService::get_by_nombres($request);
        $directoresGenerales = DirectorGeneralService::get_by_nombres($request);

        $resultados = $servidoresPublicos
            ->merge($particulares)
            ->merge($apoderadosLegales)
            ->merge($directoresGenerales);
        
        //formato para indicar que es resultado nacional
        foreach($resultados as $resultado){
            $resultado['nacional'] = true;
        }

        return $this->jsonResponse("Registro obtenido correctamente", $resultados, Response::HTTP_OK , []);
    }

    public function get_sancionados_paginados(Request $request)
    {
        $servidoresPublicos = ServidorPublicoService::get_by_nombres($request);
        $particulares = ParticularService::get_by_nombres($request);
        $apoderadosLegales = ApoderadoLegalService::get_by_nombres($request);
        $directoresGenerales = DirectorGeneralService::get_by_nombres($request);

        $resultadosCombinados = $servidoresPublicos
                        ->merge($particulares)
                        ->merge($apoderadosLegales)
                        ->merge($directoresGenerales);

        $perPage = 10;

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $currentItems = $resultadosCombinados->slice(($currentPage - 1) * $perPage, $perPage)->values();

        $paginador = new LengthAwarePaginator($currentItems, $resultadosCombinados->count(), $perPage, $currentPage);
        $paginador->setPath($request->url());

        return $this->jsonResponse("Registro obtenido correctamente", $paginador, Response::HTTP_OK , []);
    }

    public function get_sanciones(Request $request)
    {
        //si es apoderado legal 
        if($request->tipo == 'App\Models\ApoderadoLegal'){
            $apoderadoLegal = ApoderadoLegal::where('nombres', $request->nombres)
                                            ->where('primerApellido', $request->primerApellido)
                                            ->where('segundoApellido', $request->segundoApellido)
                                            ->orWhere('id', $request->id)
                                            ->with('particular.sanciones')
                                            ->first();
            //obtener sanciones
            $sanciones = isset($apoderadoLegal->particular) ? $apoderadoLegal->particular->sanciones : [];
            if(!empty($sanciones)){
                $sanciones->load('sancionado.apoderadosLegales', 'sancionado.directoresGenerales', 'institucionDependencia', 'tipoFalta', 'tiposSancion', 'responsableSancion');
            }
        }

        //si es un director general
        if($request->tipo == 'App\Models\DirectorGeneral'){
            $directorGeneral = DirectorGeneral::where('nombres', $request->nombres)
                                            ->where('primerApellido', $request->primerApellido)
                                            ->where('segundoApellido', $request->segundoApellido)
                                            ->orWhere('id', $request->id)
                                            ->with('particular.sanciones')
                                            ->first();
            //obtener sanciones
            $sanciones = isset($directorGeneral->particular) ? $directorGeneral->particular->sanciones : [];
            if(!empty($sanciones)){
                $sanciones->load('sancionado.directoresGenerales', 'sancionado.apoderadosLegales', 'institucionDependencia', 'tipoFalta', 'tiposSancion', 'responsableSancion');
            }
        }

        //si es un particular
        if($request->tipo == 'App\Models\Particular'){
            $sanciones = Particular::where('id', $request->id)
                                    ->orWhere('nombreRazonSocial', $request->nombreRazonSocial)
                                    ->with('sanciones', function($q){
                                        $q->with('sancionado', 'institucionDependencia', 'tipoFalta', 'tiposSancion', 'responsableSancion', 'inhabilitacion', 'resolucion', 'multa', 'documentos');
                                    })
                                    ->first()->sanciones ?? [];
        }
        
        //si es un servidor público
        if($request->tipo == 'App\Models\ServidorPublico'){
            $sanciones = ServidorPublico::where('id', $request->id)
                            ->with('sanciones', function($q){
                                $q->with('sancionado', 'institucionDependencia', 'tipoFalta', 'tiposSancion', 'responsableSancion', 'inhabilitacion', 'resolucion', 'multa', 'documentos');
                            })
                            ->first()->sanciones ?? [];
        }

        return $this->jsonResponse("Registro obtenido correctamente", $sanciones ?? [], Response::HTTP_OK , []);
    }

    public function get_servidores_publicos_sancionados(Request $request)
    {
        $servidoresPublicos = ServidorPublico::query()
                            ->when(isset($request->nombres), function($q) use($request){
                                $q->where('nombres', 'LIKE', '%' . $request->nombres . '%');
                            })
                            ->when(isset($request->primerApellido), function($q) use($request){
                                $q->where('primerApellido', 'LIKE', '%' . $request->primerApellido . '%');
                            })
                            ->when(isset($request->segundoApellido), function($q) use($request){
                                $q->where('segundoApellido', 'LIKE', '%' . $request->segundoApellido . '%');
                            })
                            ->when(isset($request->rfc), function($q) use($request){
                                $q->where('rfc', $request->rfc);
                            })
                            ->when(isset($request->curp), function($q) use($request){
                                $q->where('curp', $request->curp);
                            })
                            ->orderBy('id')
                            ->paginate(10);
                    
        return $this->jsonResponse("Registro obtenido correctamente", $servidoresPublicos, Response::HTTP_OK , []);
    }

    public function get_sanciones_servidores_publicos()
    {
        $sanciones =  Sancion::where('sancionadoTipo', 'App\Models\ServidorPublico')
                                ->with('sancionado', 'institucionDependencia', 'tipoFalta', 'tiposSancion', 'responsableSancion')
                                ->get();

        return $this->jsonResponse("Registro obtenido correctamente", $sanciones, Response::HTTP_OK , []);
    }
}
