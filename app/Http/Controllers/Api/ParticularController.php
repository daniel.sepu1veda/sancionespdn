<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Models\Particular;

class ParticularController extends Controller
{
    public function get_particulares(Request $request)
    {
        $particulares = Particular::query()
                        ->when(isset($request->nombres), function($q) use($request){
                            $q->where('nombreRazonSocial', 'LIKE', '%' . $request->nombres . '%');
                        })
                        ->when(isset($request->primerApellido), function($q) use($request){
                            $q->where('nombreRazonSocial', 'LIKE', '%' . $request->primerApellido . '%');
                        })
                        ->when(isset($request->segundoApellido), function($q) use($request){
                            $q->where('nombreRazonSocial', 'LIKE', '%' . $request->segundoApellido . '%');
                        })
                        ->when(isset($request->rfc), function($q) use($request){
                            $q->where('rfc', $request->rfc);
                        })
                        ->orderBy('id')
                        ->paginate(10);
                    
        return $this->jsonResponse("Registro obtenido correctamente", $particulares, Response::HTTP_OK , []);
    }
}
