<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Models\ResponsableSancion;

class ResponsableSancionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $responsables_sancion = ResponsableSancion::with('institucionDependencia', 'sanciones')->get();

        #return $responsables_sancion;
        $this->crearLog('Consultar responsables sancion', 'consultar', 'responsables-sancion', null);
        return view('admin.responsables_sancion.index', get_defined_vars());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $responsable_sancion = ResponsableSancion::create($request->all());

        $this->crearLog('Registro responsables sancion', 'registro', 'responsables-sancion', $responsable_sancion);
        return redirect()->back()->with('success', 'ok');
    }

    /**
     * Display the specified resource.
     */
    public function get(string $id)
    {
        $responsable_sancion = ResponsableSancion::with('institucionDependencia', 'sanciones')->findOrFail($id);

        #return $responsable_sancion;
        $this->crearLog('Consultar responsables sancion', 'consultar', 'responsables-sancion', $responsable_sancion);
        return $this->jsonResponse("Registro consultado correctamente", $responsable_sancion, Response::HTTP_OK, null);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        $responsable_sancion = ResponsableSancion::findOrFail($request->id);

        $responsable_sancion->update($request->all());

        $this->crearLog('Actualizar responsables sancion', 'actualizar', 'responsables-sancion', $responsable_sancion);
        return redirect()->back()->with('success', 'ok');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $responsable_sancion = ResponsableSancion::findOrFail($id);

        $responsable_sancion->delete();

        $this->crearLog('Eliminar responsables sancion', 'eliminar', 'responsables-sancion', $responsable_sancion);
        return $this->jsonResponse("Registro eliminado correctamente", $responsable_sancion, Response::HTTP_OK, null);
    }
}
