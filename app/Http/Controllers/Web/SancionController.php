<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Exports\SancionExport;
use App\Services\RequestService;
use App\Services\DomicilioService;
use Illuminate\Support\Facades\DB;
use App\Services\ParticularService;
use App\Http\Controllers\Controller;
use App\Services\ServidorPublicoService;
use App\Models\InstitucionDependencia;
use App\Models\DomicilioExtranjero;
use App\Models\ResponsableSancion;
use App\Models\DomicilioMexico;
use App\Models\ServidorPublico;
use App\Models\DirectorGeneral;
use App\Models\ApoderadoLegal;
use App\Models\Inhabilitacion;
use App\Models\TipoDocumento;
use App\Models\TipoVialidad;
use App\Models\TipoSancion;
use App\Models\TipoPersona;
use App\Models\Resolucion;
use App\Models\Particular;
use App\Models\TipoFalta;
use App\Models\Documento;
use App\Models\Localidad;
use App\Models\Vialidad;
use App\Models\Sancion;
use App\Models\Moneda;
use App\Models\Multa;
use App\Models\Pais;
use Carbon\Carbon;
use ZipArchive;
use Excel;
use PDF;

class SancionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $sanciones = Sancion::where('sancionadoTipo', 'App\Models\Particular')
                    ->with('institucionDependencia', 'tipoFalta', 'tiposSancion', 'responsableSancion', 'resolucion', 'inhabilitacion')
                    ->with('domicilioMexico', 'domicilioExtranjero')
                    ->with('documentos.tipoDocumento')
                    ->with('multa.moneda')
                    ->with('sancionado')
                    ->get();

        return $sanciones;
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
        //catálogos
        $institucionesDependencias = InstitucionDependencia::all();
        $responsablesSancion = ResponsableSancion::all();
        $tiposDocumento = TipoDocumento::all();
        $tiposVialidad = TipoVialidad::all();
        $tiposPersona = TipoPersona::all();
        $tiposSancion = TipoSancion::all();
        $tiposFalta = TipoFalta::all();
        $monedas = Moneda::all();
        $paises = Pais::all();
        $rfc = $request->rfc;
        
        //redireccionar a vista de registro de sanción de particulares
        if(request()->route()->getName() == 'sanciones.create.particulares'){
            //obtener particular
            $particular = isset($rfc) ? ParticularService::get_particular_by_rfc($rfc) : null;
            $localidades = isset($particular) ? ParticularService::get_localidades_particular($particular) : [];

            //redireccionar a la vista de particulares
            return view('admin.sanciones.create-particulares', get_defined_vars());
        }

        //obtener servidor público
        $servidorPublico = isset($rfc) ? ServidorPublicoService::get_servidor_publico_by_rfc($rfc) : null;

        //redireccionar a vista de registro de sanción de servidores públicos
        return view('admin.sanciones.create-servidores-pubicos', get_defined_vars());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $sancion = Sancion::create($request->all());

        //responsable sancion
        if(isset($request->responsableSancion)){
            //registrar responsable de sancion
            $responsableSancion = isset($request->responsableSancion['id']) && $request->responsableSancion['id'] != 0
                                ? ResponsableSancion::find($request->responsableSancion['id'])
                                : ResponsableSancion::create($request->responsableSancion);

            //asignar a la sancion
            $sancion->responsableSancionId = $responsableSancion->id ?? null;
            $sancion->save();
        }

        //resolucion
        if(isset($request->resolucion)){
            $resolucion = Resolucion::create($request->resolucion);
            $resolucion->sancionId = $sancion->id;
            $resolucion->save();
        }

        //inhabilitacion
        if(isset($request->inhabilitacion)){
            $inhabilitacion = Inhabilitacion::create($request->inhabilitacion);
            $inhabilitacion->sancionId = $sancion->id;
            $inhabilitacion->save();
        }

        //multa
        if(isset($request->multa)){
            $multa = Multa::create($request->multa);
            $multa->sancionId = $sancion->id;
            $multa->save();
        }

        //documentos
        if(isset($request->documentos)){
            foreach($request->documentos as $documento){
                $documento = Documento::create($documento);
                $documento->sancionId = $sancion->id;
                //guardar clave de tipo de documento
                $documento->tipo = TipoDocumento::find($documento->tipoDocumentoId)->clave ?? null;
                $documento->save();
            }
        }  

        //tipos de sancion
        if($request->has('tiposSancion')){
            $tiposSancion = explode(',', $request->tiposSancion);
            foreach($tiposSancion as $tipoSancionId){
                $tipoSancion = TipoSancion::find($tipoSancionId);
                if($tipoSancion){
                    $sancion->tiposSancion()->attach($tipoSancion->id, [
                        'valor' => ($tipoSancion->clave == 'O') ? $request->otroTipoSancion : $tipoSancion->valor,
                        'created_at' => now(),
                        'updated_at' => now(),
                    ]);
                }
            }
        }
        
        //registrar particular
        if($request->sancionadoTipo == 'App\Models\Particular'){
            //registrar particular y relacionar sanción
            ParticularService::store_sancion($request, $sancion);
            //crear log y redireccionar a vista de particulares sancionados
            $this->crearLog('Registro sanciones de particulares', 'registro', 'sanciones', $sancion);
            return redirect()->route('particulares.sancionados')->with('success', 'ok');
        }

        //registrar servidor público
        if($request->sancionadoTipo == 'App\Models\ServidorPublico'){
            //registrar particular y relacionar sanción
            ServidorPublicoService::store_sancion($request, $sancion);
        }
        
        //actualizar fecha de captura
        $sancion->fechaCaptura = $sancion->updated_at;
        $sancion->save();
        
        //crear log y redireccionar a vista de servidores públicos sancionados
        $this->crearLog('Registro sanciones de servidores publicos', 'registro', 'sanciones', $sancion);
        return redirect()->route('servidores.publicos.sancionados')->with('success', 'ok');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //catálogos de sancion
        $institucionesDependencias = InstitucionDependencia::all();
        $responsablesSancion = ResponsableSancion::all();
        $tiposDocumento = TipoDocumento::all();
        $tiposSancion = TipoSancion::all();
        $tiposFalta = TipoFalta::all();
        $monedas = Moneda::all();

        //obtener sanción
        $sancion = Sancion::with('sancionado', 'institucionDependencia', 'tipoFalta', 'tiposSancion', 'responsableSancion', 'resolucion', 'inhabilitacion')
                    ->with('documentos.tipoDocumento')
                    ->with('multa.moneda')
                    ->findOrFail($id);

        if($sancion->sancionadoTipo == 'App\Models\ServidorPublico'){
            $sancion['sancionado']['genero'] = null;
        }
        
        //si es particular, cargar relaciones
        if($sancion->sancionadoTipo == 'App\Models\Particular'){
            $sancion = ParticularService::get_particular_sancion($sancion);
            
            //catálogos para particulares y domicilios
            $tiposVialidad = TipoVialidad::all();
            $tiposPersona = TipoPersona::all();
            $paises = Pais::all();
            $localidades = [];

            //obtener localidades
            if(isset($sancion->sancionado->domicilioMexico) && isset($sancion->sancionado->domicilioMexico->codigoPostal)){
                $localidades = DomicilioService::get_localidades_by_cp(intval($sancion->sancionado->domicilioMexico->codigoPostal ?? 0));
                $localidades = $localidades['localidades'] ?? [];
            }

            #return $localidades;
            return view('admin.sanciones.details-particulares', get_defined_vars());
        }

        #return $sancion;
        $this->crearLog('Consultar sanciones', 'consultar', 'sanciones', $sancion);
        return view('admin.sanciones.details-servidores-publicos', get_defined_vars());
    }

    /**
     * Display the specified resource.
     */
    public function get(string $id)
    {
        //obtener sanción
        $sancion = Sancion::with('sancionado', 'institucionDependencia', 'tipoFalta', 'tiposSancion', 'responsableSancion', 'resolucion', 'inhabilitacion')
                    ->with('documentos.tipoDocumento')
                    ->with('multa.moneda')
                    ->findOrFail($id);

        if($sancion->sancionadoTipo == 'App\Models\ServidorPublico'){
            $sancion['sancionado'] = ServidorPublicoService::get_servidor_publico_with_data($sancion['sancionado']);
        }
        
        //si es particular, cargar relaciones
        if($sancion->sancionadoTipo == 'App\Models\Particular'){
            $sancion = ParticularService::get_particular_sancion($sancion);
        }
        
        return $this->jsonResponse("Registro consultado correctamente", $sancion, Response::HTTP_OK, null);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        $sancion = Sancion::findOrFail($request->id);

        $sancion->update($request->all());

        //responsable sancion
        if(isset($request->responsableSancion)){
            //registrar responsable de sancion
            $responsableSancion = isset($request->responsableSancion['id']) && $request->responsableSancion['id'] != 0
                                ? ResponsableSancion::find($request->responsableSancion['id'])
                                : ResponsableSancion::create($request->responsableSancion);

            //asignar a la sancion
            $sancion->responsableSancionId = $responsableSancion->id ?? null;
            $sancion->save();
        }

        //tipos de sancion
        if($request->has('tiposSancion')){
            //eliminar relaciones anteriores
            $sancion->tiposSancion()->detach();
            //obtener ids de tipos de sancion
            $tiposSancion = explode(',', $request->tiposSancion);
            foreach($tiposSancion as $tipoSancionId){
                $tipoSancion = TipoSancion::find($tipoSancionId);
                //crear relacion
                if($tipoSancion){
                    $sancion->tiposSancion()->attach($tipoSancion->id, [
                        'valor' => ($tipoSancion->clave == 'O') ? $request->otroTipoSancion : $tipoSancion->valor,
                        'created_at' => now(),
                        'updated_at' => now(),
                    ]);
                }
            }
        }

        //actualizar fecha de actualización
        $sancion->fechaCaptura = $sancion->updated_at;
        $sancion->save();

        $this->crearLog('Actualizar sanciones', 'actualizar', 'sanciones', $sancion);
        return redirect()->back()->with('success', 'ok');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $sancion = Sancion::findOrFail($id);

        $sancion->delete();

        $this->crearLog('Eliminar sanción', 'eliminar', 'sanciones', $sancion);
        return $this->jsonResponse("Registro Eliminado correctamente", null, Response::HTTP_OK, null);
    }

    /**
     * Display the specified resource.
     */
    public function generate_pdf(string $id)
    {
        //obtener sanción
        $sancion = Sancion::with('sancionado', 'institucionDependencia', 'tipoFalta', 'tiposSancion', 'responsableSancion', 'resolucion', 'inhabilitacion')
                    ->with('documentos.tipoDocumento')
                    ->with('multa.moneda')
                    ->findOrFail($id);

        if($sancion->sancionadoTipo == 'App\Models\ServidorPublico'){
            $sancion['sancionado'] = ServidorPublicoService::get_servidor_publico_with_data($sancion['sancionado']);
        }
        
        //si es particular, cargar relaciones
        if($sancion->sancionadoTipo == 'App\Models\Particular'){
            $sancion = ParticularService::get_particular_sancion($sancion);
        }
        
        //formato para nombre de archivo
        $nombre = 'SANCION_' . $sancion->sancionado->rfc . '_' . $sancion->id . '.pdf';
        
        #return $sancion;
        $pdf = PDF::loadView('pdf.sanciones', get_defined_vars())
                        ->setOption('margin-bottom',2)
                        ->setOption('margin-left', 0)
                        ->setOption('margin-right', 0)
                        ->setOption('margin-top', 2)
                        ->setOption('page-width', '8.5in')
                        ->setOption('page-height', '11in');
                        
        return $pdf->download($nombre);
    }

    public function generate_excel(Request $request)
    {
        return Excel::download(new SancionExport($request), 'sanciones.xlsx');
    }

    public function import_sanciones_servidores_publicos()
    {
        return Excel::download(new SancionExport($request), 'sanciones.xlsx');
    }

    //generar sanciones de servidores públicos
    public function generate_sanciones_servidores_publicos(Request $request)
    {
        //tiempo de ejecución
        ini_set('max_execution_time', 1000);

        //guardar archivo
        $file = $request->file;
        $name = 's3s' . '.'. $file->getClientOriginalExtension();
        
        $file->storeAs(
            'public/files/' . $name
        );
    
        $zipPath = storage_path('app/public/files/' . $name);
        $extractPath = storage_path('app/public/files/s3s');
    
        // Crear una instancia de ZipArchive
        $zip = new ZipArchive;
    
        // Abre el archivo zip
        if ($zip->open($zipPath) === TRUE) {
            // Extraer el contenido en el directorio especificado
            $zip->extractTo($extractPath);
            $zip->close();
    
            // Obtener carpetas del directorio extraído
            $carpetas = $this->get_carpetas($extractPath);
            $contador = 1;
            $i = 0;
    
            //por cada carpeta
            foreach($carpetas as $carpeta){
                $contador = 1;
                //ciclo para obtener ruta de archivos json
                do{
                    $nombreArchivo = $this->generate_nombre_archivo($contador);
                    $rutaArchivo = $extractPath . '/' . $carpeta . '/' . $nombreArchivo;

                    //si existe el archivo obtener los datos
                    if(file_exists($rutaArchivo)){
                        $sanciones = json_decode(file_get_contents($rutaArchivo), true);
                        foreach($sanciones as $sancion){
                            //registrar servidor público
                            if(isset($sancion['servidorPublicoSancionado'])){
                                $servidorPublico = ServidorPublico::create([
                                    'rfc' => $sancion['servidorPublicoSancionado']['rfc'] ?? null,
                                    'nombres' => $sancion['servidorPublicoSancionado']['nombres'] ?? null,
                                    'primerApellido' => $sancion['servidorPublicoSancionado']['primerApellido'] ?? null,
                                    'segundoApellido' => $sancion['servidorPublicoSancionado']['segundoApellido'] ?? null,
                                    'puesto' => $sancion['servidorPublicoSancionado']['puesto'] ?? null,
                                    'nivel' => $sancion['servidorPublicoSancionado']['nivel'] ?? null,
                                ]);
                            }

                            //obtener tipo de falta
                            if(isset($sancion['tipoFalta'])){
                                $tipoFalta = TipoFalta::where('clave', $sancion['tipoFalta']['clave'])->first();
                            }   

                            //registrar institución
                            if(isset($sancion['institucionDependencia'])){
                                //encontrar institución si existe
                                $institucionDependencia = InstitucionDependencia::where('nombre', $sancion['institucionDependencia']['nombre'] ?? null)
                                                                                ->where('siglas', $sancion['institucionDependencia']['siglas'] ?? null)
                                                                                ->where('clave', $sancion['institucionDependencia']['clave'] ?? null)
                                                                                ->first();
                                //si no existe, crearla
                                if(is_null($institucionDependencia)){
                                    $institucionDependencia = InstitucionDependencia::create([
                                        'nombre' => $sancion['institucionDependencia']['nombre'] ?? null,
                                        'siglas' => $sancion['institucionDependencia']['siglas'] ?? null,
                                        'clave' => $sancion['institucionDependencia']['clave'] ?? null,
                                    ]);
                                }
                            }
                            
                            //registrar sancion
                            $nuevaSancion = Sancion::create([
                                'fechaCaptura' => $this->get_fecha($sancion['fechaCaptura'] ?? null),
                                'expediente' => $sancion['expediente'] ?? null,
                                'autoridadSancionadora' => $sancion['autoridadSancionadora'] ?? null,
                                'causaMotivoHechos' => $sancion['causaMotivoHechos'] ?? null,
                                'observaciones' => $sancion['observaciones'] ?? null,
                                'sancionadoTipo' => 'App\Models\ServidorPublico',
                                'sancionadoId' => $servidorPublico->id ?? null,
                                'institucionDependenciaId' => $institucionDependencia->id ?? null,
                                'tipoFaltaId' => $tipoFalta->id ?? null,
                            ]);

                            //resolucion
                            if(isset($sancion['resolucion'])){
                                $resolucion = Resolucion::create([
                                    'url' => $sancion['resolucion']['url'] ?? null,
                                    'fechaResolucion' => $this->get_fecha($sancion['resolucion']['fechaResolucion'] ?? null),
                                    'sancionId' => $nuevaSancion->id,
                                ]);
                            }

                            //inhabilitacion
                            if(isset($sancion['inhabilitacion'])){
                                $inhabilitacion = Inhabilitacion::create([
                                    'plazo' => $sancion['inhabilitacion']['plazo'] ?? null,
                                    'fechaInicial' => $this->get_fecha($sancion['inhabilitacion']['fechaInicial'] ?? null),
                                    'fechaFinal' => $this->get_fecha($sancion['inhabilitacion']['fechaFinal'] ?? null),
                                    'sancionId' => $nuevaSancion->id,
                                ]);
                            }

                            //multa
                            if(isset($sancion['multa'])){
                                //obtener moneda
                                if(isset($sancion['multa']['monto']) && $sancion['multa']['monto'] != '0'){
                                    $moneda = Moneda::where('clave', $sancion['multa']['moneda']['clave'])->first();
                                    $multa = Multa::create([
                                        'monto' => $sancion['multa']['monto'] ?? null,
                                        'monedaId' => $moneda->id ?? null,
                                        'sancionId' => $nuevaSancion->id,
                                    ]);
                                }
                            }

                            //documentos
                            if(isset($sancion['documentos'])){
                                foreach($sancion['documentos'] as $documento){
                                    if(!is_null($documento['tipo'] ?? null) || !is_null($documento['url'] ?? null) || !is_null($documento['titulo'] ?? null)){
                                        $tipoDocumento = TipoDocumento::where('clave', ($documento['tipo'] ?? null))->first();
                                        $documento = Documento::create([
                                            'tipo' => $documento['tipo'] ?? null,
                                            'titulo' => $documento['titulo'] ?? null,
                                            'descripcion' => $documento['descripcion'] ?? null,
                                            'url' => $documento['url'] ?? null,
                                            'fecha' => $this->get_fecha($documento['fecha'] ?? null),
                                            'tipoDocumentoId' => $tipoDocumento->id ?? null,
                                            'sancionId' => $nuevaSancion->id,
                                        ]);
                                    }
                                }
                            }  

                            //tipos de sancion
                            if(isset($sancion['tipoSancion'])){
                                foreach($sancion['tipoSancion'] as $tipoSancion){
                                    $tipoSancion = TipoSancion::where('clave', $tipoSancion['clave'])->first();
                                    if($tipoSancion){
                                        $nuevaSancion->tiposSancion()->attach($tipoSancion->id, [
                                            'valor' => $tipoSancion->valor,
                                            'created_at' => now(),
                                            'updated_at' => now(),
                                        ]);
                                    }
                                }
                            }
                        }
                    }
                    $contador++;
                }while(file_exists($rutaArchivo));
            }
    
        }else{
            redirect()->back()->with('error', 'error');
        }
        return redirect()->route('servidores.publicos.sancionados')->with('success', 'ok');
    }

    //generar sanciones de particulares
    public function generate_sanciones_particulares(Request $request)
    {
        //tiempo de ejecución
        ini_set('max_execution_time', 2000);

        //guardar archivo
        $file = $request->file;
        $name = 's3p' . '.'. $file->getClientOriginalExtension();
        
        $file->storeAs(
            'public/files/' . $name
        );
    
        $zipPath = storage_path('app/public/files/' . $name);
        $extractPath = storage_path('app/public/files/s3p');
    
        // Crear una instancia de ZipArchive
        $zip = new ZipArchive;
    
        // Abre el archivo zip
        if ($zip->open($zipPath) === TRUE) {
            // Extraer el contenido en el directorio especificado
            $zip->extractTo($extractPath);
            $zip->close();
    
            // Obtener carpetas del directorio extraído
            $carpetas = $this->get_carpetas($extractPath);
            $contador = 1;
            $i = 0;
    
            // Resto de tu lógica con las carpetas
            //por cada carpeta
            foreach($carpetas as $carpeta){
                $contador = 1;
                //ciclo para obtener ruta de archivos json
                do{
                    $nombreArchivo = $this->generate_nombre_archivo($contador);
                    $rutaArchivo = $extractPath . '/' . $carpeta . '/' . $nombreArchivo;

                    //si existe el archivo obtener los datos
                    if(file_exists($rutaArchivo)){
                        $sanciones = json_decode(file_get_contents($rutaArchivo), true);
                        foreach($sanciones as $sancion){
                            //obtener tipo persona
                            $tipoPersona = TipoPersona::where('clave', $sancion['particularSancionado']['tipoPersona'] ?? null)
                                                        ->orWhere('valor', $sancion['particularSancionado']['tipoPersona'] ?? null)
                                                        ->first();
                            //validar persona moral
                            if(is_null($tipoPersona)){
                                if(isset($sancion['particularSancionado'])){
                                    $nombre = $sancion['particularSancionado']['nombreRazonSocial'];
                                    if(strpos($nombre, 'S.') !== false){
                                        $tipoPersona = TipoPersona::where('clave', 'M')->first();
                                    }
                                }
                            }

                            //registrar particular
                            if(isset($sancion['particularSancionado'])){
                                $particular = Particular::where('nombreRazonSocial', $sancion['particularSancionado']['nombreRazonSocial'] ?? null)
                                                        ->first();
                                if(is_null($particular)){
                                    $particular = Particular::create([
                                        'rfc' => $sancion['particularSancionado']['rfc'] ?? null,
                                        'nombreRazonSocial' => $sancion['particularSancionado']['nombreRazonSocial'] ?? null,
                                        'objetoSocial' => $sancion['particularSancionado']['objetoSocial'] ?? null,
                                        'tipoPersonaId' => $tipoPersona->id ?? null,
                                    ]);
                                }   
                            }

                            //obtener tipo de falta
                            if(isset($sancion['tipoFalta'])){
                                $tipoFaltaClave = $sancion['tipoFalta']['clave'] ?? ($sancion['tipoFalta'] ?? null);
                                $tipoFalta = TipoFalta::where('clave', $tipoFaltaClave)->orWhere('valor', $tipoFaltaClave)->first();
                            }   

                            //registrar institución
                            if(isset($sancion['institucionDependencia'])){
                                //encontrar institución si existe
                                $institucionDependencia = InstitucionDependencia::where('nombre', $sancion['institucionDependencia']['nombre'] ?? null)
                                                                                ->where('siglas', $sancion['institucionDependencia']['siglas'] ?? null)
                                                                                ->where('clave', $sancionz['institucionDependencia']['clave'] ?? null)
                                                                                ->first();
                                //si no existe, crearla
                                if(is_null($institucionDependencia)){
                                    $institucionDependencia = InstitucionDependencia::create([
                                        'nombre' => $sancion['institucionDependencia']['nombre'] ?? null,
                                        'siglas' => $sancion['institucionDependencia']['siglas'] ?? null,
                                        'clave' => $sancion['institucionDependencia']['clave'] ?? null,
                                    ]);
                                }
                            }
                            
                            //registrar responsable sancion
                            if(isset($sancion['responsableSancion']) && $sancion['responsableSancion']['nombres'] != ''){
                                //encontrar institución si existe
                                $responsableSancion = ResponsableSancion::where('nombres', $sancion['responsableSancion']['nombres'] ?? null)
                                                                        ->where('primerApellido', $sancion['responsableSancion']['primerApellido'] ?? null)
                                                                        ->where('segundoApellido', $sancion['responsableSancion']['segundoApellido'] ?? null)
                                                                        ->first();
                                //si no existe, crearla
                                if(is_null($responsableSancion)){
                                    $responsableSancion = ResponsableSancion::create([
                                        'nombres' => $sancion['responsableSancion']['nombres'] ?? null,
                                        'primerApellido' => $sancion['responsableSancion']['primerApellido'] ?? null,
                                        'segundoApellido' => $sancion['responsableSancion']['segundoApellido'] ?? null,
                                        'institucionDependenciaId' => $institucionDependencia->id ?? null,
                                    ]);
                                }
                            }

                            //apoderado legal
                            if(isset($sancion['particularSancionado']['apoderadoLegal']) && $sancion['particularSancionado']['apoderadoLegal']['nombres'] != ''){
                                $apoderadoLegal = ApoderadoLegal::create([
                                    'nombres' => $sancion['particularSancionado']['apoderadoLegal']['nombres'] ?? null,
                                    'primerApellido' => $sancion['particularSancionado']['apoderadoLegal']['nombres'] ?? null,
                                    'segundoApellido' => $sancion['particularSancionado']['apoderadoLegal']['nombres'] ?? null,
                                    'curp' => $sancion['particularSancionado']['apoderadoLegal']['curp'] ?? null,
                                    'rfc' => $sancion['particularSancionado']['apoderadoLegal']['rfc'] ?? null,
                                    'particularId' => $particular->id ?? null,
                                ]);
                            }

                            //director general
                            if(isset($sancion['particularSancionado']['directorGeneral']) && $sancion['particularSancionado']['directorGeneral']['nombres'] != ''){
                                $directorGeneral = DirectorGeneral::create([
                                    'nombres' => $sancion['particularSancionado']['directorGeneral']['nombres'] ?? null,
                                    'primerApellido' => $sancion['particularSancionado']['directorGeneral']['nombres'] ?? null,
                                    'segundoApellido' => $sancion['particularSancionado']['directorGeneral']['nombres'] ?? null,
                                    'curp' => $sancion['particularSancionado']['directorGeneral']['curp'] ?? null,
                                    'rfc' => $sancion['particularSancionado']['directorGeneral']['rfc'] ?? null,
                                    'particularId' => $particular->id ?? null,
                                ]);
                            }

                            //registrar sancion
                            $nuevaSancion = Sancion::create([
                                'fechaCaptura' => $this->get_fecha($sancion['fechaCaptura'] ?? null),
                                'expediente' => $sancion['expediente'] ?? null,
                                'autoridadSancionadora' => $sancion['autoridadSancionadora'] ?? null,
                                'objetoContrato' => $sancion['objetoContrato'] ?? null,
                                'acto' => $sancion['acto'] ?? null,
                                'causaMotivoHechos' => $sancion['causaMotivoHechos'] ?? null,
                                'observaciones' => $sancion['observaciones'] ?? null,
                                'sancionadoTipo' => 'App\Models\Particular',
                                'sancionadoId' => $particular->id ?? null,
                                'institucionDependenciaId' => $institucionDependencia->id ?? null,
                                'tipoFaltaId' => $tipoFalta->id ?? null,
                                'responsableSancionId' => $responsableSancion->id ?? null,
                            ]);

                            //resolucion
                            if(isset($sancion['resolucion'])){
                                $resolucion = Resolucion::create([
                                    'sentido' => $sancion['sentido'] ?? null,
                                    'url' => $sancion['resolucion']['url'] ?? null,
                                    'fechaNotificacion' => $this->get_fecha($sancion['resolucion']['fechaNotificacion'] ?? null),
                                    'sancionId' => $nuevaSancion->id,
                                ]);
                            }

                            //inhabilitacion
                            if(isset($sancion['inhabilitacion'])){
                                $inhabilitacion = Inhabilitacion::create([
                                    'plazo' => $sancion['inhabilitacion']['plazo'] ?? null,
                                    'fechaInicial' => $this->get_fecha($sancion['inhabilitacion']['fechaInicial'] ?? null),
                                    'fechaFinal' => $this->get_fecha($sancion['inhabilitacion']['fechaFinal'] ?? null),
                                    'sancionId' => $nuevaSancion->id,
                                ]);
                            }

                            //multa
                            if(isset($sancion['multa'])){
                                //obtener moneda
                                if(isset($sancion['multa']['monto']) && $sancion['multa']['monto'] != '0'){
                                    $moneda = Moneda::where('clave', $sancion['multa']['moneda']['clave'])->first();
                                    $multa = Multa::create([
                                        'monto' => $sancion['multa']['monto'] ?? null,
                                        'monedaId' => $moneda->id ?? null,
                                        'sancionId' => $nuevaSancion->id,
                                    ]);
                                }
                            }

                            //documentos
                            if(isset($sancion['documentos'])){
                                foreach($sancion['documentos'] as $documento){
                                    if(!is_null($documento['tipo'] ?? null) || !is_null($documento['url'] ?? null) || !is_null($documento['titulo'] ?? null)){
                                        $tipoDocumento = TipoDocumento::where('clave', ($documento['tipo'] ?? null))->first();
                                        $documento = Documento::create([
                                            'tipo' => $documento['tipo'] ?? null,
                                            'titulo' => $documento['titulo'] ?? null,
                                            'descripcion' => $documento['descripcion'] ?? null,
                                            'url' => $documento['url'] ?? null,
                                            'fecha' => $this->get_fecha($documento['fecha'] ?? null),
                                            'tipoDocumentoId' => $tipoDocumento->id ?? null,
                                            'sancionId' => $nuevaSancion->id,
                                        ]);
                                    }
                                }
                            }  

                            //tipos de sancion
                            if(isset($sancion['tipoSancion'])){
                                foreach($sancion['tipoSancion'] as $tipoSancion){
                                    $tipoSancion = TipoSancion::where('clave', $tipoSancion['clave'])->first();
                                    if($tipoSancion){
                                        $nuevaSancion->tiposSancion()->attach($tipoSancion->id, [
                                            'valor' => $tipoSancion->valor,
                                            'created_at' => now(),
                                            'updated_at' => now(),
                                        ]);
                                    }
                                }
                            }
                        }
                    }
                    $contador++;
                }while(file_exists($rutaArchivo));
            }
        }else{
            redirect()->back()->with('error', 'error');
        }
        
        return redirect()->route('particulares.sancionados')->with('success', 'ok');
    }
    
    //obtener fecha
    public static function get_fecha($fecha)
    {
        if(isset($fecha)){
            if(strlen($fecha) == 10) {
                if (preg_match('/^\d{2}\/\d{2}\/\d{4}$/', $fecha)) {
                    return Carbon::createFromFormat('d/m/Y', $fecha)->startOfDay();
                }else{
                    return Carbon::parse($fecha);
                }
            }else if (strlen($fecha) > 10){
                if($fecha == 'Dato no proporcionado'){
                    return null;
                }
                if (preg_match('/^\d{2}-[A-Z][a-z]{2}-\d{4}$/', $fecha)) {
                    return null;
                }
                if (preg_match('/^\d{2} de [A-Z][a-z]+ de \d{4}$/', $fecha)) {
                    return null;
                }
                if (preg_match('/^\d{2} DE [A-Z]+ DE \d{4}$/', $fecha)) {
                    return null;
                }
                return Carbon::parse($fecha);
            }
        }
        return null;
    }

    //generar nombre de archivo con contador
    public static function generate_nombre_archivo($contador)
    {
        $nombreArchivo = '0' . $contador;

        while(strlen($nombreArchivo)<10){
            $nombreArchivo = '0' . $nombreArchivo;
        }

        $nombreArchivo = 'data-' . $nombreArchivo . '.json';

        return $nombreArchivo;
    }

    //obtener nombres de carpetas
    public function get_carpetas($directory)
    {
        $carpetas = [];
        $archivos = scandir($directory);

        foreach ($archivos as $archivo) {
            if (is_dir($directory . '/' . $archivo) && $archivo != '.' && $archivo != '..') {
                $carpetas[] = $archivo;
            }
        }

        return $carpetas;
    }

    public function get_carpetas_totales()
    {
        $carpetas = [
            'AGUASCALIENTES',
            'BAJA CALIFORNIA',
            'BAJA CALIFORNIA SUR',
            'CAMPECHE',
            'CHIAPAS',
            'CHIHUAHUA',
            'COAHUILA',
            'COLIMA',
            'DURANGO',
            'EDOMEX',
            'GUANAJUATO',
            'GUERRERO',
            'HIDALGO',
            'JALISCO',
            'MICHOACÁN',
            'MORELOS',
            'NAYARIT',
            'NUEVO LEÓN',
            'OAXACA',
            'PUEBLA',
            'QUERÉTARO',
            'QUINTANA ROO',
            'SAN LUIS POTOSÍ',
            'SINALOA',
            'SONORA',
            'TABASCO',
            'TAMAULIPAS',
            'TLAXCALA',
            'VERACRUZ',
            'YUCATÁN',
            'ZACATECAS',
            'INAI',
            'SFP',
        ];

        return $carpetas;
    }

    public function delete_all()
    {
        try {
            DB::table('fines')->delete();
            DB::table('documents')->delete();
            DB::table('resolutions')->delete();
            DB::table('sanction_sanction_type')->delete();
            DB::table('sanctions')->delete();
            DB::table('sanction_imposers')->delete();
            DB::table('individuals')->delete();
            DB::table('public_servants')->delete();
            DB::table('institutions')->delete();

            return redirect()->back()->with('success', 'ok');
            
        } catch (\Exception $e) {
            return 'Error al vaciar las tablas: ' . $e->getMessage();
        }
    }

}