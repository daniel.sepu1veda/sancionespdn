<?php

namespace App\Http\Controllers\Web;

use App\Models\InstitucionDependencia;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Sancion;
use App\Models\User;
use Carbon\Carbon;

class HomeController extends Controller
{
    public function index()
    {
        //si es el primer login del usuario, redirigir a la vista para cambiar contraseña
        if(auth()->user()->firstLogin) return redirect()->route('usuarios.change.password');

        //widgets
        $widgets = $this->get_widgets();
        //sanciones por mes
        $meses = $this->get_meses();
        //instituciones con número de sanciones
        $institucionesDependencias = InstitucionDependencia::whereHas('sanciones')
                                                            ->withCount('sanciones')
                                                            ->orderByDesc('sanciones_count') // 'sanciones_count' es el nombre predeterminado generado por withCount
                                                            ->take(15)
                                                            ->get();
        #return $meses;
        return view('admin.home', get_defined_vars());
    }

    public function get_widgets()
    {
        $widgets = collect();

        $widgets->put('sancionesCount', Sancion::count());
        $widgets->put('servidoresPublicosSancionados', Sancion::where('sancionadoTipo', 'App\Models\ServidorPublico')->count());
        $widgets->put('particularesSancionados', Sancion::where('sancionadoTipo', 'App\Models\Particular')->count());
        $widgets->put('estaSemana', Sancion::where('created_at', '>', Carbon::now()->subDays(7))->count());
        $widgets->put('esteMes', Sancion::whereMonth('created_at', Carbon::now()->format('m'))->count());
        $widgets->put('esteAnio', Sancion::whereYear('created_at', Carbon::now()->format('Y'))->count());

        return $widgets;
    }

    public function get_meses()
    {
        $fechaActual = Carbon::now();
        $fechaInicio = $fechaActual->copy()->subYear()->startOfMonth(); 

        // Establecer la localización en español
        Carbon::setLocale('es');

        $meses = [];

        for ($i = 11; $i >= 0; $i--) {
            $mes = $fechaActual->copy()->subMonths($i)->format('F');
            $meses[] = $mes;
        }

        $resultados = [];

        foreach ($meses as $mes) {
            $numeroMes = Carbon::parse($mes)->month;
            //obtener cantidad de sanciones
            $cantidadSanciones = Sancion::whereBetween('created_at', [$fechaInicio, $fechaActual->endOfMonth(),])
                                ->whereMonth('created_at', $numeroMes)
                                ->when($numeroMes == Carbon::now()->month, function($q){
                                    $q->whereYear('created_at', Carbon::now()->format('Y'));
                                })->count();

            $resultados[] = [
                'mes' => $this->get_nombre_mes($mes),
                'sanciones' => $cantidadSanciones,
            ];
        }

        return $resultados;
    }

    
    public function get_nombre_mes($mes)
    {
        switch ($mes) {
            case 'January':
                return 'Enero';
            case 'February':
                return 'Febrero';
            case 'March':
                return 'Marzo';
            case 'April':
                return 'Abril';
            case 'May':
                return 'Mayo';
            case 'June':
                return 'Junio';
            case 'July':
                return 'Julio';
            case 'August':
                return 'Agosto';
            case 'September':
                return 'Septiembre';
            case 'October':
                return 'Octubre';
            case 'November':
                return 'Noviembre';
            case 'December':
                return 'Diciembre';
            default:
                return $mes;
        }
    }
}
