<?php

namespace App\Http\Controllers\Web;

use SimpleSoftwareIO\QrCode\Facades\QrCode;
use App\Services\ServidorPublicoService;
use App\Http\Controllers\Controller;
use App\Services\DirectorGeneralService;
use App\Services\ApoderadoLegalService;
use App\Services\ConstanciaService;
use App\Services\ParticularService;
use App\Models\ServidorPublico;
use Illuminate\Http\Request;
use App\Models\TipoPersona;
use App\Models\Constancia;
use App\Models\Particular;
use App\Models\Sancion;
use PDF;

class ConstanciaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('admin.constancias.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $folio)
    {
        $constancia = Constancia::where('folio', $folio)
                                ->with('constanciable')
                                ->with('constanciable', function($q){
                                    $q->with('sanciones.institucionDependencia');
                                    $q->with('sanciones.tiposSancion');
                                })
                                ->with('user')
                                ->first();

        #return $constancia;
        return view('admin.constancias.details', get_defined_vars());
    }

    public function create()
    {
        //catálogos
        $tiposPersona = TipoPersona::all();

        return view('admin.constancias.create', get_defined_vars());
    }

    public function generate_constancia(Request $request)
    {
        switch($request->tipo){
            case 'App\Models\ServidorPublico':
                $servidorPublico = ServidorPublico::where('rfc', $request->rfc)
                                                ->with('sanciones')
                                                ->first();
                if(isset($servidorPublico)){
                    $sanciones = $servidorPublico->sanciones;
                    $servidorPublico['historial'] = ServidorPublicoService::get_historial($servidorPublico);
                }else{
                    $servidorPublico = ServidorPublicoService::get_servidor_publico_by_rfc($request->rfc);
                    $servidorPublico['historial'] = ServidorPublicoService::get_historial($servidorPublico);
                    $sanciones = [];
                }
                break;
            case 'App\Models\Particular':
                $particular = Particular::with('sanciones')->find($request->id);
                
                if(isset($particular)){
                    $sanciones = $particular->sanciones;
                }else{
                    $particular = [
                        'rfc' => $request->rfc,
                        'nombreRazonSocial' => $request->nombreRazonSocial
                    ];
                    $sanciones = [];
                }

                break;
            case 'App\Models\ApoderadoLegal':
                $apoderadoLegal = ApoderadoLegal::with('particular.sanciones')->find($request->id);
                
                if(isset($apoderadoLegal)){
                    $particular = $apoderadoLegal->particular;
                    $sanciones = $apoderadoLegal->particular->sanciones;
                }
                break;
            case 'App\Models\DirectorGeneral':
                $directorGeneral = DirectorGeneral::with('particular.sanciones')->find($request->id);
                
                if(isset($directorGeneral)){
                    $particular = $directorGeneral->particular;
                    $sanciones = $directorGeneral->particular->sanciones;
                }
            default:
                break;
        }

        if(isset($sanciones) && !empty($sanciones)){
            $sancion = $sanciones->first();
            $sancion->load('sancionado', 'institucionDependencia', 'tipoFalta', 'tiposSancion', 'responsableSancion', 'resolucion', 'inhabilitacion', 'documentos.tipoDocumento', 'multa.moneda');

            //si es servidor, cargar relaciones
            if($sancion->sancionadoTipo == 'App\Models\ServidorPublico'){
                $sancion['sancionado'] = ServidorPublicoService::get_servidor_publico_with_data($sancion['sancionado']);
            }

            //si es particular, cargar relaciones
            if($sancion->sancionadoTipo == 'App\Models\Particular'){
                $sancion = ParticularService::get_particular_sancion($sancion);
            }
        }else{
            $sancion = null;
        }

        //crear registro de constancia
        $constancia = Constancia::create([
            'rfc' => $request->rfc ?? null,
            'folio' => ConstanciaService::generate_folio(),
            'sancionado' => isset($sancion),
            'constanciableTipo' => isset($sancion) ? $sancion->sancionadoTipo : null,
            'constanciableId' => isset($sancion) ? $sancion->sancionadoId : null,
            'userId' => auth()->user()->id,
        ]);

        //obtener url
        $constancia->append('url');

        //log
        $this->crearLog('Generar constancia', 'registro', 'constancias', $constancia);
            
        #return view('pdf.constancias', get_defined_vars());
        $pdf = PDF::loadView('pdf.constancias', get_defined_vars());
        return $pdf->download('CONSTANCIA_' . $constancia->folio .  '.pdf');
    }

    public function get_all()
    {
        $constancias = Constancia::with('constanciable', 'user')->paginate(10);
        
        return $constancias;
    }

    public function busqueda_estatal(Request $request)
    {
        $servidoresPublicos = ServidorPublicoService::get_by_nombres($request);
        $particulares = ParticularService::get_by_nombres($request);
        $apoderadosLegales = ApoderadoLegalService::get_by_nombres($request);
        $directoresGenerales = DirectorGeneralService::get_by_nombres($request);

        $resultados = $servidoresPublicos
            ->merge($particulares)
            ->merge($apoderadosLegales)
            ->merge($directoresGenerales);

        //para pruebas
        foreach($resultados as $resultado){
            $resultado['nacional'] = true;
        }

        return $resultados;
    }

    public function busqueda_nacional(Request $request)
    {
        $servidoresPublicos = ServidorPublicoService::get_by_nombres($request);
        $particulares = ParticularService::get_by_nombres($request);
        $apoderadosLegales = ApoderadoLegalService::get_by_nombres($request);
        $directoresGenerales = DirectorGeneralService::get_by_nombres($request);

        $resultados = $servidoresPublicos
            ->merge($particulares)
            ->merge($apoderadosLegales)
            ->merge($directoresGenerales);

        //para pruebas
        foreach($resultados as $resultado){
            $resultado['nacional'] = true;
        }

        return $resultados;
    }
}
