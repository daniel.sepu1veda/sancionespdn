<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\InstitucionDependencia;
use App\Models\EstadoNotificacion;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Models\Notificacion;
use App\Models\User;

class NotificacionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //permiso
        $notificacionesDelete = auth()->check() ? auth()->user()->rol->hasPermissionTo('notificaciones.delete') : false;

        $this->crearLog('Consultar notificaciones', 'consultar', 'notificaciones', null);
        return view('admin.notificaciones.index', get_defined_vars());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //por cada destinatario
        if($request->has('destinatarios')){
            $destinatarios = explode(',', $request->destinatarios);
            foreach($destinatarios as $destinatarioId){
                //registrar notificación
                $notificacion = Notificacion::create([
                    'descripcion' => $request->descripcion,
                    'texto' => $request->texto,
                    'url' => $request->url,
                    'usuarioId' => auth()->user()->id,
                    'destinatarioId' => $destinatarioId,
                    'estadoNotificacionId' => 1,
                ]);
            }
        }

        $this->crearLog('Registro notificaciones', 'registro', 'notificaciones', $notificacion);
        return redirect()->route('notificaciones')->with('success', 'ok');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $notificacion = Notificacion::with('usuario', 'destinatario', 'estadoNotificacion')->findOrFail($id);

        //catálogos
        $estadosNotificacion = EstadoNotificacion::where('id', '<>', 1)->get();
        $usuarios = User::where('id', '<>', 1)->get();
        
        //si el usuario vio la notificación marcarla como leída
        if($notificacion->destinatarioId == auth()->user()->id &&
            $notificacion->estadoNotificacionId == 1){
            $notificacion->estadoNotificacionId = 2;
            $notificacion->save();
        }

        #return $notificacion;
        $this->crearLog('Consultar notificaciones', 'consultar', 'notificaciones', $notificacion);
        return view('admin.notificaciones.details', get_defined_vars());
    }

    /**
     * Display the specified resource.
     */
    public function get(string $id)
    {
        $notificacion = Notificacion::with('usuario', 'destinatario')->findOrFail($id);

        #return $notificacion;
        $this->crearLog('Consultar notificaciones', 'consultar', 'notificaciones', $notificacion);
        return $this->jsonResponse("Registro consultado correctamente", $notificacion, Response::HTTP_OK, null);
    }

    /**
     * Display the specified resource.
     */
    public function create()
    {
        //catálogos
        $estadosNotificacion = EstadoNotificacion::all();
        $institucionesDependencias = InstitucionDependencia::with(['usuarios' => function($q) {
                                                            $q->where('users.id', '<>', 1);
                                                        }])->get();

        #return $institucionesDependencias;
        return view('admin.notificaciones.create', get_defined_vars());
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        $notificacion = Notificacion::findOrFail($request->id);

        $notificacion->update($request->all());

        $this->crearLog('Actualizar notificaciones', 'actualizar', 'notificaciones', $notificacion);
        return redirect()->back()->with('success', 'ok');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $notificacion = Notificacion::findOrFail($id);

        $notificacion->delete();

        $this->crearLog('Eliminar notificaciones', 'eliminar', 'notificaciones', $notificacion);
        return $this->jsonResponse("Registro eliminado correctamente", $notificacion, Response::HTTP_OK, null);
    }

    public function get_all()
    {
        //obtener
        $notificaciones = Notificacion::select('id', 'descripcion', 'usuarioId', 'destinatarioId', 'estadoNotificacionId', 'created_at')
                                        ->with('usuario', 'destinatario', 'estadoNotificacion')
                                        ->orderBy('id', 'DESC');

        //validación
        if(!auth()->user()->rol->hasPermissionTo('notificaciones.add')){
            $notiicaciones = $notificaciones->where('destinatarioId', auth()->user()->id);
        }

        //notificaciones paginadas
        $notificaciones = $notificaciones->paginate(10);
                                        
        //agregar el nombre de la institución
        foreach($notificaciones->getCollection() as $notificacion){
            $notificacion->destinatario->append('nombre_institucion');
        }

        return $notificaciones;
    }
}
