<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Models\Documento;

class DocumentoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $documentos = Documento::with('tipoDocumento', 'sancion')->get();

        #return $documentos;
        $this->crearLog('Consultar documentos', 'consultar', 'documentos', null);
        return view('admin.documentos.index', get_defined_vars());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $documento = Documento::create($request->all());

        $this->crearLog('Registro documentos', 'registro', 'documentos', $documento);
        return redirect()->back()->with('success', 'ok');
    }

    /**
     * Display the specified resource.
     */
    public function get(string $id)
    {
        $documento = Documento::with('tipoDocumento', 'sancion')->findOrFail($id);

        #return $documento;
        $this->crearLog('Consultar documentos', 'consultar', 'documentos', $documento);
        return $this->jsonResponse("Registro consultado correctamente", $documento, Response::HTTP_OK, null);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        $documento = Documento::findOrFail($request->id);

        $documento->update($request->all());

        $this->crearLog('Actualizar documentos', 'actualizar', 'documentos', $documento);
        return redirect()->back()->with('success', 'ok');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $documento = Documento::findOrFail($id);

        $documento->delete();

        $this->crearLog('Eliminar documentos', 'eliminar', 'documentos', $documento);
        return $this->jsonResponse("Registro eliminado correctamente", $documento, Response::HTTP_OK, null);
    }
}