<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Imports\SancionServidorPublicoImportValidator;
use App\Exports\SancionServidorPublicoExport;
use App\Imports\SancionServidorPublicoImport;
use App\Services\ServidorPublicoService;
use App\Models\InstitucionDependencia;
use App\Models\ResponsableSancion;
use App\Models\ServidorPublico;
use App\Services\FiltroService;
use App\Models\TipoSancion;
use App\Models\TipoFalta;
use App\Models\Sancion;
use Carbon\Carbon;
use Excel;

class ServidorPublicoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //catálogos
        $institucionesDependencias = InstitucionDependencia::all();

        //permiso
        $sancionesAdd = auth()->check() ? auth()->user()->rol->hasPermissionTo('sanciones.add') : false;

        return view('admin.servidores.index', get_defined_vars());
    }

    /**
     * Display a listing of the resource.
     */
    public function sancionados()
    {
        //catálogos
        $institucionesDependencias = InstitucionDependencia::all();
        $responsablesSancion = ResponsableSancion::all();
        $tiposSancion = TipoSancion::all();
        $tiposFalta = TipoFalta::all();
        
        //permiso
        $sancionesDelete = auth()->check() ? auth()->user()->rol->hasPermissionTo('sanciones.delete') : false;

        return view('admin.servidores.sancionados', get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $rfc)
    {
        //obtener servidor publico
        $servidorPublico = ServidorPublicoService::get_servidor_publico_institucion_dependencia($rfc);
        
        //obtener sanciones
        $servidorPublico['sanciones'] = Sancion::where('sancionadoTipo', 'App\Models\ServidorPublico')
                                            ->with('institucionDependencia', 'tipoFalta', 'tiposSancion')
                                            ->whereHas('sancionado', function($q) use ($rfc) {
                                                $q->where('rfc', $rfc);
                                            })
                                            ->get();

        #return $servidorPublico;
        return view('admin.servidores.details', get_defined_vars());
    }

    /**
     * Display the specified resource.
     */
    public function get(string $id)
    {
        $servidorPublico = ServidorPublicoService::get_servidor_publico($id);
                                                   
        return $servidorPublico; 
    }

    /**
     * Display the specified resource.
     */
    public function get_by_rfc(string $rfc)
    {
        //validar rfc 
        if(!ServidorPublicoService::validate_rfc($rfc)) return $this->jsonResponse("Formato de RFC incorrecto", null, Response::HTTP_UNPROCESSABLE_ENTITY, null);

        //obtener datos
        $servidorPublico = ServidorPublicoService::get_servidor_publico_by_rfc($rfc);

        //respuesta satisfactoria
        if($servidorPublico) return $this->jsonResponse("Registro consultado correctamente", $servidorPublico, Response::HTTP_OK, null);

        //respuesta negativa
        return $this->jsonResponse("Registro no encontrado", null, Response::HTTP_NOT_FOUND, null);
    }

    /**
     * Display the specified resource.
     */
    public function get_by_nombres(Request $request)
    {
        //obtener datos
        $servidoresPublicos = ServidorPublicoService::get_by_nombres($request);
        return $servidoresPublicos;
    }

    /**
     * Display the specified resource.
     */
    public function get_historial(string $rfc)
    {
        //validar rfc 
        if(!ServidorPublicoService::validate_rfc($rfc)) return $this->jsonResponse("Formato de RFC incorrecto", null, Response::HTTP_UNPROCESSABLE_ENTITY, null);

        //obtener datos
        $servidorPublico = ServidorPublicoService::get_servidor_publico_by_rfc($rfc);
                            
        $servidorPublico['historial'] = ServidorPublicoService::get_historial($servidorPublico);

        return $servidorPublico; 
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        $servidorPublico = ServidorPublico::findOrFail($request->id);

        $servidorPublico->update($request->all());

        //actualizar sancion
        if($request->has('sancion')){
            $sancion = Sancion::find($request->sancion['id']);
            if($sancion){
                $sancion->update($request->sancion);
            }
        }

        $this->crearLog('Actualizar servidores publicos', 'actualizar', 'servidores-publicos', $servidorPublico);
        return redirect()->back()->with('success', 'ok');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $servidorPublico = ServidorPublico::findOrFail($id);

        $servidorPublico->delete();

        $this->crearLog('Eliminar servidores publicos', 'eliminar', 'servidores-publicos', $servidorPublico);
        return $this->jsonResponse("Registro eliminado correctamente", $servidorPublico, Response::HTTP_OK, null);
    }

    /**
     * Display a listing of the resource.
     */
    public function list()
    {
        $servidoresPublicos = ServidorPublicoService::get_servidores_publicos();
        
        return $servidoresPublicos; 
    }

/**
     * Display a listing of the resource.
     */
    public function get_all(Request $request)
    {
        $servidoresPublicos = ServidorPublico::query()
                            ->when(isset($request->rfc), function($q) use($request){
                                $q->where('rfc', $request->rfc);
                            })
                            ->when(isset($request->institucionesIds), function($q) use ($request){
                                $q->whereHas('sanciones', function($q) use ($request){
                                    $q->whereIn('institucionDependenciaId', $request->institucionesIds);
                                });
                            })
                            ->paginate(10);
        
        foreach($servidoresPublicos->getCollection() as $servidorPublico){
            $servidorPublico->append('nombre_completo');
            $servidorPublico->load('sanciones');
            $servidorPublico['institucionDependencia'] = $servidorPublico->sanciones->first()->institucionDependencia;
            unset($servidorPublico->sanciones);
        }

        return $servidoresPublicos;
    }

    /**
     * Display a listing of the resource.
     */
    public function get_all_sancionados(Request $request)
    {
        //obtener request con fechas
        $request = isset($request->rango) ? FiltroService::get_fechas($request) : $request;

        //obtener servidores sancionados con filtros
        $servidoresSancionados =  Sancion::where('sancionadoTipo', 'App\Models\ServidorPublico')
                                    ->when(isset($request->institucionesIds), function($q) use($request){
                                        $q->whereIn('institucionDependenciaId', $request->institucionesIds);
                                    })
                                    ->when(isset($request->tiposFaltaIds), function($q) use($request){
                                        $q->whereIn('tipoFaltaId', $request->tiposFaltaIds);
                                    })
                                    ->when(isset($request->responsablesSancionIds), function($q) use($request){
                                        $q->whereIn('responsableSancionId', $request->responsablesSancionIds);
                                    })
                                    ->when(isset($request->tiposSancionIds), function($q) use($request){
                                        $q->whereHas('tiposSancion', function($q) use($request){
                                            $q->whereIn('tipoSancionId', $request->tiposSancionIds);
                                        });
                                    })
                                    ->when(isset($request->rfc), function($q) use($request){
                                        $q->whereHas('sancionado', function($q) use($request){
                                            $q->where('rfc', $request->rfc);
                                        });
                                    })
                                    ->when(isset($request->fechaInicial) && isset($request->fechaFinal), function($q) use($request){
                                        $q->whereHas('resolucion', function($q) use($request){
                                            $q->whereBetween('fechaResolucion', [$request->fechaInicial, $request->fechaFinal]);
                                        });
                                    })
                                    ->with('resolucion', 'sancionado', 'institucionDependencia', 'tipoFalta', 'tiposSancion')
                                    ->paginate(10);
        
        foreach($servidoresSancionados->getCollection() as $servidorSancionado){
            $servidorSancionado->sancionado->append('nombre_completo');
        }
        
        return $servidoresSancionados;
    }

    //función para mostrar vista de carga masiva
    public function carga_masiva()
    {
        //catálogos
        $institucionesDependencias = InstitucionDependencia::all();
        $tiposSancion = TipoSancion::all();
        $tiposFalta = TipoFalta::all();

        return view('admin.servidores.carga', get_defined_vars());
    }

    //export
    public function export_sanciones(Request $request)
    {
        return Excel::download(new SancionServidorPublicoExport($request), 'sanciones-servidores-publicos.xlsx');
    }

    //import
    public function import_sanciones(Request $request)
    {
        //validar archivo
        Excel::import(new SancionServidorPublicoImportValidator, request()->file('file'));
        //obtener log
        $log = session('log');
        //si existe log redireccionar a vista
        if(isset($log)){
            //eliminar últimos 3 caracteres
            $log = substr($log, 0, -3);
            return view('admin.servidores.carga', get_defined_vars());
        }
        //si no existe log realizar importación
        Excel::import(new SancionServidorPublicoImport, request()->file('file'));

        return redirect()->route('servidores.publicos.sancionados')->with('success', 'ok');
    }

    //obtener log
    public static function get_import_log()
    {
        if(!is_null(session('log'))){
            $log = explode('&&&', session('log'));
            $text = implode(PHP_EOL, $log);

            // Guardar la cadena de texto en un archivo .txt
            $filename = 'log_import_' . Carbon::now()->format('Y_m_d_H_i_s') . '.txt';
            $filepath = storage_path('logs/' . $filename);
            file_put_contents($filepath, $text);

            // Descargar el archivo
            if (file_exists($filepath)) {
                session(['log' => null]);
                return response()->download($filepath);
            }
        }
        
        return redirect()->back()->with('errors','error servidor');
    }
}
