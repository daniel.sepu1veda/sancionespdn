<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Models\Multa;

class MultaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $multas = Multa::with('moneda', 'sancion')->get();

        #return $multas;
        $this->crearLog('Consultar multas', 'consultar', 'multas', null);
        return view('admin.multas.index', get_defined_vars());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $multa = Multa::create($request->all());

        $this->crearLog('Registro multas', 'registro', 'multas', $multa);
        return redirect()->back()->with('success', 'ok');
    }

    /**
     * Display the specified resource.
     */
    public function get(string $id)
    {
        $multa = Multa::with('moneda', 'sancion')->findOrFail($id);

        #return $multa;
        $this->crearLog('Consultar multas', 'consultar', 'multas', $multa);
        return $this->jsonResponse("Registro consultado correctamente", $multa, Response::HTTP_OK, null);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        $multa = Multa::findOrFail($request->id);

        $multa->update($request->all());

        $this->crearLog('Actualizar multas', 'actualizar', 'multas', $multa);
        return redirect()->back()->with('success', 'ok');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $multa = Multa::findOrFail($id);

        $multa->delete();

        $this->crearLog('Eliminar multas', 'eliminar', 'multas', $multa);
        return $this->jsonResponse("Registro eliminado correctamente", $multa, Response::HTTP_OK, null);
    }
}
