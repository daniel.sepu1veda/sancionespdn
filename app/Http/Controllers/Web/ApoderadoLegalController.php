<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Models\ApoderadoLegal;

class ApoderadoLegalController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $apoderadosLegales = ApoderadoLegal::with('particular')->get();

        #return $apoderadosLegales;
        $this->crearLog('Consultar apoderados legales', 'consultar', 'apoderados-legales', null);
        return view('admin.apoderados-legales.index', get_defined_vars());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $apoderadoLegal = ApoderadoLegal::create($request->all());

        if(isset($request->sancionId)){
            $apoderadoLegal->sancion()->attach($request->sancionId, [
                'created_at' => now(),
                'updated_at' => now()
            ]);
        }
        
        $this->crearLog('Registro apoderados legales', 'registro', 'apoderados-legales', $apoderadoLegal);
        return redirect()->back()->with('success', 'ok');
    }

    /**
     * Display the specified resource.
     */
    public function get(string $id)
    {
        $apoderadoLegal = ApoderadoLegal::findOrFail($id);

        #return $apoderadoLegal;
        $this->crearLog('Consultar apoderados legales', 'consultar', 'apoderados-legales', $apoderadoLegal);
        return $this->jsonResponse("Registro consultado correctamente", $apoderadoLegal, Response::HTTP_OK, null);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        $apoderadoLegal = ApoderadoLegal::findOrFail($request->id);

        $apoderadoLegal->update($request->all());

        $this->crearLog('Actualizar apoderados legales', 'actualizar', 'apoderados-legales', $apoderadoLegal);
        return redirect()->back()->with('success', 'ok');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $apoderadoLegal = ApoderadoLegal::findOrFail($id);

        $apoderadoLegal->delete();

        $this->crearLog('Eliminar apoderados legales', 'eliminar', 'apoderados-legales', $apoderadoLegal);
        return $this->jsonResponse("Registro eliminado correctamente", $apoderadoLegal, Response::HTTP_OK, null);
    }
}
