<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Services\FiltroService;
use App\Services\RequestService;
use App\Services\ParticularService;
use App\Models\InstitucionDependencia;
use App\Models\DomicilioExtranjero;
use App\Models\ResponsableSancion;
use App\Models\DomicilioMexico;
use App\Models\DirectorGeneral;
use App\Models\ApoderadoLegal;
use App\Models\TipoSancion;
use App\Models\TipoPersona;
use App\Models\Particular;
use App\Models\TipoFalta;
use App\Models\Localidad;
use App\Models\Vialidad;
use App\Models\Sancion;

class ParticularController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //catálogos
        $institucionesDependencias = InstitucionDependencia::all();
        $responsablesSancion = ResponsableSancion::all();
        $tiposSancion = TipoSancion::all();
        $tiposFalta = TipoFalta::all();

        //permiso
        $sancionesAdd = auth()->check() ? auth()->user()->rol->hasPermissionTo('sanciones.add') : false;

        $this->crearLog('Consulta particulares', 'consulta', 'particulares', null);
        return view('admin.particulares.index', get_defined_vars());
    }

    /**
     * Display a listing of the resource.
     */
    public function sancionados()
    {
        //catálogos
        $institucionesDependencias = InstitucionDependencia::all();
        $responsablesSancion = ResponsableSancion::all();
        $tiposSancion = TipoSancion::all();
        $tiposFalta = TipoFalta::all();

        //permiso
        $sancionesDelete = auth()->check() ? auth()->user()->rol->hasPermissionTo('sanciones.delete') : false;

        #return $particularesSancionados; 
        $this->crearLog('Consulta particulares', 'consulta', 'particulares', null);
        return view('admin.particulares.sancionados', get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {   
        //catálogos
        $institucionesDependencias = InstitucionDependencia::all();
        $tiposFalta = TipoFalta::all();
        $tiposSancion = TipoSancion::all();
        $tiposPersona = TipoPersona::all();
        
        #return $tiposPersona;
        return view('admin.particulares.create-edit', get_defined_vars());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //crear particular
        $particular = Particular::create($request->all());

        //apoderado legal
        if($request->has('apoderadoLegal')){
            $apoderadoLegal = ApoderadoLegal::create($request->apoderadoLegal);
            $apoderadoLegal->particularId = $particular->id;
            $apoderadoLegal->save();
        } 

        //director general
        if($request->has('directorGeneral')){
            $directorGeneral = DirectorGeneral::create($request->directorGeneral);
            $directorGeneral->particularId = $particular->id;
            $directorGeneral->save();
        } 

        //domicilio mexico
        if($request->has('domicilioMexico')){
            $domicilio = DomicilioMexico::create($request->domicilioMexico);

            //localidad
            if(isset($request->domicilioMexico['localidad'])){
                $localidad = Localidad::create($request->domicilioMexico['localidad']);
                $domicilio->localidadId = $localidad->id;
            }

            //vialidad
            if(isset($request->domicilioMexico['vialidad'])){
                $vialidad = Vialidad::create($request->domicilioMexico['vialidad']);
                $domicilio->vialidadId = $vialidad->id;
            }

            //asignar relaciones
            $domicilio->particularId = $particular->id;
            $domicilio->save();
        } 

        //domicilio extranjero
        if($request->has('domicilioExtranjero')){
            $domicilio = DomicilioExtranjero::create($request->domicilioExtranjero);
            $domicilio->particularId = $particular->id;
            $domicilio->save();
        }

        $this->crearLog('Registro particulares', 'registro', 'particulares', $particular);
        return redirect()->back()->with('success', 'ok');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $particular = Particular::with('tipoPersona', 'apoderadosLegales', 'directoresGenerales')
                                ->with('domiciliosMexico', function ($q){
                                    $q->with('vialidad');
                                }) 
                                ->with('domiciliosExtranjero')
                                ->with('sanciones', function ($q){
                                    $q->with('institucionDependencia', 'tipoFalta', 'tiposSancion', 'responsableSancion');
                                })
                                ->findOrFail($id);
                                
        #return $particular;
        $this->crearLog('Consultar particulares', 'consultar', 'particulares', $particular);
        return view('admin.particulares.details', get_defined_vars());
    }

    /**
     * Display the specified resource.
     */
    public function get(string $id)
    {
        $particular = Particular::with('tipoPersona', 'apoderadosLegales', 'directoresGenerales')
                                ->with('domiciliosMexico', function ($q){
                                    $q->with('vialidad');
                                }) 
                                ->with('domiciliosExtranjero')
                                ->with('sanciones', function ($q){
                                    $q->with('institucionDependencia', 'tipoFalta', 'tiposSancion', 'responsableSancion');
                                })
                                ->findOrFail($id);
                                
        #return $particular;
        $this->crearLog('Consultar particulares', 'consultar', 'particulares', $particular);
        return $this->jsonResponse("Registro consultado correctamente", $particular, Response::HTTP_OK, null);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //catálogos 
        $institucionesDependencias = InstitucionDependencia::all();
        $tiposFalta = TipoFalta::all();
        $tiposSancion = TipoSancion::all();
        $tiposPersona = TipoPersona::all();

        return view('admin.particulares.create-edit', get_defined_vars());
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        //obtener particular
        $particular = Particular::findOrFail($request->id);

        //actualizar particular
        $particular->update($request->all());

        $this->crearLog('Actualizar particulares', 'actualizar', 'particulares', $particular);
        return redirect()->back()->with('success', 'ok');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        //obtener particular
        $particular = Particular::with('domiciliosMexico', 'domiciliosExtranjero', 'sanciones')->findOrFail($id);

        //eliminar relaciones
        $particular->apoderadosLegales()->delete();
        $particular->directoresGenerales()->delete();
        $particular->domiciliosMexico()->delete();
        $particular->domiciliosExtranjero()->delete();
        $particular->sanciones()->delete();

        //eliminar particular
        $particular->delete();
        
        $this->crearLog('Eliminar particulares', 'eliminar', 'particulares', $particular);
        return redirect()->back()->with('success', 'ok');
    }

    //lista
    public function list()
    {
        $particulares = Particular::with('tipoPersona')
                                ->withCount('sanciones')
                                ->get();

        #return $particulares; 
        $this->crearLog('Consulta particulares', 'consulta', 'particulares', null);
        return view('admin.particulares.list', get_defined_vars());
    }

    public function store_test()
    {
        //request de prueba
        $request = RequestService::particular_store_request();

        //crear particular
        $particular = ParticularService::store($request);
        
        return $particular;
    }

    public function update_test()
    {
        //request de prueba
        $request = RequestService::particular_update_request(1);
        
        //actualizar particular
        $particular = ParticularService::update($request);

        return $particular;
    }

    public function get_all(Request $request)
    {
        $particulares = Particular::with('tipoPersona')
                                ->when(isset($request->rfc), function($q) use($request){
                                    $q->where('rfc', $request->rfc);
                                })
                                ->withCount('sanciones')
                                ->paginate(10);
                            
        return $particulares;
    }

    public function get_all_sancionados(Request $request)
    {
        //obtener request con fechas
        $request = isset($request->rango) ? FiltroService::get_fechas($request) : $request;

        $particularesSancionados =  Sancion::where('sancionadoTipo', 'App\Models\Particular')
                                    ->when(isset($request->institucionesIds), function($q) use($request){
                                        $q->whereIn('institucionDependenciaId', $request->institucionesIds);
                                    })
                                    ->when(isset($request->tiposFaltaIds), function($q) use($request){
                                        $q->whereIn('tipoFaltaId', $request->tiposFaltaIds);
                                    })
                                    ->when(isset($request->responsablesSancionIds), function($q) use($request){
                                        $q->whereIn('responsableSancionId', $request->responsablesSancionIds);
                                    })
                                    ->when(isset($request->tiposSancionIds), function($q) use($request){
                                        $q->whereHas('tiposSancion', function($q) use($request){
                                            $q->whereIn('tipoSancionId', $request->tiposSancionIds);
                                        });
                                    })
                                    ->when(isset($request->rfc), function($q) use($request){
                                        $q->whereHas('sancionado', function($q) use($request){
                                            $q->where('rfc', $request->rfc);
                                        });
                                    })
                                    ->when(isset($request->fechaInicial) && isset($request->fechaFinal), function($q) use($request){
                                        $q->whereHas('resolucion', function($q) use($request){
                                            $q->whereBetween('fechaNotificacion', [$request->fechaInicial, $request->fechaFinal]);
                                        });
                                    })
                                    ->with('resolucion', 'institucionDependencia', 'tipoFalta', 'tiposSancion')
                                    ->with('sancionado')
                                    ->get();
                                   
        return $particularesSancionados;
    }

    //función para mostrar vista de carga masiva
    public function carga_masiva()
    {
        return view('admin.particulares.carga', get_defined_vars());
    }

    //export
    public function export_sanciones(Request $request)
    {
        return Excel::download(new SancionParticularExport($request), 'sanciones-particulares.xlsx');
    }

    //import
    public function import_sanciones(Request $request)
    {
        Excel::import(new SancionParticularImport, request()->file('file'));

        return redirect()->route('particulares.sancionados')->with('success', 'ok');
    }

}
