<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Models\DomicilioExtranjero;

class DomicilioExtranjeroController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $domiciliosExtranjero = DomicilioExtranjero::get();
        
        #return $domiciliosExtranjero;
        $this->crearLog('Consultar domicilios extranjero', 'consultar', 'domicilios-extranjero', null);
        return view('admin.domicilios-extranjero.index', get_defined_vars());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $domicilioExtranjero = DomicilioExtranjero::create($request->all());

        $this->crearLog('Registro domicilios extranjero', 'registro', 'domicilios-extranjero', $domicilioExtranjero);
        return redirect()->back()->with('success', 'ok');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function get(string $id)
    {
        $domicilioExtranjero = DomicilioExtranjero::with('particular')->findOrFail($id);
        
        #return $domicilioExtranjero;
        $this->crearLog('Consultar domicilios extranjero', 'consultar', 'domicilios-extranjero', $domicilioExtranjero);
        return $this->jsonResponse("Registro consultado correctamente", $domicilioExtranjero, Response::HTTP_OK, null);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        $domicilioExtranjero = DomicilioExtranjero::findOrFail($request->id);

        $domicilioExtranjero->update($request->all());

        $this->crearLog('Actualizar domicilios extranjero', 'actualizar', 'domicilios-extranjero', $domicilioExtranjero);
        return redirect()->back()->with('success', 'ok');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $domicilioExtranjero = DomicilioExtranjero::findOrFail($id);

        $domicilioExtranjero->delete();

        $this->crearLog('Eliminar domicilios extranjero', 'eliminar', 'domicilios-extranjero', $domicilioExtranjero);
        return $this->jsonResponse("Registro eliminado correctamente", $domicilioExtranjero, Response::HTTP_OK, null);
    }
}
