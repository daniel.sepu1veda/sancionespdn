<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Models\Inhabilitacion;

class InhabilitacionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $inhabilitaciones = Inhabilitacion::with('sancion')->get();

        #return $inhabilitaciones;
        $this->crearLog('Consultar inhabilitaciones', 'consultar', 'inhabilitaciones', null);
        return view('admin.inhabilitaciones.index', get_defined_vars());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $inhabilitacion = Inhabilitacion::create($request->all());

        $this->crearLog('Registro inhabilitaciones', 'registro', 'inhabilitaciones', $inhabilitacion);
        return redirect()->back()->with('success', 'ok');
    }

    /**
     * Display the specified resource.
     */
    public function get(string $id)
    {
        $inhabilitacion = Inhabilitacion::with('sancion')->findOrFail($id);

        #return $inhabilitacion;
        $this->crearLog('Consultar inhabilitaciones', 'consultar', 'inhabilitaciones', $inhabilitacion);
        return $this->jsonResponse("Registro consultado correctamente", $inhabilitacion, Response::HTTP_OK, null);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        $inhabilitacion = Inhabilitacion::findOrFail($request->id);

        $inhabilitacion->update($request->all());

        $this->crearLog('Actualizar inhabilitaciones', 'actualizar', 'inhabilitaciones', $inhabilitacion);
        return redirect()->back()->with('success', 'ok');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $inhabilitacion = Inhabilitacion::findOrFail($id);

        $inhabilitacion->delete();

        $this->crearLog('Eliminar inhabilitaciones', 'eliminar', 'inhabilitaciones', $inhabilitacion);
        return $this->jsonResponse("Registro eliminado correctamente", $inhabilitacion, Response::HTTP_OK, null);
    }
}
