<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Particular;
use App\Models\Sancion;
use App\Models\User;
use App\Models\Log;

class LogController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $this->crearLog('Consultar logs', 'consultar', 'logs', null);
        return view('admin.logs.index', get_defined_vars());
    }

    public function get_all()
    {
        //obtener instituciones relacionadas al usuario
        $institucionesDependencias = auth()->user()->institucionesDependencias;

        //obtener usuarios relacionados con la institucion
        $institucionesDependencias->load('usuarios');

        //filtrar usuarios, si no es admin no ve el usuario
        if(auth()->user()->roleId == 1){
            $users = $institucionesDependencias->pluck('usuarios')->flatten()->unique('id')->values()->all();
        }else{
            $users = $institucionesDependencias->pluck('usuarios')->flatten()->unique('id')->reject(function ($user) {
                return $user['role_id'] === 1;
            })->values()->all();
        }
        
        //obtener arreglo de ids
        $usersIds = array_column($users, 'id');

        //obtener logs
        $logs = Log::whereIn('userId', $usersIds)
                   ->with('usuario.institucionesDependencias')
                   ->with(['logeable' => function ($query) {
                        $query->withTrashed();
                    }])
                   ->orderBy('created_at', 'desc')
                   ->paginate(10);

        //cargar relaciones de logeables
        foreach($logs->getCollection() as $log){
            if($log->logeableTipo == 'App\Models\DomicilioMexico' ||
               $log->logeableTipo == 'App\Models\DomicilioExtranjero' || 
               $log->logeableTipo == 'App\Models\ApoderadoLegal' ||
               $log->logeableTipo == 'App\Models\DirectorGeneral'){
                $log->logeable->load('particular');
            }
            if($log->logeableTipo == 'App\Models\Multa' ||
               $log->logeableTipo == 'App\Models\Documento' ||
               $log->logeableTipo == 'App\Models\Resolucion' || 
               $log->logeableTipo == 'App\Models\Inhabilitacion'){
                $log->logeable->load('sancion');
            }
        }
        
        return $logs;
    }
}
