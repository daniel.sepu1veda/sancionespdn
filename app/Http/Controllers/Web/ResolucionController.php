<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Models\Resolucion;

class ResolucionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $resoluciones = Resolucion::with('sancion')->get();

        #return $resoluciones;
        $this->crearLog('Consultar resoluciones', 'consultar', 'resoluciones', null);
        return view('admin.resoluciones.index', get_defined_vars());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $resolucion = Resolucion::create($request->all());

        $this->crearLog('Registro resoluciones', 'registro', 'resoluciones', $resolucion);
        return redirect()->back()->with('success', 'ok');
    }

    /**
     * Display the specified resource.
     */
    public function get(string $id)
    {
        $resolucion = Resolucion::with('sancion')->findOrFail($id);

        #return $resolucion;
        $this->crearLog('Consultar resoluciones', 'consultar', 'resoluciones', $resolucion);
        return $this->jsonResponse("Registro consultado correctamente", $resolucion, Response::HTTP_OK, null);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        $resolucion = Resolucion::findOrFail($request->id);

        $resolucion->update($request->all());

        $this->crearLog('Actualizar resoluciones', 'actualizar', 'resoluciones', $resolucion);
        return redirect()->back()->with('success', 'ok');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $resolucion = Resolucion::findOrFail($id);

        $resolucion->delete();

        $this->crearLog('Eliminar resoluciones', 'eliminar', 'resoluciones', $resolucion);
        return $this->jsonResponse("Registro eliminado correctamente", $resolucion, Response::HTTP_OK, null);
    }
}
