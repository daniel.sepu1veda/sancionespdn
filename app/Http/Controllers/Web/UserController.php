<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Models\InstitucionDependencia;
use Spatie\Permission\Models\Role;
use App\Services\UserService;
use App\Models\User;
use App\Models\Log;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        #return $users;
        $this->crearLog('Consultar usuarios', 'consultar', 'usuarios', null);
        return view('admin.usuarios.index', get_defined_vars());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //verificar si el usuario existe para actualizar datos
        $user = User::select('id','email')
                   ->where('nombres', $request->nombres)
                   ->where('primerApellido', $request->primerApellido)
                   ->where('segundoApellido', $request->segundoApellido)
                   ->withTrashed()
                   ->first();
        
        //si existe, sólo actualizarlo
        if($user){
            $request['id'] = $user->id;
            $user->restore();
            $this->update($request);

            //crear log y redireccionar
            $this->crearLog('Actualizar usuarios', 'actualizar', 'usuarios', null);
            return redirect()->back()->with('success', 'ok');
        }

        //crear el usuario
        $user = User::create($request->all());

        //crear nombre de usuario y contraseña
        $username = UserService::generate_username($user);
        
        //actualizar contraseña y correo
        $user->password = bcrypt($username);
        $user->email = $username;
        $user->save();

        //asignar rol
        $user->assignRole($user->role_id);
        
        //instituciones
        if($request->has('institucionesDependencias')){
            //obtener los ids de instituciones
            $institucionesDependencias = explode(',', $request->institucionesDependencias);
            foreach($institucionesDependencias as $institucionDependenciaId){
                $institucionDependencia = InstitucionDependencia::find($institucionDependenciaId);
                //crear relacion
                if($institucionDependencia){
                    $user->institucionesDependencias()->attach($institucionDependencia->id, [
                        'created_at' => now(),
                        'updated_at' => now(),
                    ]);
                }
            }
        }

        $this->crearLog("Registrar usuarios", "registrar", "usuarios", $user);
        return redirect()->route('usuarios.show', $user->id)->with('success', 'ok');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //obtener usuario
        $user = User::with('rol')
                    ->with('institucionesDependencias')
                    ->findOrFail($id);

                    
        //logs
        $logs = Log::where('userId', $id)
                    ->with('logeable')
                    ->orderBy('created_at', 'desc')
                    ->paginate(10);

        //catálogos
        $institucionesDependencias = InstitucionDependencia::all();
        $roles = Role::whereNot('id', 1)->with('permissions')->get();

        #return $logs;
        $this->crearLog('Consultar usuarios', 'consultar', 'usuarios', $user);
        return view('admin.usuarios.details', get_defined_vars());
    }

    /**
     * Display the specified resource.
     */
    public function create()
    {
        //catálogos
        $institucionesDependencias = InstitucionDependencia::all();
        $roles = Role::whereNot('id', 1)->with('permissions')->get();

        #return $institucionesDependencias;
        return view('admin.usuarios.create', get_defined_vars());
    }

    /**
     * Display the specified resource.
     */
    public function get(string $id)
    {
        $user = User::with('rol', 'institucionesDependencias')->findOrFail($id);

        #return $user;
        $this->crearLog('Consultar usuarios', 'consultar', 'usuarios', $user);
        return $this->jsonResponse("Registro consultado correctamente", $user, Response::HTTP_OK, null);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        //obtener usuario
        $user = User::findOrFail($request->id);
        //actualizar
        $user->update($request->all());

        //instituciones
        if($request->has('institucionesDependencias')){
            //eliminar relaciones
            $user->institucionesDependencias()->detach();
            //obtener ids de instituciones
            $institucionesDependencias = explode(',', $request->institucionesDependencias);
            foreach($institucionesDependencias as $institucionDependenciaId){
                $institucionDependencia = InstitucionDependencia::find($institucionDependenciaId);
                //crear relacion
                if($institucionDependencia){
                    $user->institucionesDependencias()->attach($institucionDependencia->id, [
                        'created_at' => now(),
                        'updated_at' => now(),
                    ]);
                }
            }
        }

        //si se actualiza la contraseña se redirigirá a home
        if($request->has('password')){
            $user->firstLogin = false;
            $user->password = bcrypt($request->password);
            $user->save();

            return redirect()->route('home');
        }

        $this->crearLog('Actualizar usuarios', 'actualizar', 'usuarios', $user);
        return redirect()->back()->with('success', 'ok');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //obtener usuario
        $user = User::findOrFail($id);
        //eliminar
        $user->delete();

        $this->crearLog('Eliminar usuarios', 'eliminar', 'usuarios', $user);
        return $this->jsonResponse("Registro eliminado correctamente", $user, Response::HTTP_OK, null);
    }

    public function get_all()
    {
        $users = User::with('rol', 'institucionesDependencias')
                        ->get()->append('nombre_institucion');

        return $users;
    }

    //reestablecer contraseña
    public function reset_password(string $id)
    {
        $user = User::find($id);
        $user->password = bcrypt($user->email);
        $user->save();

        $this->crearLog('Reestablecer contraseña', 'actualizar', 'usuarios', $user);
        return $this->jsonResponse("Registro obtenido correctamente", $user->email, Response::HTTP_OK, null);
    }

    //primer login
    public function change_password()
    {
        $user = auth()->user();
        
        #return $user;
        return view('admin.usuarios.change-password', get_defined_vars());
    }
}

