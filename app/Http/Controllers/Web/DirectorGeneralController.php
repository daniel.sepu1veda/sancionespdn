<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Models\DirectorGeneral;

class DirectorGeneralController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $directoresGenerales = DirectorGeneral::with('particular')->get();

        #return $directoresGenerales;
        $this->crearLog('Consultar directores generales', 'consultar', 'directores-generales', null);
        return view('admin.directores-generales.index', get_defined_vars());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $directorGeneral = DirectorGeneral::create($request->all());

        if(isset($request->sancionId)){
            $directorGeneral->sancion()->attach($request->sancionId, [
                'created_at' => now(),
                'updated_at' => now()
            ]);
        }

        $this->crearLog('Registro directores generales', 'registro', 'directores-generales', $directorGeneral);
        return redirect()->back()->with('success', 'ok');
    }

    /**
     * Display the specified resource.
     */
    public function get(string $id)
    {
        $directorGeneral = DirectorGeneral::with('particular')->findOrFail($id);

        #return $directorGeneral;
        $this->crearLog('Consultar directores generales', 'consultar', 'directores-generales', $directorGeneral);
        return $this->jsonResponse("Registro consultado correctamente", $directorGeneral, Response::HTTP_OK, null);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        $directorGeneral = DirectorGeneral::findOrFail($request->id);

        $directorGeneral->update($request->all());

        $this->crearLog('Actualizar directores generales', 'actualizar', 'directores-generales', $directorGeneral);
        return redirect()->back()->with('success', 'ok');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $directorGeneral = DirectorGeneral::findOrFail($id);

        $directorGeneral->delete();

        $this->crearLog('Eliminar directores generales', 'eliminar', 'directores-generales', $directorGeneral);
        return $this->jsonResponse("Registro eliminado correctamente", $directorGeneral, Response::HTTP_OK, null);
    }
}
