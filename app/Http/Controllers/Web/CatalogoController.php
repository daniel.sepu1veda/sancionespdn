<?php

namespace App\Http\Controllers\Web;

use App\Exports\CatalogoExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Excel;

class CatalogoController extends Controller
{
    //función para exportar catálogo en excel
    public function export(Request $request)
    {
        $request = $this->get_info($request);
        return Excel::download(new CatalogoExport($request), $request->title . '.xlsx');
    }

    //función para obtener campos y encabezados
    public function get_info(Request $request)
    {
        switch($request->model){
            case 'App\Models\InstitucionDependencia':
                $request['fields'] = ['nombre', 'siglas'];
                $request['headings'] = ['Nombre', 'Siglas'];
                $request['title']= 'Instituciones';
                break;
            case 'App\Models\TipoFalta':
                $request['fields'] = ['clave', 'valor'];
                $request['headings'] = ['Clave', 'Valor'];
                $request['title']= 'Tipos de Falta';
                break;
            case 'App\Models\TipoSancion':
                $request['fields'] = ['clave', 'valor'];
                $request['headings'] = ['Clave', 'Valor'];
                $request['title']= 'Tipos de Sanción';
                break; 
            default:
                $request['fields'] = ['clave', 'valor'];
                $request['headings'] = ['Clave', 'Valor'];
                $request['title']= '';
                break;
        }
        return $request;
    }
}
