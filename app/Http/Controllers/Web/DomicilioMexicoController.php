<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Services\DomicilioService;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Models\DomicilioMexico;
use App\Models\Localidad;
use App\Models\Vialidad;

class DomicilioMexicoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $domiciliosMexico = DomicilioMexico::with('vialidad', 'particular')->get();
        
        #return $domiciliosMexico;
        $this->crearLog('Consultar domicilios mexico', 'consultar', 'domicilios-mexico', null);
        return view('admin.domicilios-mexico.index', get_defined_vars());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $domicilioMexico = DomicilioMexico::create($request->all());

        $this->crearLog('Registro domicilios mexico', 'registro', 'domicilios-mexico', $domicilioMexico);
        return redirect()->back()->with('success', 'ok');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function get(string $id)
    {
        $domicilioMexico = DomicilioMexico::with('vialidad', 'particular')->findOrFail($id);
        
        #return $domicilioMexico;
        $this->crearLog('Consultar domicilios mexico', 'consultar', 'domicilios-mexico', $domicilioMexico);
        return $this->jsonResponse("Registro consultado correctamente", $domicilioMexico, Response::HTTP_OK, null);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {
        $domicilioMexico = DomicilioMexico::findOrFail($request->id);

        $domicilioMexico->update($request->all());
        
        //actualizar o crear vialidad
        if(isset($request->vialidad)){
            $vialidad = Vialidad::updateOrCreate(
                ['id' => $request->domicilioMexico['vialidad']['id'] ?? null], 
                $request->vialidad 
            );
            $domicilioMexico->vialidadId = $vialidad->id;
            $domicilioMexico->save();
        }

        $this->crearLog('Actualizar domicilios mexico', 'actualizar', 'domicilios-mexico', $domicilioMexico);
        return redirect()->back()->with('success', 'ok');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $domicilioMexico = DomicilioMexico::findOrFail($id);

        $domicilioMexico->delete();

        $this->crearLog('Eliminar domicilios mexico', 'eliminar', 'domicilios-mexico', $domicilioMexico);
        return $this->jsonResponse("Registro eliminado correctamente", $domicilioMexico, Response::HTTP_OK, null);
    }

    //obtener localidades por código postal
    public function get_localidades_by_cp($cp)
    {
        $domicilio = DomicilioService::get_localidades_by_cp(intval($cp));

        if($domicilio) return $this->jsonResponse("Registro consultado correctamente", $domicilio, Response::HTTP_OK, null);

        return $this->jsonResponse("Registro no encontrado", null, Response::HTTP_NOT_FOUND, null);
    }
}
