<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ServidorPublicoController;
use App\Http\Controllers\Api\ParticularController;
use App\Http\Controllers\Api\SancionController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::get('get/sanciones', [SancionController::class, 'get_sanciones']);
Route::get('get/sancionados', [SancionController::class, 'get_sancionados']);
Route::get('get/sancionados/paginados', [SancionController::class, 'get_sancionados_paginados']);
Route::get('get/servidores-publicos', [ServidorPublicoController::class, 'get_servidores_publicos']);
Route::get('get/particulares', [ParticularController::class, 'get_particulares']);
