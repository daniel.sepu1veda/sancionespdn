<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Web\DomicilioExtranjeroController;
use App\Http\Controllers\Web\ResponsableSancionController;
use App\Http\Controllers\Web\DomicilioMexicoController;
use App\Http\Controllers\Web\ServidorPublicoController;
use App\Http\Controllers\Web\DirectorGeneralController;
use App\Http\Controllers\Web\ApoderadoLegalController;
use App\Http\Controllers\Web\InhabilitacionController;
use App\Http\Controllers\Web\NotificacionController;
use App\Http\Controllers\Web\ConstanciaController;
use App\Http\Controllers\Web\ParticularController;
use App\Http\Controllers\Web\ResolucionController;
use App\Http\Controllers\Web\DocumentoController;
use App\Http\Controllers\Web\CatalogoController;
use App\Http\Controllers\Web\SancionController;
use App\Http\Controllers\Web\MultaController;
use App\Http\Controllers\Web\UserController;
use App\Http\Controllers\Web\HomeController;
use App\Http\Controllers\Web\LogController;

//bienvenido
Route::get('/', function () {
    return view('welcome');
});

//login
Route::get('login', function () {
    return redirect('/');
})->name('login');

//registro
Route::get('register', function () {
    return redirect('/');
});

//rutas públicas de servidores públicos
Route::controller(ServidorPublicoController::class)->prefix('servidores-publicos')
->group(function () {
    Route::get('/sancionados','sancionados')->name('servidores.publicos.sancionados');
    Route::get('/get/all/sancionados','get_all_sancionados')->name('servidores.publicos.get.all.sancionados');
});

//rutas públicas de particulares
Route::controller(ParticularController::class)->prefix('particulares')
->group(function () {
    Route::get('/sancionados','sancionados')->name('particulares.sancionados');
    Route::get('/get/all/sancionados','get_all_sancionados')->name('particulares.get.all.sancionados');
});

//rutas públicas sanciones
Route::get('sanciones/{id}', [SancionController::class, 'show'])->name('sanciones.show');

//middleware de autenticación
Route::middleware(['auth'])->group(function () {

    //dashboard
    Route::get('home', [HomeController::class, 'index'])->name('home');

    //usuarios
    Route::controller(UserController::class)->prefix('usuarios')
    ->group(function () {
        Route::get('/','index')->name('usuarios');
        Route::post('/','store')->name('usuarios.store');
        Route::get('/create','create')->name('usuarios.create');
        Route::get('/get/all','get_all')->name('usuarios.get.all');
        Route::get('/get/{id}','get')->name('usuarios.get');
        Route::get('/{id}','show')->name('usuarios.show');
        Route::put('/','update')->name('usuarios.edit');
        Route::delete('/{id}','destroy')->name('usuarios.destroy');
        Route::get('/change/password', 'change_password')->name('usuarios.change.password');
        Route::get('/reset/password/{id}','reset_password')->name('usuarios.reset.password');
    });
    
    //particulares
    Route::controller(ParticularController::class)->prefix('particulares')
    ->group(function () {
        Route::get('/','index')->name('particulares');
        Route::post('/','store')->name('particulares.store');
        Route::get('/carga/masiva','carga_masiva')->name('particulares.carga.masiva');
        Route::get('/create','create')->name('particulares.create');
        Route::get('/list','list')->name('particulares.list');
        Route::get('/get/all','get_all')->name('particulares.get.all');
        Route::get('/edit/{id}','edit')->name('particulares.change');
        Route::get('/get/{id}','get')->name('particulares.get');
        Route::get('/{id}','show')->name('particulares.show');
        Route::put('/','update')->name('particulares.edit');
        Route::delete('/{id}','destroy')->name('particulares.destroy');
        Route::post('/export','export_sanciones')->name('particulares.export');
        Route::post('/import','import_sanciones')->name('particulares.import');
    });

    //servidores públicos
    Route::controller(ServidorPublicoController::class)->prefix('servidores-publicos')
    ->group(function () {
        Route::get('/','index')->name('servidores.publicos');
        Route::get('/sancionados','sancionados')->name('servidores.publicos.sancionados');
        Route::get('/get-nombres','get_by_nombres')->name('servidores.publicos.get.nombres');
        Route::get('/carga/masiva','carga_masiva')->name('servidores.publicos.carga.masiva');
        Route::get('/{rfc}','show')->name('servidores.publicos.show');
        Route::get('/get/all','get_all')->name('servidores.publicos.get.all');
        Route::get('/get/{id}','get')->name('servidores.publicos.get');
        Route::get('/get-rfc/{rfc}','get_by_rfc')->name('servidores.publicos.get.rfc');
        Route::get('/get-historial/{rfc}','get_historial')->name('servidores.publicos.get.historial');
        Route::get('/list','list')->name('servidores.publicos.list');
        Route::put('/','update')->name('servidores.publicos.edit');
        Route::delete('/{id}','destroy')->name('servidores.publicos.destroy');
        Route::get('/get/import/log','get_import_log')->name('servidores.publicos.get.import.log');
        Route::post('/export','export_sanciones')->name('servidores.publicos.export');
        Route::post('/import','import_sanciones')->name('servidores.publicos.import');
    });

    //sanciones
    Route::controller(SancionController::class)->prefix('sanciones')
    ->group(function () {
        Route::get('/','index')->name('sanciones');
        Route::post('/','store')->name('sanciones.store');
        Route::get('/excel','generate_excel')->name('sanciones.excel');
        Route::get('/create/particulares','create')->name('sanciones.create.particulares');
        Route::get('/create/servidores-publicos','create')->name('sanciones.create.servidores.publicos');
        Route::get('/delete/all','delete_all')->name('sanciones.destroy.all');
        Route::post('/generate/servidores-publicos','generate_sanciones_servidores_publicos')->name('sanciones.generate.servidores.publicos');
        Route::post('/generate/particulares','generate_sanciones_particulares')->name('sanciones.generate.particulares');
        Route::get('/get/{id}','get')->name('sanciones.get');
        Route::get('/pdf/{id}','generate_pdf')->name('sanciones.pdf');
        Route::put('/','update')->name('sanciones.edit');
        Route::delete('/{id}','destroy')->name('sanciones.destroy');
    });

    //apoderados legales
    Route::controller(ApoderadoLegalController::class)->prefix('apoderados-legales')
    ->group(function () {
        Route::get('/','index')->name('apoderados.legales');
        Route::post('/','store')->name('apoderados.legales.store');
        Route::get('/get/{id}','get')->name('apoderados.legales.get');
        Route::put('/','update')->name('apoderados.legales.edit');
        Route::delete('/{id}','destroy')->name('apoderados.legales.destroy');
    });

    //directores generales
    Route::controller(DirectorGeneralController::class)->prefix('directores-generales')
    ->group(function () {
        Route::get('/','index')->name('directores.generales');
        Route::post('/','store')->name('directores.generales.store');
        Route::get('/get/{id}','get')->name('directores.generales.get');
        Route::put('/','update')->name('directores.generales.edit');
        Route::delete('/{id}','destroy')->name('directores.generales.destroy');
    });

    //domicilios mexico
    Route::controller(DomicilioMexicoController::class)->prefix('domicilios-mexico')
    ->group(function () {
        Route::get('/','index')->name('domicilios.mexico');
        Route::post('/','store')->name('domicilios.mexico.store');
        Route::get('/get/{id}','get')->name('domicilios.mexico.get');
        Route::put('/','update')->name('domicilios.mexico.edit');
        Route::delete('/{id}','destroy')->name('domicilios.mexico.destroy');
    });

    //domicilios extranjero
    Route::controller(DomicilioExtranjeroController::class)->prefix('domicilios-extranjero')
    ->group(function () {
        Route::get('/','index')->name('domicilios.extranjero');
        Route::post('/','store')->name('domicilios.extranjero.store');
        Route::get('/get/{id}','get')->name('domicilios.extranjero.get');
        Route::put('/','update')->name('domicilios.extranjero.edit');
        Route::delete('/{id}','destroy')->name('domicilios.extranjero.destroy');
    });

    //responsables de sancion
    Route::controller(ResponsableSancionController::class)->prefix('responsables-sancion')
    ->group(function () {
        Route::get('/','index')->name('responsables.sancion');
        Route::post('/','store')->name('responsables.sancion.store');
        Route::get('/get/{id}','get')->name('responsables.sancion.get');
        Route::put('/','update')->name('responsables.sancion.edit');
        Route::delete('/{id}','destroy')->name('responsables.sancion.destroy');
    });

    //resoluciones
    Route::controller(ResolucionController::class)->prefix('resoluciones')
    ->group(function () {
        Route::get('/','index')->name('resoluciones');
        Route::post('/','store')->name('resoluciones.store');
        Route::get('/get/{id}','get')->name('resoluciones.get');
        Route::put('/','update')->name('resoluciones.edit');
        Route::delete('/{id}','destroy')->name('resoluciones.destroy');
    });

    //inhabilitaciones
    Route::controller(InhabilitacionController::class)->prefix('inhabilitaciones')
    ->group(function () {
        Route::get('/','index')->name('inhabilitaciones');
        Route::post('/','store')->name('inhabilitaciones.store');
        Route::get('/get/{id}','get')->name('inhabilitaciones.get');
        Route::put('/','update')->name('inhabilitaciones.edit');
        Route::delete('/{id}','destroy')->name('inhabilitaciones.destroy');
    });

    //multas
    Route::controller(MultaController::class)->prefix('multas')
    ->group(function () {
        Route::get('/','index')->name('multas');
        Route::post('/','store')->name('multas.store');
        Route::get('/get/{id}','get')->name('multas.get');
        Route::put('/','update')->name('multas.edit');
        Route::delete('/{id}','destroy')->name('multas.destroy');
    });

    //documentos
    Route::controller(DocumentoController::class)->prefix('documentos')
    ->group(function () {
        Route::get('/','index')->name('documentos');
        Route::post('/','store')->name('documentos.store');
        Route::get('/get/{id}','get')->name('documentos.get');
        Route::put('/','update')->name('documentos.edit');
        Route::delete('/{id}','destroy')->name('documentos.destroy');
    });

    //constancias
    Route::controller(ConstanciaController::class)->prefix('constancias')
    ->group(function () {
        Route::get('/','index')->name('constancias');
        Route::get('/create','create')->name('constancias.create');
        Route::get('/get/all','get_all')->name('constancias.get.all');
        Route::get('/busqueda/estatal','busqueda_estatal')->name('constancias.busqueda.estatal');
        Route::get('/busqueda/nacional','busqueda_nacional')->name('constancias.busqueda.nacional');
        Route::get('/generate/{rfc}','generate_constancia')->name('constancias.pdf');
    });

    //notificaciones
    Route::controller(NotificacionController::class)->prefix('notificaciones')
    ->group(function () {
        Route::get('/','index')->name('notificaciones');
        Route::post('/','store')->name('notificaciones.store');
        Route::get('/create','create')->name('notificaciones.create');
        Route::get('/get/all','get_all')->name('notificaciones.get.all');
        Route::get('/get/{id}','get')->name('notificaciones.get');
        Route::get('/{id}','show')->name('notificaciones.show');
        Route::put('/','update')->name('notificaciones.edit');
        Route::delete('/{id}','destroy')->name('notificaciones.destroy');
    });

    //logs
    Route::controller(LogController::class)->prefix('logs')
    ->group(function () {
        Route::get('/','index')->name('logs');
        Route::get('/get/all','get_all')->name('logs.get.all');
    });

    //catalogos
    Route::controller(CatalogoController::class)->prefix('catalogos')
    ->group(function () {
        Route::post('/export','export')->name('catalogos.export');
    });

    //rutas pruebas
    Route::get('particulares-store-test', [ParticularController::class, 'store_test'])->name('particulares.store.test');
    Route::get('particulares-update-test', [ParticularController::class, 'update_test'])->name('particulares.update.test');
    Route::get('sanciones-store-test', [SancionController::class, 'store_test'])->name('sanciones.store.test');
    Route:: get('sanciones-update-test', [SancionController::class, 'update'])->name('sanciones.update.test');

    //rutas adicionales
    Route:: get('get-localidades/{cp}', [DomicilioMexicoController::class, 'get_localidades_by_cp'])->name('localidades.get');

});

//rutas públicas constancias
Route::get('constancias/{folio}', [ConstanciaController::class, 'show'])->name('constancias.show');
